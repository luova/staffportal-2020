webpackJsonp([0],{

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export User */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var User = (function () {
    function User(name, email) {
        this.name = name;
        this.email = email;
    }
    return User;
}());

var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        this.apiURL = "http://portaladmin.luovalabs.com";
        console.log('Hello AuthService Provider');
    }
    AuthService.prototype.login = function (credentials) {
        var _this = this;
        if (credentials.telephone === null) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw("Please insert credentials");
        }
        else {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (observer) {
                // At this point make a request to your backend to make a real check!
                var access = (credentials.telephone === "12462411511");
                _this.currentUser = new User('Test User', 'hello@luovalabs.com');
                observer.next(access);
                observer.complete();
            });
        }
    };
    AuthService.prototype.getUserInfo = function () {
        return this.currentUser;
    };
    AuthService.prototype.logout = function () {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (observer) {
            _this.currentUser = null;
            observer.next(true);
            observer.complete();
        });
    };
    AuthService.prototype.postData = function (credentials, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
            _this.http.post(_this.apiURL + type, JSON.stringify(credentials), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    return AuthService;
}());
AuthService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
], AuthService);

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Config */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return sender_id; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return oneSignalAppId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return debugMode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleAnalyticsCode; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Config = (function () {
    function Config() {
        this.wpurl = "";
    }
    return Config;
}());
Config = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], Config);

var sender_id = '801139965353';
var oneSignalAppId = 'bc17d9b8-a757-418d-9e5f-1f369f5df780';
var debugMode = false;
var GoogleAnalyticsCode = 'UA-104903092-1';
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 158:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 158;

/***/ }),

/***/ 201:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/login/login.module": [
		202
	],
	"../pages/search/search.module": [
		361
	],
	"../pages/welcome/welcome.module": [
		362
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 201;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = (function () {
    function LoginPageModule() {
    }
    return LoginPageModule;
}());
LoginPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]
        ]
    })
], LoginPageModule);

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_staff_staff__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__welcome_welcome__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_sim__ = __webpack_require__(711);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_sms__ = __webpack_require__(712);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_onesignal__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(http, menu, navCtrl, navParams, authService, alertCtrl, loadingCtrl, storage, sim, staff, sms, oneSignal) {
        var _this = this;
        this.http = http;
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authService = authService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.sim = sim;
        this.staff = staff;
        this.sms = sms;
        this.oneSignal = oneSignal;
        this.credentials = { telephone: '', verification: '' };
        this.verfyStep1 = true;
        this.verfyStep2 = false;
        this.sendState = "";
        this.loginAction = false;
        this.autoVerify = false;
        // Check to see if the user if already verified in the application
        // If so then redirect to the HomePage
        // This maybe handled by the App Component already though
        this.storage.get('usertoken').then(function (result) {
            if (result) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
            }
        });
    }
    // Helper Functions
    LoginPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait. Verifying Phone Number ..."
        });
        this.loader.present();
    };
    LoginPage.prototype.showError = function (text) {
        if (this.loader != null) {
            this.loader.dismiss();
        }
        var alert = this.alertCtrl.create({
            title: 'Fail',
            subTitle: text,
            buttons: ['OK']
        });
        alert.present();
    };
    // Disable the menu so unauthorized users cannot get into the app
    LoginPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
        this.menu.enable(false);
    };
    // Reenable the menu swipping ability
    LoginPage.prototype.ionViewDidLeave = function () {
        // Don't forget to return the swipe to normal, otherwise 
        // the rest of the pages won't be able to swipe to open menu
        this.menu.swipeEnable(true);
        this.menu.enable(true);
    };
    // Verify the phone number that the user is entering is valid
    LoginPage.prototype.verify = function () {
        // Verify the phone number format
        this.credentials.telephone = this.credentials.telephone.replace(/\D/g, '');
        var telephoneNumber = this.credentials.telephone;
        this.saveNumber = telephoneNumber;
        if (telephoneNumber.length != 10) {
            var alert_1 = this.alertCtrl.create({
                title: 'Invalid Number',
                subTitle: 'Invalid phone number entered, remember to include your area code.',
                buttons: ['OK']
            });
            alert_1.present();
        }
        else {
            // Loading
            var loading = this.loadingCtrl.create({
                content: 'Sending Verification Code. Please wait...'
            });
            loading.present();
            this.loading = loading;
            this.checkStaffVerification();
        }
    };
    // Determine if the user is allowed to skip the authentication
    LoginPage.prototype.checkStaffVerification = function () {
        var _this = this;
        /* Check if the user is allowed to skip number authorisation */
        // The admin personal adjust this setting from their admin panel
        this.staff.checkStaffAutoVerification(this.credentials.telephone).subscribe(function (verificationStateBypass) {
            console.log("verificationStateBypass");
            console.log(verificationStateBypass);
            if (verificationStateBypass) {
                console.log("User Auto Verified");
                _this.generateVerificationNumberForAutoVerify(_this.credentials.telephone);
            }
            else {
                console.log("User Not Auto Verified. Proceed with Auth Verification");
                _this.verifyTheUserWithTextMessage();
            }
        }, function (error) {
            _this.loginAction = false;
            _this.verifyTheUserWithTextMessage();
        });
    };
    LoginPage.prototype.verifyTheUserWithTextMessage = function () {
        var _this = this;
        console.log("verifyNumberWithText");
        var apiVerifyTelephone = "http://portaladmin.luovalabs.com/dev/index.php/sendText/";
        this.http.get("http://portaladmin.luovalabs.com/dev/index.php/sendText/" + this.credentials.telephone)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.loading.dismiss();
            // Switch Form Fields
            _this.verfyStep1 = false;
            _this.verfyStep2 = true;
        }, function (error) {
            alert("An error occured with sending the verification code.");
            alert(error);
        });
    };
    // This function generates a code saved to the users device
    // to enable the user to bypass authentication without a code
    LoginPage.prototype.generateVerificationNumberForAutoVerify = function (telephoneNumber) {
        var _this = this;
        this.autoVerify = true;
        var loading = this.loadingCtrl.create({
            content: 'Auto verifying your account. Please wait...'
        });
        loading.present();
        var random = this.generateRandomNumber();
        this.storage.set('verify_number', random);
        this.credentials.verification = random + "";
        setTimeout(function () {
            loading.dismiss();
            _this.loginAction = false;
            _this.handleIntro();
        }, 2500);
    };
    LoginPage.prototype.generateRandomNumber = function () {
        return Math.floor(Math.random() * (999999 - 100000)) + 100000;
    };
    /* Triggers the generation of another random number and resends the SMS */
    LoginPage.prototype.retryVerificationNumber = function () {
        this.verifyTheUserWithTextMessage();
    };
    LoginPage.prototype.LoginUsingVerifyCode = function () {
        var _this = this;
        var verification = this.credentials.verification;
        var telephone = this.credentials.telephone;
        this.staff.verifyNumberWithTextCode(telephone, verification).subscribe(function (allowed) {
            if (allowed["error"] == false) {
                _this.sendState = "Verification Successful, One Moment";
                // Set variable stating login successfull
                // Setting this tells our application that the user is successfully verified
                _this.storage.set('verified_number', _this.credentials.telephone);
                _this.handleIntro();
            }
            else {
                _this.sendState = "Invalid Auth Code";
                _this.loginAction = false;
                _this.showError("An error occured verifying your access, please enter the authentication code sent to your phone");
            }
        });
    };
    /* Allow the user to re-enter their telephone number */
    LoginPage.prototype.reenterNumber = function () {
        this.sendState = "";
        this.verfyStep2 = false;
        this.verfyStep1 = true;
    };
    LoginPage.prototype.handleIntro = function () {
        var _this = this;
        // Determine if the user has seen the intro our not as yet
        this.storage.get('introImgShown').then(function (result) {
            //If the intro is shown then redirect the user to the home screen
            if (result == true) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
            }
            else {
                _this.storage.set('introImgShown', true);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__welcome_welcome__["a" /* WelcomePage */]);
            }
        });
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/login/login.html"*/'<ion-content class="login-content" padding>\n  <ion-row class="logo-row">\n    <ion-col></ion-col>\n    <ion-col width-67>\n      <img src="assets/logo.png"/>\n    </ion-col>\n    <ion-col></ion-col>\n  </ion-row>\n  <div class="login-box" *ngIf = "verfyStep1">\n    <form (ngSubmit)="verify()" #registerForm="ngForm">\n      <ion-row>\n        <ion-col>\n          <ion-list inset>\n\n            <h1 color = "#fff"> Verify Phone Number </h1>\n            <p> We will send you a SMS message to verify your phone number. Please include the country code.</p>\n            <ion-input class = "phoneInput" type="tel" placeholder="Phone Number" name="tel" [(ngModel)]="credentials.telephone" required></ion-input>\n\n          </ion-list>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col class="signup-col">\n          <button ion-button class="submit-btn" full type="submit" [disabled]="!registerForm.form.valid">Verify</button>\n        </ion-col>\n      </ion-row>\n\n    </form>\n  </div>\n\n  <div class="login-box" *ngIf = "verfyStep2">\n    <form (ngSubmit)="LoginUsingVerifyCode()" #loginForm="ngForm">\n      <ion-row>\n        <ion-col>\n          <ion-list inset>\n\n            <h1 color = "#fff"> Enter Verification Number </h1>\n            <ion-input class = "phoneInput" type="tel" placeholder="" name="tel" [(ngModel)]="credentials.verification" required></ion-input>\n            <p>{{sendState}}</p>\n\n          </ion-list>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col class="signup-col">\n          <button ion-button class="submit-btn" full type="submit" [disabled]="!loginForm.form.valid">Login &nbsp; <ion-spinner color = "light" *ngIf = "loginAction"></ion-spinner> </button>\n          <br/><br/>\n          <div class="col-sm-12" (click) = "retryVerificationNumber()"> Resend SMS </div>\n          <br/>\n          <div class="col-sm-12" (click) = "reenterNumber()"> Re-enter Number </div>\n        </ion-col>\n\n      </ion-row>\n\n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/login/login.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_7__ionic_native_sim__["a" /* Sim */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_sms__["a" /* SMS */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_onesignal__["a" /* OneSignal */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_sim__["a" /* Sim */], __WEBPACK_IMPORTED_MODULE_3__providers_staff_staff__["a" /* StaffProvider */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_sms__["a" /* SMS */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_onesignal__["a" /* OneSignal */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WelcomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_badge__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_post_post__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var WelcomePage = (function () {
    function WelcomePage(ga, badge, platform, navCtrl, navParams, storage, oneSignal) {
        this.ga = ga;
        this.badge = badge;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.oneSignal = oneSignal;
        this.appbuttontext = "Skip To App";
        this.pagerOption = true;
        this.debugMode = false;
        this.initializeApp();
    }
    WelcomePage.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.setupOneSignalNotification();
            _this.setupGoogleAnalytics();
        });
    };
    WelcomePage.prototype.setupOneSignalNotification = function () {
        var _this = this;
        this.oneSignal.startInit(__WEBPACK_IMPORTED_MODULE_7__config__["c" /* oneSignalAppId */], __WEBPACK_IMPORTED_MODULE_7__config__["d" /* sender_id */]);
        //The inFocusDisplaying() method, controls how OneSignal notifications will be shown when one is received while your app is in focus. 
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        //The handleNotificationReceived() is a method that runs when a notification is received. It calls the onPushReceived()
        this.oneSignal.handleNotificationReceived().subscribe(function (data) { return _this.onPushReceived(data, data.payload); });
        // The handleNotificationOpened() is a method that runs when a notification is tapped or when closing an alert notification shown in the app.
        this.oneSignal.handleNotificationOpened().subscribe(function (data) { return _this.onPushOpened(data, data.notification.payload); });
        // Check to see if the user gave us permission
        var state = this.oneSignal.getPermissionSubscriptionState();
        //The endInit() method must be called to complete the initialization of OneSignal.
        this.oneSignal.endInit();
        // Get the User ID, this identifies the user. We'll use this ID and set the Player ID
        this.oneSignal.getIds().then(function (ids) {
            var playerID = ids.userId;
            _this.storage.set("playerID", playerID);
        });
    };
    WelcomePage.prototype.setupGoogleAnalytics = function () {
        var _this = this;
        //Google Analytics
        this.ga.startTrackerWithId('UA-104903092-1')
            .then(function () {
            //console.log('Google analytics is ready now');
            _this.ga.trackView('test');
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
    };
    // Code to handle the notifications
    WelcomePage.prototype.onPushReceived = function (data, payload) {
        if (this.notHandlerCheck != true) {
            //alert("Running handleNotificationReceived from handleNotificationOpened ");
            this.notHandlerCheck = true;
            this.notificationHandling(data);
        }
    };
    WelcomePage.prototype.onPushOpened = function (data, payload) {
        if (this.notHandlerCheck != true) {
            //alert("Running notificationHandling from handleNotificationOpened ");
            this.notHandlerCheck = true;
            this.notificationHandling(data);
        }
    };
    // Custom behavious to handle how the app should behave with a new notification
    WelcomePage.prototype.notificationHandling = function (data) {
        this.notHandlerCheck = false;
        if (this.debugMode == true) {
            this.storage.set('launchPost', data);
            if (data) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_post_post__["a" /* PostPage */], {
                    item: data,
                    type: "notification"
                });
            }
        }
        else {
            var sid = data.notification.payload.additionalData.postID;
            data["sid"] = sid;
            this.storage.set('launchPost', data);
            if (data) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_post_post__["a" /* PostPage */], {
                    item: data,
                    type: "notification"
                });
            }
        }
    };
    WelcomePage.prototype.provideAppWithData = function () {
        var _this = this;
        this.oneSignal.getIds().then(function (ids) {
            _this.playerID = ids.userId;
            _this.storage.set("playerID", _this.playerID);
        });
    };
    WelcomePage.prototype.skipIntro = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    WelcomePage.prototype.slideChanged = function () {
        var currentIndex = this.slides.getActiveIndex();
        this.pagerOption = true;
        if (currentIndex >= 2) {
            this.appbuttontext = "Start App!";
            this.pagerOption = false;
        }
        else {
            this.appbuttontext = "Skip Intro";
        }
    };
    return WelcomePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* Slides */])
], WelcomePage.prototype, "slides", void 0);
WelcomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-welcome',template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/welcome/welcome.html"*/'<ion-slides pager="{{pagerOption}}" (ionSlideDidChange)="slideChanged()">\n  \n    <ion-slide>\n      <img src="assets/WelcomeNew.png" />\n    \n    <h1> Welcome! </h1>\n    <p>To the Simpson Motors Staff Portal. Now you can keep up-to-date with the latest general and staff news. </p>\n    \n    </ion-slide>\n  \n    <ion-slide>\n      <img src="assets/slide_StaffGenStories.png" style="width:350px;"/>\n    \n    <h1> Let\'s Get Started. </h1>\n    <p> Articles are categorised into staff and general sections for easy reading and navigation. </p>\n    \n    </ion-slide>\n  \n    <ion-slide>\n      <img src="assets/Welcome_slide_new.png" />\n    \n    <h1> Article Search. </h1>\n    <p> Easily search for archived articles from both staff and general news. </p>\n    \n    <button ion-button color="blue" (click)="skipIntro()"> Start App !</button>\n    \n    </ion-slide>\n  \n  </ion-slides>\n  \n  <button id = "startApp" *ngIf = "skipAllowed" ion-button color="light" (click)="skipIntro()">{{appbuttontext}}</button>\n  '/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/welcome/welcome.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__["a" /* OneSignal */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_badge__["a" /* Badge */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__["a" /* GoogleAnalytics */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_badge__["a" /* Badge */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__["a" /* OneSignal */]])
], WelcomePage);

//# sourceMappingURL=welcome.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search__ = __webpack_require__(713);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchPageModule = (function () {
    function SearchPageModule() {
    }
    return SearchPageModule;
}());
SearchPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */]),
        ],
    })
], SearchPageModule);

//# sourceMappingURL=search.module.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomePageModule", function() { return WelcomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__welcome__ = __webpack_require__(360);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var WelcomePageModule = (function () {
    function WelcomePageModule() {
    }
    return WelcomePageModule;
}());
WelcomePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__welcome__["a" /* WelcomePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__welcome__["a" /* WelcomePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__welcome__["a" /* WelcomePage */]
        ]
    })
], WelcomePageModule);

//# sourceMappingURL=welcome.module.js.map

/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_staff_staff__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic2_super_tabs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_photo_viewer_ngx__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_badge__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser_ngx__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


//import { DatePipe } from '@angular/common';










var PostPage = (function () {
    function PostPage(photoViewer, _browser, _element, badge, platform, superTabsCtrl, sanitizer, nav, navParams, storage, staff, toastCtrl, alertCtrl, modalCtrl, oneSignal) {
        var _this = this;
        this.photoViewer = photoViewer;
        this._browser = _browser;
        this._element = _element;
        this.badge = badge;
        this.platform = platform;
        this.superTabsCtrl = superTabsCtrl;
        this.sanitizer = sanitizer;
        this.nav = nav;
        this.navParams = navParams;
        this.storage = storage;
        this.staff = staff;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.oneSignal = oneSignal;
        this.readTime = null;
        this.wordsPerMinute = 270;
        this.contentLoaded = false;
        this.articleError = false;
        this.replaceVideoEmbed = "";
        // Get the user playerID
        this.storage.get("playerID").then(function (data) {
            if (data == null) {
                //alert("Get Player ID");
                _this.oneSignal.getIds().then(function (ids) {
                    _this.playerID = ids.userId;
                    _this.storage.set("playerID", _this.playerID);
                });
            }
            else {
                //alert("Already got it");
                _this.playerID = data;
            }
        });
    }
    PostPage.prototype.hideToolbar = function () {
        this.superTabsCtrl.showToolbar(false);
    };
    PostPage.prototype.showToolbar = function () {
        this.superTabsCtrl.showToolbar(true);
    };
    PostPage.prototype.ionViewWillLeave = function () {
        /*alert("Clear badge on leave");
        this.badge.get().then((bcount) => {
            alert(bcount);
            this.badge.decrease(bcount);
            alert( this.badge.get() );
        });*/
    };
    /**
  * Use Angular OnInit lifecycle hook to trigger functionality when the view is initialising
  * @public
  * @method ngOnInit
  * @return {none}
  */
    PostPage.prototype.ngOnInit = function () {
        //this._enableDynamicHyperlinks();
    };
    /**
    * Enable hyperlinks that are embedded within a HTML string
    * @private
    * @method _enableDynamicHyperlinks
    * @return {none}
    */
    PostPage.prototype._enableDynamicHyperlinks = function () {
        var _this = this;
        // Provide a minor delay to allow the HTML to be rendered and 'found'
        // within the view template
        setTimeout(function () {
            // Query the DOM to find ALL occurrences of the <a> hyperlink tag
            var urls = _this._element.nativeElement.querySelectorAll('a');
            var imgurls = _this._element.nativeElement.querySelectorAll('img');
            // Iterate through these
            urls.forEach(function (url) {
                // Listen for a click event on each hyperlink found
                url.addEventListener('click', function (event) {
                    // Retrieve the href value from the selected hyperlink
                    event.preventDefault();
                    _this._link = event.target.href;
                    // Log values to the console and open the link within the InAppBrowser plugin
                    //alert('Name is: ' + event.target.innerText);
                    window.open("https://www.luovalabs.com", "_blank");
                }, false);
            });
        }, 2000);
    };
    /**
    * Creates/launches an Ionic Native InAppBrowser window to display hyperlink locations within
    * @private
    * @method _launchInAppBrowser
    * @param {string}    link           The URL to visit within the InAppBrowser window
    * @return {none}
    */
    PostPage.prototype._launchInAppBrowser = function (link) {
        var opts = "location=yes,clearcache=yes,hidespinner=no";
        this._browser.create(link, '_self', opts);
    };
    PostPage.prototype.ngAfterViewInit = function () {
        var _this = this;
        //alert("Clear badge on destroy");
        this.badge.get().then(function (bcount) {
            //alert(bcount);
            _this.badge.decrease(bcount - 1);
            //alert( this.badge.get() );
        });
        try {
            var y = document.querySelector("#mainHeader").style.display = "none";
            var i = document.querySelector("super-tabs-toolbar").style.display = "none";
            var x = document.querySelector(".supertabs").style.marginTop = "0px";
            var h = document.querySelector(".supertabs").style.height = "100%";
        }
        catch (e) {
            //alert(e);
        }
        this.hideToolbar();
    };
    // Update the view before leaving by changing it back to normal
    PostPage.prototype.ionViewDidLeave = function () {
        this.showToolbar();
        try {
            var y = document.querySelector("#mainHeader").style.display = "inline";
            var i = document.querySelector("super-tabs-toolbar").style.display = "block";
            var x = document.querySelector(".supertabs").style.marginTop = "68px";
            var h = document.querySelector(".supertabs").style.height = "100%";
        }
        catch (e) {
            //alert(e);
        }
    };
    // for tasks you want to do every time you enter in the view (setting event listeners, updating a table, etc.).
    // Check if the article is still valid
    // Track the users progress
    PostPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = this.navParams.get('item');
        var type = this.navParams.get('type');
        // Determine whether or not this article is from a regular page or its from a notification
        var re = new RegExp("<a([^>]* )href=\"([^\"]+)\"", "g");
        //alert("type " + type);
        if (type == "simple") {
            //alert("Simple");
            this.stitle = this.selectedItem.title;
            this.sid = this.selectedItem.id;
            this.postDate = this.selectedItem.date;
            this.articleContent = this.selectedItem.content;
            //this.articleContent = this.articleContent.replace(re, "<a$1href=\"#\" data-nlink =\"photoViewer.show('$2')\"");
            this.articleContent = this.sanitizer.bypassSecurityTrustHtml(this.articleContent);
            // Check to see if this post if still available to users and hasn't been removed or edited.
            this.postAvailable();
            //this.calcReadTime();
        }
        else {
            this.sid = this.selectedItem.sid;
            this.notificationID = this.selectedItem.notification.payload.notificationID;
            // testing
            //this.sid = this.selectedItem;
            //this.notificationID = "e873bfa9-a092-41ec-8178-7f51acdcf947";
            // testing
            this.staff.getNotificationPost(this.sid).subscribe(function (notification) {
                // Use different attributes to access the information
                _this.stitle = notification["title"]["rendered"];
                _this.postDate = notification["date"];
                _this.articleContent = notification["content"]["rendered"];
                //this.articleContent = this.articleContent.replace(re, "<a$1href=\"#\" data-nlink =\"photoViewer.show('$2')\"");
                //this.articleContent = this.articleContent.replace("<img", "<img imageViewer");
                _this.updateNotificationRead();
                // Check to see if this post if still available to users and hasn't been removed or edited.
                _this.postAvailable();
            }, function (error) {
                var alert = _this.alertCtrl.create({
                    title: 'Oops!',
                    subTitle: "Unable to fetch article. " + error,
                    buttons: ['OK']
                });
                alert.present();
            });
            /*

            // Use different attributes to access the information
            this.stitle = this.selectedItem.notification.payload.additionalData.postTitle;
            this.sid = this.selectedItem.notification.payload.additionalData.postID;
            this.postDate = this.selectedItem.notification.payload.additionalData.postDate;
            this.notificationID = this.selectedItem.notification.payload.notificationID;
            this.articleContent = this.selectedItem.ncontent;
            
            this.updateNotificationRead();

            */
        }
    };
    PostPage.prototype.checkContent = function (event) {
        //console.log(event);
        //alert(JSON.stringify(event));
        //alert(JSON.stringify(event.target.currentSrc));
        //alert(JSON.stringify(event.target.dataset.nlink));
        //let browser = this.iab.create("https://www.google.com","_blank");
        //browser.show();
        if (event.target.localName == "img") {
            //alert(event.target.currentSrc);
            this.openLink(event.target.currentSrc);
        }
    };
    PostPage.prototype.openLink = function (url) {
        //const browser = this.iab.create(url, '_blank', 'location=yes,enableViewportScale=yes,hardwareback=yes,zoom=yes');
        //browser.show();
        //this.photoViewer.show('https://mysite.com/path/to/image.jpg');
        window.open(url, '_blank');
    };
    PostPage.prototype.postAvailable = function () {
        return __awaiter(this, void 0, void 0, function () {
            var postStatus, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.staff.checkarticle(this.sid)];
                    case 1:
                        postStatus = _a.sent();
                        console.log(" postStatus " + postStatus);
                        // Track the reading of the post by the user
                        this.trackUserPostRead();
                        // Allow content to be shown
                        this.contentLoaded = true;
                        // Remove launchPost variable which handles notifications pending to be shown
                        this.storage.set("launchPost", null);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error("Error occured while fetching the post information.");
                        // Don't content to be shown
                        this.contentLoaded = false;
                        this.articleError = true;
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PostPage.prototype.trackUserPostRead = function () {
        try {
            this.staff.track(this.playerID, this.stitle, this.sid).subscribe(function (data) {
                //alert( JSON.stringify(data) );
            });
        }
        catch (error) {
            //alert(error);
        }
    };
    // Update the datebase with the users ID regarding the notification read
    PostPage.prototype.updateNotificationRead = function () {
        try {
            this.staff.updateNotificationRead(this.playerID, this.notificationID, this.sid).subscribe(function (results) {
                //alert( "update Notification Read " + JSON.stringify(results) );
            }, function (error) {
                //alert( "Error updating Notification Read " + JSON.stringify(error) );
            });
        }
        catch (error) {
            //alert(error);
        }
    };
    /* Convert Date to proper human readable date */
    PostPage.prototype.convertDate = function (date) {
        if (date != "0000-00-00 00:00:00") {
            return __WEBPACK_IMPORTED_MODULE_5_moment___default()(date).format('ll');
        }
    };
    return PostPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])('mainHeader'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"])
], PostPage.prototype, "defaultContent", void 0);
PostPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
        selector: 'page-post',template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/post/post.html"*/'<ion-header class = "postHeader">\n  \n  <ion-navbar>\n    <ion-title *ngIf = "contentLoaded == true">News Article</ion-title>\n  </ion-navbar>\n  \n  </ion-header>\n  \n  <ion-content padding *ngIf = "contentLoaded == true">\n  <div class="selection" id = "postContent">\n    <h2 [innerHTML]="stitle"></h2>\n  \n    <ion-row>\n    <ion-col><ion-note class = "small_info"><ion-icon name="time"></ion-icon> {{ convertDate(postDate) }}</ion-note></ion-col>\n    <ion-col *ngIf = "readTime" text-right class = "small_info">Read Time {{readTime}}</ion-col>\n    </ion-row>\n  \n    <div id = "contentInnerSection" (click)="checkContent($event);" [innerHTML]="articleContent"></div>\n\n    <div *ngIf = "articleError" class = "text-center">\n    <h4> Unable to get article, article maybe removed or an internet connection to the server is unavailable.</h4>\n    </div>\n  </div>\n  </ion-content>\n  '/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/post/post.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__["a" /* OneSignal */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_badge__["a" /* Badge */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__ionic_native_photo_viewer_ngx__["a" /* PhotoViewer */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_9__ionic_native_badge__["a" /* Badge */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_7_ionic2_super_tabs__["a" /* SuperTabsController */], __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__providers_staff_staff__["a" /* StaffProvider */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["u" /* ToastController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* ModalController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__["a" /* OneSignal */]])
], PostPage);

//# sourceMappingURL=post.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__post_post__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_staff_staff__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*import { trigger, state, style, animate, transition } from '@angular/animations';*/

var ListPage = ListPage_1 = (function () {
    function ListPage(navCtrl, navParams, storage, staff, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.staff = staff;
        this.toastCtrl = toastCtrl;
        this.noarticles = false;
        this.articleList = [];
        this.removedArticleList = [];
        this.editAvailable = false;
        this.grabArticles();
    }
    ListPage.prototype.grabArticles = function () {
        var _this = this;
        // set a key/value
        this.storage.get("myarticles").then(function (val) {
            //console.log(val);
            if (val) {
                _this.noarticles = false;
                _this.articleList = val;
            }
            else {
                _this.noarticles = true;
            }
            return name;
        });
    };
    ListPage.prototype.presentRemoveToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Article removed from list',
            duration: 3000
        });
        toast.present();
    };
    ListPage.prototype.removeArticle = function (item, event, index) {
        //console.log("Remove Article Information");
        //console.log(index);
        /*console.log(item);
        console.log(event);*/
        var _this = this;
        //alert("remove");
        // set a key/value
        this.removedArticleList.push(item);
        this.storage.set("edited_articles", this.removedArticleList);
        //console.log(this.removedArticleList);
        this.storage.get("myarticles").then(function (val) {
            if (val) {
                val.splice(index, 1);
                _this.storage.set("myarticles", val);
            }
            _this.articleList = val;
            _this.presentRemoveToast();
        });
    };
    /* Convert Date to proper human readable date */
    ListPage.prototype.convertDate = function (date) {
        if (date != "0000-00-00 00:00:00") {
            return __WEBPACK_IMPORTED_MODULE_5_moment___default()(date).format('LLLL');
        }
    };
    ListPage.prototype.editItems = function (event) {
        //console.log(event);
        if (this.editAvailable == false) {
            event.target.innerText = "Done";
        }
        else {
            event.target.innerText = "Edit";
        }
        this.editAvailable = !this.editAvailable;
    };
    ListPage.prototype.getTitle = function (item) {
        //console.log(item);
        try {
            //console.log(item["article"]);
            return item["article"]["title"];
        }
        catch (e) {
        }
    };
    ListPage.prototype.getDate = function (item) {
        try {
            return __WEBPACK_IMPORTED_MODULE_5_moment___default()(item["article"]["date"]).format('LLLL');
        }
        catch (e) {
        }
    };
    ListPage.prototype.pushPage = function (item) {
        //alert(JSON.stringify(item));
        // push another page onto the navigation stack
        // causing the nav controller to transition to the new page
        // optional data can also be passed to the pushed page.
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__post_post__["a" /* PostPage */], {
            item: item["article"],
            type: 'simple'
        });
    };
    ListPage.prototype.getStorage = function (name) {
        // set a key/value
        this.storage.get(name).then(function (val) {
            //console.log('Requested Storage Data is', val);
            return name;
        });
    };
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    return ListPage;
}());
ListPage = ListPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-list',template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n\n    <ion-row>\n    <ion-col col-3>\n    <button class = "menutotoggle" ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n      <span class="menutotogglecut"></span>\n    </button>\n  </ion-col>\n\n  <ion-col col-6>\n    <ion-title class = "makeAdjust" align-title="center" mode="ios"> <img src = "assets/logo.png" width = "80px"> </ion-title>\n  </ion-col>\n\n	<ion-col col-3 text-right (click)="editItems($event)" *ngIf = "articleList.length > 0" class = "editIcoWrap">\n		<span class="editIco"><ion-icon name="edit"></ion-icon> Edit</span>\n	</ion-col>\n	\n</ion-row>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n	<div text-center *ngIf = "articleList.length > 0">\n\n		<h2> My Saved Articles </h2>\n		<p> Saved articles from the main post to be read later. </p>\n\n	</div>\n	\n	<div id = "empty_state" text-center *ngIf = "articleList.length == 0">\n\n		<img src = "assets/open-box.png">\n		<h1> No Articles Saved! </h1>\n		<p> Get started saving articles by selecting the star icon <ion-icon name = "star-outline"></ion-icon> next to the article </p>\n\n	</div>\n	\n	<ion-grid>\n		<!--<div @itemState *ngFor="let item of articleList; let i = index">-->\n		<div *ngFor="let item of articleList; let i = index">\n			<ion-row class = "article_group">\n			\n				<ion-col col-11 (click)="pushPage(item)">\n					<h2 class="post_title item-text-wrap" text-wrap>{{ getTitle(item) }}</h2>\n					<ion-note class="post-date">{{ getDate(item) }}</ion-note>\n				</ion-col>\n				\n				<ion-col *ngIf = "editAvailable" (click) = "removeArticle(item, $event, i)" col-1>\n					<ion-icon name="close" class = "removeSection"></ion-icon>\n				</ion-col>\n				\n			</ion-row>\n		</div>\n	</ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/list/list.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__providers_staff_staff__["a" /* StaffProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* ToastController */]])
], ListPage);

var ListPage_1;
//# sourceMappingURL=list.js.map

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(421);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export WpApiLoaderFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(731);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_post_post_module__ = __webpack_require__(732);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_search_search_module__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_list_list__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login_module__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_categories_news_news__ = __webpack_require__(733);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_categories_staff_staff__ = __webpack_require__(734);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_welcome_welcome_module__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_settings_settings__ = __webpack_require__(735);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_wp_wp__ = __webpack_require__(736);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_wp_api_angular__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_wp_api_angular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_18_wp_api_angular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_auth_service_auth_service__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_staff_staff__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21_ionic2_super_tabs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_badge__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_photo_viewer_ngx__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_onesignal__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_in_app_browser_ngx__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























//import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


function WpApiLoaderFactory(http) {
    return new __WEBPACK_IMPORTED_MODULE_18_wp_api_angular__["WpApiStaticLoader"](http, 'http://simpson-motors.com/wp-json/');
}
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_categories_staff_staff__["a" /* StaffPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_categories_news_news__["a" /* NewsPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_settings_settings__["a" /* SettingsPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_15__angular_http__["HttpModule"],
            __WEBPACK_IMPORTED_MODULE_5__pages_post_post_module__["a" /* PostPageModule */],
            __WEBPACK_IMPORTED_MODULE_8__pages_login_login_module__["LoginPageModule"],
            __WEBPACK_IMPORTED_MODULE_11__pages_welcome_welcome_module__["WelcomePageModule"],
            __WEBPACK_IMPORTED_MODULE_6__pages_search_search_module__["SearchPageModule"],
            __WEBPACK_IMPORTED_MODULE_21_ionic2_super_tabs__["b" /* SuperTabsModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_16__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_18_wp_api_angular__["WpApiModule"].forRoot({
                provide: __WEBPACK_IMPORTED_MODULE_18_wp_api_angular__["WpApiLoader"],
                useFactory: (WpApiLoaderFactory),
                deps: [__WEBPACK_IMPORTED_MODULE_15__angular_http__["Http"]]
            }),
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                tabsHideOnSubPages: false
            }, {
                links: [
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/welcome/welcome.module#WelcomePageModule', name: 'WelcomePage', segment: 'welcome', priority: 'low', defaultHistory: [] }
                ]
            })
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_categories_staff_staff__["a" /* StaffPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_categories_news_news__["a" /* NewsPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_settings_settings__["a" /* SettingsPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_24__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_25__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_19__providers_auth_service_auth_service__["a" /* AuthService */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_17__providers_wp_wp__["a" /* WpProvider */],
            __WEBPACK_IMPORTED_MODULE_20__providers_staff_staff__["a" /* StaffProvider */],
            __WEBPACK_IMPORTED_MODULE_22__ionic_native_badge__["a" /* Badge */],
            __WEBPACK_IMPORTED_MODULE_23__ionic_native_photo_viewer_ngx__["a" /* PhotoViewer */],
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 453:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 208,
	"./af.js": 208,
	"./ar": 209,
	"./ar-dz": 210,
	"./ar-dz.js": 210,
	"./ar-kw": 211,
	"./ar-kw.js": 211,
	"./ar-ly": 212,
	"./ar-ly.js": 212,
	"./ar-ma": 213,
	"./ar-ma.js": 213,
	"./ar-sa": 214,
	"./ar-sa.js": 214,
	"./ar-tn": 215,
	"./ar-tn.js": 215,
	"./ar.js": 209,
	"./az": 216,
	"./az.js": 216,
	"./be": 217,
	"./be.js": 217,
	"./bg": 218,
	"./bg.js": 218,
	"./bm": 219,
	"./bm.js": 219,
	"./bn": 220,
	"./bn.js": 220,
	"./bo": 221,
	"./bo.js": 221,
	"./br": 222,
	"./br.js": 222,
	"./bs": 223,
	"./bs.js": 223,
	"./ca": 224,
	"./ca.js": 224,
	"./cs": 225,
	"./cs.js": 225,
	"./cv": 226,
	"./cv.js": 226,
	"./cy": 227,
	"./cy.js": 227,
	"./da": 228,
	"./da.js": 228,
	"./de": 229,
	"./de-at": 230,
	"./de-at.js": 230,
	"./de-ch": 231,
	"./de-ch.js": 231,
	"./de.js": 229,
	"./dv": 232,
	"./dv.js": 232,
	"./el": 233,
	"./el.js": 233,
	"./en-au": 234,
	"./en-au.js": 234,
	"./en-ca": 235,
	"./en-ca.js": 235,
	"./en-gb": 236,
	"./en-gb.js": 236,
	"./en-ie": 237,
	"./en-ie.js": 237,
	"./en-nz": 238,
	"./en-nz.js": 238,
	"./eo": 239,
	"./eo.js": 239,
	"./es": 240,
	"./es-do": 241,
	"./es-do.js": 241,
	"./es-us": 242,
	"./es-us.js": 242,
	"./es.js": 240,
	"./et": 243,
	"./et.js": 243,
	"./eu": 244,
	"./eu.js": 244,
	"./fa": 245,
	"./fa.js": 245,
	"./fi": 246,
	"./fi.js": 246,
	"./fo": 247,
	"./fo.js": 247,
	"./fr": 248,
	"./fr-ca": 249,
	"./fr-ca.js": 249,
	"./fr-ch": 250,
	"./fr-ch.js": 250,
	"./fr.js": 248,
	"./fy": 251,
	"./fy.js": 251,
	"./gd": 252,
	"./gd.js": 252,
	"./gl": 253,
	"./gl.js": 253,
	"./gom-latn": 254,
	"./gom-latn.js": 254,
	"./gu": 255,
	"./gu.js": 255,
	"./he": 256,
	"./he.js": 256,
	"./hi": 257,
	"./hi.js": 257,
	"./hr": 258,
	"./hr.js": 258,
	"./hu": 259,
	"./hu.js": 259,
	"./hy-am": 260,
	"./hy-am.js": 260,
	"./id": 261,
	"./id.js": 261,
	"./is": 262,
	"./is.js": 262,
	"./it": 263,
	"./it.js": 263,
	"./ja": 264,
	"./ja.js": 264,
	"./jv": 265,
	"./jv.js": 265,
	"./ka": 266,
	"./ka.js": 266,
	"./kk": 267,
	"./kk.js": 267,
	"./km": 268,
	"./km.js": 268,
	"./kn": 269,
	"./kn.js": 269,
	"./ko": 270,
	"./ko.js": 270,
	"./ky": 271,
	"./ky.js": 271,
	"./lb": 272,
	"./lb.js": 272,
	"./lo": 273,
	"./lo.js": 273,
	"./lt": 274,
	"./lt.js": 274,
	"./lv": 275,
	"./lv.js": 275,
	"./me": 276,
	"./me.js": 276,
	"./mi": 277,
	"./mi.js": 277,
	"./mk": 278,
	"./mk.js": 278,
	"./ml": 279,
	"./ml.js": 279,
	"./mr": 280,
	"./mr.js": 280,
	"./ms": 281,
	"./ms-my": 282,
	"./ms-my.js": 282,
	"./ms.js": 281,
	"./my": 283,
	"./my.js": 283,
	"./nb": 284,
	"./nb.js": 284,
	"./ne": 285,
	"./ne.js": 285,
	"./nl": 286,
	"./nl-be": 287,
	"./nl-be.js": 287,
	"./nl.js": 286,
	"./nn": 288,
	"./nn.js": 288,
	"./pa-in": 289,
	"./pa-in.js": 289,
	"./pl": 290,
	"./pl.js": 290,
	"./pt": 291,
	"./pt-br": 292,
	"./pt-br.js": 292,
	"./pt.js": 291,
	"./ro": 293,
	"./ro.js": 293,
	"./ru": 294,
	"./ru.js": 294,
	"./sd": 295,
	"./sd.js": 295,
	"./se": 296,
	"./se.js": 296,
	"./si": 297,
	"./si.js": 297,
	"./sk": 298,
	"./sk.js": 298,
	"./sl": 299,
	"./sl.js": 299,
	"./sq": 300,
	"./sq.js": 300,
	"./sr": 301,
	"./sr-cyrl": 302,
	"./sr-cyrl.js": 302,
	"./sr.js": 301,
	"./ss": 303,
	"./ss.js": 303,
	"./sv": 304,
	"./sv.js": 304,
	"./sw": 305,
	"./sw.js": 305,
	"./ta": 306,
	"./ta.js": 306,
	"./te": 307,
	"./te.js": 307,
	"./tet": 308,
	"./tet.js": 308,
	"./th": 309,
	"./th.js": 309,
	"./tl-ph": 310,
	"./tl-ph.js": 310,
	"./tlh": 311,
	"./tlh.js": 311,
	"./tr": 312,
	"./tr.js": 312,
	"./tzl": 313,
	"./tzl.js": 313,
	"./tzm": 314,
	"./tzm-latn": 315,
	"./tzm-latn.js": 315,
	"./tzm.js": 314,
	"./uk": 316,
	"./uk.js": 316,
	"./ur": 317,
	"./ur.js": 317,
	"./uz": 318,
	"./uz-latn": 319,
	"./uz-latn.js": 319,
	"./uz.js": 318,
	"./vi": 320,
	"./vi.js": 320,
	"./x-pseudo": 321,
	"./x-pseudo.js": 321,
	"./yo": 322,
	"./yo.js": 322,
	"./zh-cn": 323,
	"./zh-cn.js": 323,
	"./zh-hk": 324,
	"./zh-hk.js": 324,
	"./zh-tw": 325,
	"./zh-tw.js": 325
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 453;

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StaffProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_do__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the StaffProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var StaffProvider = (function () {
    function StaffProvider(http, storage, plt) {
        var _this = this;
        this.http = http;
        this.storage = storage;
        this.plt = plt;
        this.apiURL = "http://portaladmin.luovalabs.com/dev/index.php/staff/verify/";
        this.apiCheckUpdates = "http://portaladmin.luovalabs.com/dev/index.php/staff/checkversion/";
        this.apiVerifyAgain = "http://portaladmin.luovalabs.com/dev/index.php/staff/verifyAgain/";
        this.apiCreateTrack = "http://portaladmin.luovalabs.com/dev/index.php/tracking/loginCreate/";
        this.apiTrack = "http://portaladmin.luovalabs.com/dev/index.php/tracking/login/";
        this.apiTrackUser = "http://portaladmin.luovalabs.com/dev/index.php/tracking/post/";
        this.apiTrackNotification = "http://portaladmin.luovalabs.com/dev/index.php/tracking/notification/";
        this.apiToken = "http://portaladmin.luovalabs.com/dev/index.php/staff/token/";
        this.articleAvailable = "https://www.simpsonmotors.com/wp-json/wp/v2/posts/";
        this.staff_verify_bypass_url = "http://portaladmin.luovalabs.com/dev/index.php/staff/autoverify/";
        this.apiVerifyTelephone = "http://portaladmin.luovalabs.com/dev/index.php/sendText/";
        this.apiVerifyTelephoneText = "http://portaladmin.luovalabs.com/dev/index.php/verifyText/";
        this.url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160';
        this.staff_url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160';
        this.articlesToLoad = 10;
        this.load_url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&per_page=' + this.articlesToLoad;
        this.load_staff_url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&per_page=' + this.articlesToLoad;
        this.featured = [];
        this.staff_featured = [];
        this.articleList = [];
        this.newsGroup = "staff";
        this.loadingActive = false;
        this.skipAllowed = false;
        this.articlesLoaded = 0;
        this.staff_articlesLoaded = 0;
        this.combine_articlesLoaded = 0;
        this.nowDate = new Date();
        this.ONE_HOUR = 60 * 60 * 1000; /* ms */
        this.playerid = null;
        this.storage.get("myarticles").then(function (val) {
            _this.articleList = val;
        });
        this.storage.get('playerID').then(function (result) {
            if (result) {
                _this.playerid = result;
            }
        });
        var s = this.nowDate < this.ONE_HOUR;
        //alert(s);
        /* Check the last time the user was verified */
        this.storage.get('last_verified').then(function (result) {
            /* if 1 hours ago */
            /* get the users information */
            if (s) {
                _this.storage.get('verified_number').then(function (result) {
                    if (result) {
                        /* If the number is verified */
                        _this.verifyNumberAgain(result).subscribe(function (data) {
                            if (data["error"] == true) {
                                _this.disableUser();
                            }
                            else {
                                /* Set time last verified */
                                _this.storage.set('last_verified', _this.nowDate);
                            }
                        });
                    }
                    else {
                        _this.disableUser();
                    }
                });
            }
        });
    }
    StaffProvider.prototype.checkUserStatus = function () {
        var _this = this;
        this.storage.get('verified_number').then(function (result) {
            if (result) {
                /* If the number is verified */
                _this.verifyNumberAgain(result).subscribe(function (data) {
                    if (data["error"] == true) {
                        _this.disableUser();
                    }
                    else {
                        /* Set time last verified */
                        _this.storage.set('last_verified', _this.nowDate);
                    }
                });
            }
            else {
                _this.disableUser();
            }
        });
    };
    StaffProvider.prototype.disableUser = function () {
        this.storage.set('verified', false);
        this.storage.set('usertoken', null);
    };
    StaffProvider.prototype.verifyNumber = function (number) {
        return this.http.get(this.apiURL + number).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.verifyNumberAgain = function (number) {
        return this.http.get(this.apiVerifyAgain + number).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.verifyNumberWithText = function (telephone) {
        return this.http.get(this.apiVerifyTelephone + telephone).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.verifyNumberWithTextCode = function (telephone, verification) {
        return this.http.get(this.apiVerifyTelephoneText + telephone + "/" + verification).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.updateLastLogin = function (token) {
        return this.http.get(this.apiTrack + token).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.getUpdateInformation = function (version) {
        var type = "android";
        if (this.plt.is('ios')) {
            type = "ios";
        }
        var url = this.apiCheckUpdates + version + "/" + type;
        //alert(url);
        return this.http.get(url).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.updateLogin = function (playerID, tel) {
        return this.http.get(this.apiCreateTrack + playerID + "/" + tel).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.updateNotificationRead = function (playerID, notificationID, postID) {
        //alert( this.playerid );
        var plyid = null;
        if (this.playerid == null) {
            plyid = playerID;
        }
        else {
            plyid = this.playerid;
        }
        //alert( plyid );
        return this.http.get(this.apiTrackNotification + plyid + "/" + notificationID + "/" + postID).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.createToken = function (playerID, number) {
        return this.http.get(this.apiToken + playerID + "/" + number).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.track = function (playerID, title, postid) {
        var _this = this;
        var plyid = null;
        if (this.playerid == null) {
            plyid = playerID;
        }
        else {
            plyid = this.playerid;
        }
        //alert(plyid);
        if (plyid) {
            //alert("plyid is here" + plyid);
            return this.http.get("http://portaladmin.luovalabs.com/dev/index.php/tracking/post/" + postid + "/" + encodeURIComponent(title) + "/" + plyid + "").map(function (res) { return res.json(); });
        }
        else {
            //alert("Check storage");
            this.storage.get('playerID').then(function (result) {
                if (result) {
                    //alert("Results id : " + result)
                    return _this.http.get("http://portaladmin.luovalabs.com/dev/index.php/tracking/post/" + postid + "/" + encodeURIComponent(title) + "/" + result + "").map(function (res) { return res.json(); });
                }
                else {
                    //alert("Nothing Found");
                }
            });
        }
    };
    StaffProvider.prototype.getMore = function (date) {
        return this.http.get(this.url + "&after=" + date).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.getMoreStaff = function (date) {
        return this.http.get(this.staff_url + "&after=" + date).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.getNotificationPost = function (id) {
        return this.http.get("https://www.simpsonmotors.com/wp-json/wp/v2/posts/" + id).map(function (res) { return res.json(); });
    };
    StaffProvider.prototype.handleStaffPost = function (data) {
    };
    StaffProvider.prototype.checkarticle = function (postID) {
        //return new Promise((resolve,reject) => {
        this.articleAvailable += postID;
        return this.http.get(this.articleAvailable)
            .map(function (res) { return res.json(); })
            .map(function (data) {
            if (data["data"]) {
                if (data["data"]["status"] == 404) {
                    //resolve(false);
                    return false;
                }
            }
            else {
                //resolve(true);
                return true;
            }
        });
        //});
    };
    /* Handles grabbing the information from the Wordpress API specifically for Staff Articles */
    StaffProvider.prototype.grabWordpressStaffInfo = function (loadType) {
        // Check if storage if empty and also check the last post
        // if empty, load all post and save to storage
        // if not empty, update post data if new article available
        // if error occurs loading articles, load from storage
        var _this = this;
        this.http.get(this.load_staff_url)
            .map(function (res) { return res.json(); })
            .map(function (data) {
            var posts = [];
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var post = data_1[_i];
                /* Determine if the post has any featured image attached to it */
                var featured_image = null;
                if (post['_embedded']) {
                    if (post['_embedded']['wp:featuredmedia']) {
                        var fmedia = post['_embedded']['wp:featuredmedia'][0];
                        if (fmedia['source_url']) {
                            featured_image = fmedia['source_url'];
                        }
                    }
                }
                var savedState = _this.checkIfSaved(post['id']);
                posts.push({ 'author': post['author'], 'id': post['id'], 'title': post['title']['rendered'], 'content': post['content']['rendered'], 'category': 'Staff', 'date': post['date'], 'featured_media': post['featured_media'], "post_image": featured_image, "savedState": savedState
                });
                _this.staff_articlesLoaded++;
            }
            _this.storage.set("staff_articles", posts);
            return posts;
        })
            .subscribe(function (data) {
            // Update Storage
            //this.storage.set("staff_articles", data);
            //console.log("Complete Staff");
            //console.log(data);
        }, function (err) {
            //alert("Error Occured");
            var items_temp = _this.getStorage("staff_articles");
            _this.items = items_temp;
            //if(items_temp){
            //this.items = items_temp;
            //}
        });
    };
    /* Handles grabbing the information from the Wordpress API specifically for Staff Articles */
    StaffProvider.prototype.grabStaff = function () {
        // Check if storage if empty and also check the last post
        // if empty, load all post and save to storage
        // if not empty, update post data if new article available
        // if error occurs loading articles, load from storage
        var _this = this;
        //alert(this.load_staff_url);
        return this.http.get(this.load_staff_url)
            .map(function (res) { return res.json(); })
            .map(function (data) {
            //alert( "data" );
            //alert( JSON.stringify(data) );
            var posts = [];
            for (var _i = 0, data_2 = data; _i < data_2.length; _i++) {
                var post = data_2[_i];
                var featured_image = _this.getFeaturedImage(post);
                var savedState = _this.checkIfSaved(post['id']);
                posts.push({ 'author': post['author'], 'id': post['id'], 'title': post['title']['rendered'], 'content': post['content']['rendered'], 'category': 'Staff', 'date': post['date'], 'featured_media': post['featured_media'], "post_image": featured_image, "savedState": savedState
                });
                _this.storage.set("staff_last_date", post['date']);
                _this.staff_articlesLoaded++;
            }
            _this.storage.set("staff_articles", posts);
            //alert( "POSTs" );
            //alert( JSON.stringify(posts) );
            return posts;
        });
    };
    /* Handles grabbing the information from the Wordpress API specifically for Staff Articles */
    StaffProvider.prototype.grabGeneral = function () {
        // Check if storage if empty and also check the last post
        // if empty, load all post and save to storage
        // if not empty, update post data if new article available
        // if error occurs loading articles, load from storage
        var _this = this;
        return this.http.get(this.load_url)
            .map(function (res) { return res.json(); })
            .map(function (data) {
            var posts = [];
            for (var _i = 0, data_3 = data; _i < data_3.length; _i++) {
                var post = data_3[_i];
                var featured_image = _this.getFeaturedImage(post);
                var category = _this.getCategory(post);
                var savedState = _this.checkIfSaved(post['id']);
                posts.push({ 'author': post['author'], 'id': post['id'], 'title': post['title']['rendered'], 'content': post['content']['rendered'], 'category': category["category"], 'date': post['date'], 'featured_media': post['featured_media'], "post_image": featured_image, "savedState": savedState
                });
                _this.storage.set("last_date", post['date']);
                _this.articlesLoaded++;
            }
            _this.storage.set("articles", posts);
            return posts;
        });
    };
    StaffProvider.prototype.getCategory = function (post) {
        var featured_category;
        var featured_category_plain;
        var info = [];
        if (post['_embedded']['wp:term']) {
            var pCategory = post['_embedded']['wp:term'][0][0];
            if (pCategory['name']) {
                featured_category = pCategory['name'];
                if (featured_category == "Automakers") {
                    featured_category = "<span class = 'color_blue'>" + featured_category + "</span>";
                    featured_category_plain = "automakers";
                }
                else if (featured_category == "News") {
                    featured_category = "<span class = 'color_red'>" + featured_category + "</span>";
                    featured_category_plain = "news";
                }
                else if (featured_category == "Staff") {
                    featured_category = "<span class = 'color_green'>" + featured_category + "</span>";
                    featured_category_plain = "staff";
                }
                else if (featured_category == "Uncategorized") {
                    featured_category = "";
                    featured_category_plain = "";
                }
            }
        }
        info["category"] = featured_category;
        info["plain"] = featured_category_plain;
        return info;
    };
    StaffProvider.prototype.getFeaturedImage = function (post) {
        /* Determine if the post has any featured image attached to it */
        var featured_image = null;
        /* Grab the image from the actual post */
        var str = post['excerpt']['rendered'] + "";
        var elem = document.createElement("div");
        elem.innerHTML = str;
        var images = elem.getElementsByTagName("img");
        var src = "";
        /* Loop through all images found in the html content */
        for (var i = 0; i < images.length; i++) {
            //console.log(images[i].src);
            src = images[i].src;
            var newStr = str.replace(/<img[^>]*>/g, '');
        }
        if (post['_embedded']) {
            if (post['_embedded']['wp:featuredmedia']) {
                var fmedia = post['_embedded']['wp:featuredmedia'][0];
                if (fmedia['source_url']) {
                    featured_image = fmedia['source_url'];
                }
            }
        }
        /* If no featured image is found then use an image from the src */
        if (featured_image == null) {
            if (src != null) {
                featured_image = src;
            }
        }
        //console.log("featured_image");
        //console.log(featured_image);
        return featured_image;
    };
    /* Handles grabbing the information from the Wordpress API */
    StaffProvider.prototype.grabWordpressInfo = function (loadType) {
        /*if( loadType != "none"){
          this.presentLoading();
          this.items = this.getStorage("articles");
        }*/
        var _this = this;
        // Check if storage if empty and also check the last post
        // if empty, load all post and save to storage
        // if not empty, update post data if new article available
        // if error occurs loading articles, load from storage
        this.http.get(this.load_url)
            .map(function (res) { return res.json(); })
            .map(function (data) {
            var posts = [];
            var index = 0;
            for (var _i = 0, data_4 = data; _i < data_4.length; _i++) {
                var post = data_4[_i];
                //console.log(data);
                /* Grab the image from the actual post */
                var str = post['excerpt']['rendered'] + "";
                var elem = document.createElement("div");
                elem.innerHTML = str;
                var images = elem.getElementsByTagName("img");
                var src = "";
                /* Loop through all images found in the html content */
                for (var i = 0; i < images.length; i++) {
                    //console.log(images[i].src);
                    src = images[i].src;
                    var newStr = str.replace(/<img[^>]*>/g, '');
                }
                /* Determine if the post has any featured image attached to it */
                var featured_image = null;
                var featured_category = null;
                var featured_category_plain = null;
                if (post['_embedded']) {
                    if (post['_embedded']['wp:featuredmedia']) {
                        var fmedia = post['_embedded']['wp:featuredmedia'][0];
                        if (fmedia['source_url']) {
                            featured_image = fmedia['source_url'];
                        }
                    }
                    /*console.log("WP Term");
                    console.log(post['_embedded']['wp:term']);
                    console.log(post['_embedded']['wp:term'][0][0]);
                    console.log(post['_embedded']['wp:term'][0][0]["name"]);*/
                    if (post['_embedded']['wp:term']) {
                        var pCategory = post['_embedded']['wp:term'][0][0];
                        if (pCategory['name']) {
                            featured_category = pCategory['name'];
                            if (featured_category == "Automakers") {
                                featured_category = "<span class = 'color_blue'>" + featured_category + "</span>";
                                featured_category_plain = "automakers";
                            }
                            else if (featured_category == "News") {
                                featured_category = "<span class = 'color_red'>" + featured_category + "</span>";
                                featured_category_plain = "news";
                            }
                            else if (featured_category == "Staff") {
                                featured_category = "<span class = 'color_green'>" + featured_category + "</span>";
                                featured_category_plain = "staff";
                            }
                            else if (featured_category == "Uncategorized") {
                                featured_category = "";
                                featured_category_plain = "";
                            }
                        }
                    }
                }
                //console.log(featured_category);
                /* If no featured image is found then use an image from the src */
                if (featured_image == null) {
                    if (src != null) {
                        featured_image = src;
                    }
                }
                if (_this.articlesLoaded == 0) {
                    _this.featured.push({ 'author': post['author'], 'id': post['id'], 'title': post['title']['rendered'], 'content': post['content']['rendered'], 'category': featured_category, 'excerpt': newStr, 'date': post['date'], 'featured_media': post['featured_media'], 'post_image': featured_image });
                    _this.storage.set("last_date", post['date']);
                }
                else {
                    var savedState = _this.checkIfSaved(post['id']);
                    posts.push({ 'author': post['author'], 'id': post['id'], 'title': post['title']['rendered'], 'content': post['content']['rendered'], 'category': featured_category, 'excerpt': newStr, 'date': post['date'], 'featured_media': post['featured_media'], 'post_image': featured_image, "savedState": savedState });
                }
                _this.articlesLoaded++;
                _this.combine_articlesLoaded++;
            }
            _this.storage.set("featured", _this.featured);
            _this.storage.set("articles", posts);
            return posts;
        })
            .subscribe(function (data) {
            // Update Storage
            //this.storage.set("articles", data);
            //console.log("Complete General");
            //console.log(data);
        }, function (err) {
            _this.items = _this.getStorage("articles");
        });
    };
    /* Helper Function when looping through articles from API, checks to see if articles loaded are saved by the user or not! */
    StaffProvider.prototype.checkIfSaved = function (id) {
        //console.log( "Length " + this.articleList.length );
        if (this.articleList.length > 0) {
            for (var i = 0; i < this.articleList.length; i++) {
                if (id == this.articleList[i]["article"].id) {
                    //alert(this.articleList.length );
                    return i;
                }
            }
            return -1;
        }
        return -1;
    };
    StaffProvider.prototype.checkCardSpecial = function (date, items) {
        var lastMonth = new Date();
        lastMonth.setMonth(lastMonth.getMonth(), 0);
        var article = new Date(date);
        var stateMonth = article.getTime() >= lastMonth.getTime();
        if (stateMonth) {
            //alert( "Article " + article );
            //alert( "Last Month " + lastMonth );
            return "card-special";
        }
    };
    /* Handles Setting Local Storage to Phone */
    StaffProvider.prototype.setStorage = function (name, data) {
        // set a key/value
        this.storage.set(name, data);
    };
    /* Handles Retrieving Local Storage from Phone */
    StaffProvider.prototype.getStorage = function (name) {
        // set a key/value
        this.storage.get(name).then(function (val) {
            //console.log('Requested Storage Data is', val);
            return name;
        });
    };
    /* Check if the user has auto verification enabled */
    StaffProvider.prototype.checkStaffAutoVerification = function (tel) {
        return this.http.get(this.staff_verify_bypass_url + tel + "/3243")
            .map(function (res) { return res.json(); });
    };
    return StaffProvider;
}());
StaffProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["r" /* Platform */]])
], StaffProvider);

//# sourceMappingURL=staff.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__post_post__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_staff_staff__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic2_super_tabs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HomePage = (function () {
    function HomePage(badge, superTabsCtrl, http, loadingCtrl, storage, navCtrl, staff, toastCtrl, alertCtrl, platform) {
        this.badge = badge;
        this.superTabsCtrl = superTabsCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.staff = staff;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.articlesToLoad = 10;
        this.url = 'https://www.simpsonmotors.com/wp-json/wp/v2/posts?_embed&per_page=' + this.articlesToLoad;
        this.loadingActive = false;
        this.articlesLoaded = 10;
        this.staff_articlesLoaded = 10;
        this.headingName = "Latest News";
        this.articleList = [];
        this.noarticlesorconnection = false;
        this.active_loading = false;
        // Load Post Information
        //this.loadWordpressInfo();
        this.updateUserSavedList();
    }
    // Flow Functions
    HomePage.prototype.ionViewWillEnter = function () {
        //this.badge.clear();
        this.badge.set(0);
        this.debugLog("ionViewWillEnter Loaded");
        this.doRefresh(null);
        this.pollServer();
    };
    HomePage.prototype.doRefresh = function (refresher) {
        //alert("Polling");
        if (this.active_loading == false) {
            this.mrefresher = refresher;
            this.active_loading = true;
            /* Once the Refresher Takes Over 8 Secs, we cancel the refresh */
            setTimeout(function () {
                if (refresher) {
                    if (refresher.state == "refreshing") {
                        refresher.cancel();
                    }
                }
            }, 10000);
            var loopNum = 0;
            var amount_to_load = this.articlesLoaded + this.articlesToLoad;
            if (amount_to_load > 30) {
                amount_to_load = 30;
            }
            this.url = 'https://www.simpsonmotors.com/wp-json/wp/v2/posts?_embed&offset=0&per_page=' + amount_to_load;
            //alert( this.url );
            this.debugLog("doRefresh started");
            console.log("refresh");
            this.grabWordpressInfo("load");
        }
        else {
            //alert("Loading Occurring");
            try {
                refresher.cancel();
            }
            catch (e) {
            }
        }
    };
    /* Handles grabbing the information from the Wordpress API */
    HomePage.prototype.grabWordpressInfo = function (loadType) {
        var _this = this;
        if (loadType == "reload") {
            this.presentLoading();
        }
        var url = this.url;
        if (loadType == "reload") {
            url = 'https://www.simpsonmotors.com/wp-json/wp/v2/posts?_embed&per_page=' + this.articlesLoaded;
        }
        // Disable Errors
        this.noarticlesorconnection = false;
        this.http.get(url)
            .map(function (res) { return res.json(); })
            .map(function (data) { return _this.handleArticleSorting(data, loadType); })
            .subscribe(function (data) {
            if (loadType == "load") {
                if (_this.loader) {
                    _this.loader.dismiss();
                }
                _this.items = data;
                // Update Storage
                _this.setStorage("articles", data);
                _this.active_loading = false;
                _this.loadingActive = false;
                // Scan articles for saved selections and higlight them
                _this.checkArticlesSaved(data);
            }
            else {
                if (_this.loader) {
                    _this.loader.dismiss();
                }
                for (var i = 0; i < data.length; i++) {
                    _this.items.push(data[i]);
                }
                // Update Storage
                _this.setStorage("articles", _this.items);
                _this.loadingActive = false;
                if (_this.infin) {
                    _this.infin.complete();
                }
                _this.debugLog("Set Active Loading False");
                _this.active_loading = false;
                _this.loadingActive = false;
                _this.debugLog("active_loading " + _this.active_loading);
            }
            if (_this.mrefresher) {
                //console.log('Async operation has ended');
                _this.mrefresher.complete();
                _this.presentToast(' Refresh Complete! ');
            }
            _this.superTabsCtrl.clearBadge('news_tab');
        }, function (err) {
            _this.debugLog("grabWordpressStaffInfo error occured with loadType " + loadType);
            _this.handleGetWordpressInformationError(err, loadType);
        });
    };
    HomePage.prototype.handleArticleSorting = function (data, loadType) {
        var posts = [];
        var index = 0;
        if (data.length > 0) {
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var post = data_1[_i];
                /* Set date to the date of the very first index post */
                if (index == 0 && loadType == "load") {
                    this.setLastDate(post['date']);
                }
                var featured_image = this.staff.getFeaturedImage(post);
                var category = this.staff.getCategory(post);
                //let savedState = this.checkIfSaved( post[ 'id' ] );
                var savedStatevar = -1;
                posts.push({ 'author': post['author'], 'id': post['id'], 'title': post['title']['rendered'], 'content': post['content']['rendered'], 'category': category["category"], 'date': post['date'], 'featured_media': post['featured_media'], "post_image": featured_image, "savedState": savedStatevar });
                index++;
                this.articlesLoaded++;
            }
        }
        else {
            this.presentToast(' No more post to be read ');
            this.loadingActive = false;
            if (this.infin) {
                this.infin.complete();
            }
        }
        return posts;
    };
    // Loop through the list of loaded articles and check the ones that
    // are saved in the users listing
    HomePage.prototype.checkArticlesSaved = function (data) {
        var _this = this;
        var matchesFound = 0;
        this.storage.get("myarticles").then(function (val) {
            // Get the saved articles
            if (val) {
                _this.debugLog(" Loop through " + val.length + "articles in myarticles listings");
                // Loop through articles in myarticles listing
                for (var i = 0; i < val.length; i++) {
                    // Loop through newly loaded data with articles
                    for (var m = 0; m < data.length; m++) {
                        if (val[i]["article"]["id"] == data[m]["id"]) {
                            _this.debugLog(val[i]["article"]["id"] + " matches " + _this.items[m]["id"]);
                            data[m]["savedState"] = 1;
                            // Update matches found so we'll know to update the listing
                            matchesFound++;
                            _this.debugLog(" Update Match Found ");
                        }
                    }
                }
                // If matches found update listing
                if (matchesFound) {
                    _this.items = data;
                    _this.debugLog(" Update Staff Items List ");
                }
            }
        });
    };
    HomePage.prototype.handleGetWordpressInformationError = function (err, loadType) {
        // Cancel infin staff loader
        if (this.infin) {
            this.infin.complete();
        }
        // Cancel staff loader
        if (this.loader) {
            this.loader.dismiss();
        }
        // Dismiss loader lock
        this.loadingActive = false;
        this.active_loading = false;
        this.debugLog("Revert to older articles");
        // Try to revert to older articles
        if (loadType != "none") {
            var items_temp = this.getStorage("articles");
            /*if(items_temp){
                this.items = items_temp;
            }else{
                this.noarticlesorconnection = true;
            }*/
        }
    };
    HomePage.prototype.debugLog = function (msg) {
        console.log(msg);
    };
    /* Handles grabbing the information from the Wordpress API specifically for News Articles */
    HomePage.prototype.loadWordpressInfo = function () {
        var _this = this;
        // Check to see if any articles are saved
        this.storage.get("articles").then(function (val) {
            if (val) {
                //alert("News Articles Found");
                // Get all articles and place into items variable to be displayed
                _this.items = val;
                // Set the length of the articles
                _this.articlesLoaded = _this.items.length;
                _this.doRefresh(null);
                // Get the last date from the last saved article
                _this.getLastDate();
                // Initiate Polling Server for More Updates
                _this.pollServer();
                //this.getMySavedArticles();
            }
            else {
                //alert("News No Articles Found");
                // Set articles loaded to 0
                _this.articlesLoaded = 0;
                // Grab Articles from server
                _this.grabWordpressInfo("load");
                // Initiate Polling Server for More Updates
                //this.pollServer();
            }
        });
    };
    HomePage.prototype.updateUserSavedList = function () {
        var _this = this;
        /* Check for any items to be removed from the users save list */
        this.storage.get("articles").then(function (newsItemsTemp) {
            if (newsItemsTemp) {
                /* Check for any items to be removed */
                _this.storage.get("edited_articles").then(function (item) {
                    try {
                        if (item && item.length > 0) {
                            for (var m = 0; m < item.length; m++) {
                                for (var i = 0; i < newsItemsTemp.length; i++) {
                                    var articleID = item[m]["article"]["id"];
                                    if (newsItemsTemp[i]["id"] == articleID) {
                                        // Set Item to -1 to say not saved
                                        newsItemsTemp[i]["savedState"] = -1;
                                        // Remove the item found
                                        item.splice(m, 1);
                                        // Update Edited Articles so we know the items are removed
                                        _this.storage.set("edited_articles", item);
                                        // Update Staff Items in Storage
                                        _this.setStorage("articles", newsItemsTemp);
                                        _this.items = newsItemsTemp;
                                    }
                                }
                            }
                        }
                    }
                    catch (e) {
                        _this.debugLog(e);
                    }
                });
            }
        });
    };
    // Helper Functions
    HomePage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000
        });
        toast.present();
    };
    HomePage.prototype.presentRemoveToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Article removed from list',
            duration: 3000
        });
        toast.present();
    };
    HomePage.prototype.getCategories = function (item) {
        return JSON.stringify(item["_embedded"]["wp:term"][0]["name"]);
    };
    HomePage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        this.loader.present();
    };
    // Check the server every so often for new messages
    // Handle editing the articles in terms of articles removed / edited
    HomePage.prototype.pollServer = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__["Observable"].interval(86400000).subscribe(function (x) {
            // Make sure the user if still authorised
            _this.staff.checkUserStatus();
            // Check in with the server to discover more articles
            //this.doRefresh(null);
            // Reload all articles
            // Fix for articles that may not have loaded with images,
            // and also this cleans up articles that were removed
            _this.grabWordpressInfo("reload");
        });
    };
    HomePage.prototype.getLastDateFromList = function () {
        var ndate = this.items[0]["date"];
        this.lastDate = ndate;
        this.setLastDate(ndate);
    };
    HomePage.prototype.checkCardSpecial = function (date) {
        return this.staff.checkCardSpecial(date, this.items);
    };
    HomePage.prototype.showMoreArticlesAvailable = function (index) {
        var _this = this;
        this.moreArticlesAlert = true;
        setTimeout(function () {
            _this.moreArticlesAlert = false;
        }, 200);
    };
    // Handle opening news articles
    HomePage.prototype.pushPage = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__post_post__["a" /* PostPage */], {
            item: item,
            type: "simple",
        });
    };
    // Load older articles
    HomePage.prototype.doInfinite = function (infiniteScroll) {
        this.debugLog("Starting doInfinite");
        this.debugLog("Loading State: " + this.active_loading);
        // Check if any other loading is occuring
        if (this.active_loading == false) {
            // Show loading icon
            this.loadingActive = true;
            // If no loading, lock loading to prevent clashing with other loaders
            // activated by the user
            this.active_loading = true;
            if (this.articlesLoaded != this.items.length) {
                this.articlesLoaded = this.items.length;
            }
            this.url = 'https://www.simpsonmotors.com/wp-json/wp/v2/posts?_embed&offset=' + this.articlesLoaded + '&per_page=' + this.articlesToLoad;
            this.grabWordpressInfo("none");
            this.infin = infiniteScroll;
        }
        else {
            //alert("Loading Occuring do infinite");
        }
    };
    // Loop through the available article and select the correct saved articles
    HomePage.prototype.checkIfSaved = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get("myarticles").then(function (articleList) {
                if (articleList) {
                    if (articleList.length > 0) {
                        for (var i = 0; i < articleList.length; i++) {
                            //alert( articleList[i]["id"] + " " + articleList[i]["article"]["id"] );
                            if (id == articleList[i]["article"]["id"]) {
                                //alert("Match");
                                resolve(true);
                            }
                        }
                        reject(false);
                    }
                    else {
                        reject(false);
                    }
                }
            });
        });
    };
    HomePage.prototype.saveArticle = function (item, event) {
        var _this = this;
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i]["id"] == item.id) {
                this.items[i]["savedState"] = true;
                this.storage.set("articles", this.items);
            }
        }
        // set a key/value
        this.storage.get("myarticles").then(function (val) {
            var article_double = false;
            if (val) {
                for (var i = 0; i < val.length; i++) {
                    if (val[i]["article"]["id"] == item.id) {
                        //alert(" Article Already Added");
                        article_double = true;
                    }
                }
                if (article_double == false) {
                    val.push({ "article": item });
                    _this.storage.set("myarticles", val);
                }
            }
            else {
                var data = [];
                data.push({ "article": item });
                _this.storage.set("myarticles", data);
            }
            try {
                event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star color_blue";
            }
            catch (e) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Oops!',
                    subTitle: "Unable to save " + e,
                    buttons: ['OK']
                });
                alert_1.present();
            }
            _this.presentToast('Article saved to list');
            //return name;
        });
    };
    HomePage.prototype.removeArticle = function (item, event, index) {
        var _this = this;
        //console.log(event);
        var savedFound = false;
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i]["id"] == item.id) {
                item["savedState"] = -1;
                this.debugLog("Found");
            }
        }
        // set a key/value
        this.storage.get("myarticles").then(function (val) {
            if (val) {
                for (var i = 0; i < val.length; i++) {
                    if (val[i]["article"]["id"] == item.id) {
                        var mindex = val.indexOf(val[i]);
                        //alert("Val I Index " + mindex )
                        var removed = val.splice(mindex, 1);
                        //alert("Removed " + JSON.stringify(removed) )
                        _this.storage.set("myarticles", val);
                        //alert( JSON.stringify(val) );
                        _this.presentRemoveToast();
                        return name;
                    }
                }
            }
        });
        event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star-outline";
    };
    /* Convert Date to proper human readable date */
    HomePage.prototype.convertDate = function (date) {
        if (date != "0000-00-00 00:00:00") {
            return __WEBPACK_IMPORTED_MODULE_8_moment___default()(date).format('LL');
            //return this.datePipe.transform(date, 'fullDate');
        }
    };
    HomePage.prototype.setStorage = function (name, data) {
        // set a key/value
        this.storage.set(name, data);
    };
    HomePage.prototype.getStorage = function (name) {
        // set a key/value
        this.storage.get(name).then(function (val) {
            //console.log('Requested Storage Data is', val);
            return name;
        });
    };
    HomePage.prototype.setLastDate = function (date) {
        this.lastDate = date;
        this.storage.set("last_date", date);
    };
    HomePage.prototype.getLastDate = function () {
        var _this = this;
        this.storage.get("last_date").then(function (val) {
            _this.lastDate = val;
            if (_this.lastDate) {
            }
            else {
                _this.lastDate = _this.items[0]["date"];
            }
        });
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/home/home.html"*/'<ion-header id = "mainHeader">\n  <ion-navbar>\n\n    <ion-row>\n    <ion-col col-5 no-padding>\n    <button class = "menutotoggle" ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n      <span class="menutotogglecut"></span>\n    </button>\n  </ion-col>\n\n  <ion-col col-2 no-padding>\n    <ion-title class = "makeAdjust" align-title="center" mode="ios"> <img src = "assets/logo.png" width = "80px"> </ion-title>\n  </ion-col>\n\n  <ion-col col-5 no-padding text-right>\n    <button ion-button right (click) = "openModal({charNum: 1})" class="toggleSearch">\n      <ion-icon name="search"></ion-icon>\n    </button>\n  </ion-col>\n</ion-row>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-row class = "notificationAlert" *ngIf = "show_alert == true">\n    <ion-icon name="alert"></ion-icon>\n    <ion-col> {{alert_message}} </ion-col>\n</ion-row>\n\n<ion-content>\n	\n	<ion-refresher (ionRefresh)="doRefresh($event)">\n		<ion-refresher-content></ion-refresher-content>\n	</ion-refresher>\n	\n    <ion-grid no-padding>\n		\n		<h1 class = "specialH1"> {{headingName}} </h1>\n		\n		<div *ngFor="let item of items; let i = index" no-padding no-margin>\n	  \n      <div class = "card-background-page card-special" *ngIf = "i == 0" (click)="pushPage(item)">\n        \n        <ion-card no-padding no-margin class = "fullwidth">\n          <img src="{{item.post_image}}"/>\n          <div class="card-title" [innerHTML]="item.title"></div>\n        </ion-card>\n        \n      </div>\n		\n		<ion-row ngClass = "article_preview_wrap {{ checkCardSpecial(item.date) }}" *ngIf = "i > 0">\n        \n		<ion-col (click)="pushPage(item)" *ngIf = "item.post_image" col-4 no-padding class="post-image" [ngStyle]="{\'background-image\': \'url(\' + item.post_image + \')\' }">\n\n        </ion-col>\n\n        <ion-col *ngIf = "item.post_image" (click)="pushPage(item)" col-7 padding class="item-text-wrap">\n          <div class = "postCategory" [innerHTML] = "item.category"></div>\n          <h2 class = "post_title" [innerHTML]="item.title"></h2>\n		  \n          <ion-note class="post-date"> <ion-icon name="calendar"></ion-icon> {{convertDate(item.date)}}</ion-note>\n        </ion-col>\n\n        <ion-col *ngIf = "!item.post_image" (click)="pushPage(item)" col-11 padding class="item-text-wrap">\n          <div class = "postCategory" [innerHTML] = "item.category"></div>\n          <h2 class = "post_title" [innerHTML]="item.title"></h2>\n		  \n          <ion-note class="post-date"> <ion-icon name="calendar"></ion-icon> {{convertDate(item.date)}}</ion-note>\n        </ion-col>\n\n        <ion-col col-1>\n		  \n          <ion-icon *ngIf = "item.savedState == -1" (click) = "saveArticle(item, $event)" name="star-outline" class = "saveArticleIcon ext-right"></ion-icon>\n		  \n		  <ion-icon *ngIf = "item.savedState != -1" (click) = "removeArticle(item, $event, item.savedState)" name="star" class = "saveArticleIcon color_blue ext-right"></ion-icon> \n		  \n        </ion-col>\n\n      </ion-row>\n      </div>\n\n\n      <div class = "groupLoader" *ngIf="loadingActive == true" >\n\n        <ion-row no-padding class = "article_preview_wrap">\n          <ion-col col-4 no-padding class="post-image"> <span class="load_fill"></span> </ion-col>\n\n          <ion-col col-7 padding class="item-text-wrap">\n            <div class = "postCategory"> <span class="load_fill load_fill_category"></span> </div>\n            <h2 class = "post_title"> <span class="load_fill"></span> <span class="load_fill"></span> </h2>\n            <ion-note class="post-date"> <span class="load_fill"></span> </ion-note>\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row no-padding class = "article_preview_wrap">\n          <ion-col col-4 no-padding class="post-image"> <span class="load_fill"></span> </ion-col>\n\n          <ion-col col-7 padding class="item-text-wrap">\n            <div class = "postCategory"> <span class="load_fill load_fill_category"></span> </div>\n            <h2 class = "post_title"> <span class="load_fill"></span> <span class="load_fill"></span> </h2>\n            <ion-note class="post-date"> <span class="load_fill"></span> </ion-note>\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row no-padding class = "article_preview_wrap">\n          <ion-col col-4 no-padding class="post-image"> <span class="load_fill"></span> </ion-col>\n\n          <ion-col col-7 padding class="item-text-wrap">\n            <div class = "postCategory"> <span class="load_fill load_fill_category"></span> </div>\n            <h2 class = "post_title"> <span class="load_fill"></span> <span class="load_fill"></span> </h2>\n            <ion-note class="post-date"> <span class="load_fill"></span> </ion-note>\n          </ion-col>\n \n        </ion-row>\n\n      </div>\n\n\n		<ion-infinite-scroll (ionInfinite)="doInfinite($event)" threshold="35%">\n		<ion-infinite-scroll-content></ion-infinite-scroll-content>\n		</ion-infinite-scroll>\n\n    </ion-grid>\n	\n	<div *ngIf = "moreArticlesAlert" class = "articlesMoreHover"> New Articles Available </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/home/home.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__["a" /* Badge */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__["a" /* Badge */], __WEBPACK_IMPORTED_MODULE_9_ionic2_super_tabs__["a" /* SuperTabsController */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_staff_staff__["a" /* StaffProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["u" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["r" /* Platform */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 713:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__post_post__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_debounceTime__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_distinctUntilChanged___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_distinctUntilChanged__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_switchMap__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_switchMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_switchMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the SearchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SearchPage = (function () {
    function SearchPage(alertCtrl, http, platform, params, viewCtrl, storage, navCtrl) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.platform = platform;
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.searchQuery = '';
        this.searchTextDisplay = true;
        this.loadingResults = false;
        this.showInternal = true;
        this.resText = "";
        this.searchList = [];
        this.url = "";
        this.storage.get("articles").then(function (val) {
            _this.items = val;
            _this.itemsOrig = val;
        });
        //  this.initializeItems();
    }
    SearchPage.prototype.initializeItems = function () {
        this.items = this.itemsOrig;
    };
    SearchPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        this.keys = val;
        this.resText = " for " + val;
        if (val.length != 0) {
            this.searchTextDisplay = false;
        }
        else {
            this.searchTextDisplay = true;
        }
        if (val.length >= 2) {
            //setTimeout(() => {
            // if the value is an empty string don't filter the items
            /*if (val && val.trim() != '') {
              this.items = this.items.filter((item) => {
                return (item["title"].toLowerCase().indexOf(val.toLowerCase()) > -1);
              })
            }*/
            this.searchForString(val.toLowerCase());
            //}, 1500);
        }
    };
    /*
  
    if ( this.subscription ) {
      this.subscription.unsubscribe();
    }
    this.subscription = this.http.get( 'awesomeApi' )
    .subscribe((res)=> {
      // your awesome code..
    })
  
    */
    SearchPage.prototype.searchForString = function (keyword) {
        var _this = this;
        this.loadingResults = true;
        this.url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&search=' + keyword;
        //if ( this.subscription ) {
        //this.subscription.unsubscribe();
        //}
        this.http.get(this.url)
            .debounceTime(1500)
            .distinctUntilChanged()
            .switchMap(function (keyword) { return _this.http.get('https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&search=' + _this.keys); })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            //console.log("data");
            //console.log(data);
            // this.searchList = [];
            if (data) {
                for (var x = 0; x < data.length; x++) {
                    //if( ! this.checkID( data[x][ 'id' ] ) ){
                    _this.searchList.push({ 'author': data[x]['author'], 'id': data[x]['id'], 'title': data[x]['title']['rendered'], 'content': data[x]['content']['rendered'], 'date': data[x]['date'], 'featured_media': data[x]['featured_media'] });
                    //}
                }
            }
            _this.loadingResults = false;
            _this.showInternal = false;
        }, function (err) {
            //console.log(err);
            var alert = _this.alertCtrl.create({
                title: 'Unable to load search results',
                buttons: ['OK']
            });
            alert.present();
            _this.loadingResults = false;
            _this.showInternal = true;
        });
    };
    SearchPage.prototype.checkID = function (id) {
        if (this.items) {
            for (var i = 0; i < this.items.length; i++) {
                if (id == this.items[i]["id"]) {
                    return true;
                }
            }
        }
        return false;
    };
    SearchPage.prototype.pushPage = function (item) {
        // push another page onto the navigation stack
        // causing the nav controller to transition to the new page
        // optional data can also be passed to the pushed page.
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__post_post__["a" /* PostPage */], {
            item: item,
            type: 'simple'
        });
    };
    SearchPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    /* Convert Date to proper human readable date */
    SearchPage.prototype.convertDate = function (date) {
        if (date != "0000-00-00 00:00:00") {
            return __WEBPACK_IMPORTED_MODULE_9_moment___default()(date).format('LL');
            //return this.datePipe.transform(date, 'fullDate');
        }
    };
    return SearchPage;
}());
SearchPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-search',template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/search/search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n\n    <button (click)="dismiss()" class = "btnBack"> <ion-icon name="close"></ion-icon> Close </button>\n\n    <ion-title align-title="center" mode="ios"> <img src = "assets/logo.png" width = "80px"> </ion-title>\n\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="searchBg">\n\n  <ion-row text-center *ngIf = "searchTextDisplay">\n    <ion-col class = "searchGroup">\n      <h3> Search </h3>\n      <p> Discover Articles, Product Releases, New Staff and more ! </p>\n    </ion-col>\n  </ion-row>\n\n  <ion-searchbar (ionInput)="getItems($event)" (animated)="true" [showCancelButton]="true"></ion-searchbar>\n\n  <h4 class = "list_heading"> <span *ngIf = "searchTextDisplay == false"> Results <span *ngIf = "resText">{{resText}}</span> </span> <ion-spinner *ngIf = "loadingResults" class = "resultsLoader" stroke = "#fff"></ion-spinner></h4>\n  <p class = "text_small_heading" *ngIf = "searchList.length > 0"> {{searchList.length}} results found. </p>\n\n  <ion-list *ngIf = "searchTextDisplay == false">\n\n      <div class = "" *ngIf = "showInternal">\n    <ion-item text-wrap class="item-text-wrap" *ngFor="let item of items" (click)="pushPage(item)">\n      <h2 class = "post_title" [innerHTML]="item.title"></h2>\n      <ion-note class="post-date"> <ion-icon name="calendar"></ion-icon> {{convertDate(item.date)}}</ion-note>\n    </ion-item>\n    </div>\n\n    <ion-item text-wrap class="item-text-wrap" *ngFor="let item of searchList" (click)="pushPage(item)">\n      <h2 class = "post_title" [innerHTML]="item.title"></h2>\n      <ion-note class="post-date"> <ion-icon name="calendar"></ion-icon> {{convertDate(item.date)}}</ion-note>\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/search/search.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */]])
], SearchPage);

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 731:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_post_post__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_staff_staff__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_auth_service_auth_service__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_google_analytics__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_badge__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_onesignal__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__config__ = __webpack_require__(126);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
















var MyApp = (function () {
    function MyApp(appRef, ngzone, platform, statusBar, splashScreen, oneSignal, ga, loadingCtrl, storage, staff, authService, alertCtrl, badge) {
        this.appRef = appRef;
        this.ngzone = ngzone;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.oneSignal = oneSignal;
        this.ga = ga;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.staff = staff;
        this.authService = authService;
        this.alertCtrl = alertCtrl;
        this.badge = badge;
        this.username = "";
        this.swapRoot = false;
        this.nowDate = new Date();
        this.ONE_HOUR = 60 * 60 * 1000; /* ms */
        this.debugMode = __WEBPACK_IMPORTED_MODULE_14__config__["b" /* debugMode */];
        this.articlesToBeRead = 0;
        this.isUpdateAvailable = false;
        this.new_version_link = null;
        this.initializeAppTest();
        //this.appVersion.getVersionNumber().then((s) => {
        //	this.version_code = s;
        //});    
        this.version_code = "5.3";
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'List', component: __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */] }
        ];
    }
    MyApp.prototype.initializeAppTest = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.storage.get("verified_number").then(function (val) {
                if (val) {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
                    _this.setupOneSignalNotification();
                    //this.getUserInformation();
                    //this.setUserLoginState();
                }
                else {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
                }
            });
            _this.checkForUpdate();
        });
    };
    MyApp.prototype.setupOneSignalNotification = function () {
        var _this = this;
        this.oneSignal.startInit(__WEBPACK_IMPORTED_MODULE_14__config__["c" /* oneSignalAppId */], __WEBPACK_IMPORTED_MODULE_14__config__["d" /* sender_id */]);
        //The inFocusDisplaying() method, controls how OneSignal notifications will be shown when one is received while your app is in focus. 
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        this.oneSignal.setSubscription(true);
        //The handleNotificationReceived() is a method that runs when a notification is received. It calls the onPushReceived()
        this.oneSignal.handleNotificationReceived().subscribe(function (data) { return _this.onPushReceived(data, data.payload); });
        // The handleNotificationOpened() is a method that runs when a notification is tapped or when closing an alert notification shown in the app.
        this.oneSignal.handleNotificationOpened().subscribe(function (data) { return _this.onPushOpened(data, data.notification.payload); });
        // Check to see if the user gave us permission
        var state = this.oneSignal.getPermissionSubscriptionState();
        //The endInit() method must be called to complete the initialization of OneSignal.
        this.oneSignal.endInit();
        // Get the User ID, this identifies the user. We'll use this ID and set the Player ID
        this.oneSignal.getIds().then(function (ids) {
            var playerID = ids.userId;
            //alert(playerID);
            _this.storage.set("playerID", playerID);
        });
        window["plugins"].OneSignal.clearOneSignalNotifications();
        this.badge.clear();
    };
    MyApp.prototype.getUserInformation = function () {
        var _this = this;
        try {
            this.storage.get('userinfo').then(function (result) {
                if (result) {
                    _this.username = result.firstname + " " + result.lastname;
                }
                else {
                    setTimeout(function () {
                        _this.storage.get('userinfo').then(function (result) {
                            if (result) {
                                _this.username = result.firstname + " " + result.lastname;
                            }
                        });
                    }, 120000);
                }
            });
        }
        catch (e) {
        }
    };
    // Code to handle the notifications
    MyApp.prototype.onPushReceived = function (data, payload) {
        //alert('Push recevied:' + payload.body);
        if (this.notHandlerCheck != true) {
            //alert("Running handleNotificationReceived from handleNotificationOpened ");
            this.notHandlerCheck = true;
            this.notificationHandling(data);
        }
    };
    MyApp.prototype.onPushOpened = function (data, payload) {
        //alert('Push opened: ' + payload.body);
        if (this.notHandlerCheck != true) {
            //alert("Running notificationHandling from handleNotificationOpened ");
            this.notHandlerCheck = true;
            this.notificationHandling(data);
        }
    };
    // Custom behavious to handle how the app should behave with a new notification
    MyApp.prototype.notificationHandling = function (data) {
        this.notHandlerCheck = false;
        if (this.debugMode == true) {
            this.storage.set('launchPost', data);
            if (data) {
                this.nav.push(__WEBPACK_IMPORTED_MODULE_6__pages_post_post__["a" /* PostPage */], {
                    item: data,
                    type: "notification"
                });
            }
        }
        else {
            var sid = data.notification.payload.additionalData.postID;
            data["sid"] = sid;
            this.storage.set('launchPost', data);
            if (data) {
                this.nav.push(__WEBPACK_IMPORTED_MODULE_6__pages_post_post__["a" /* PostPage */], {
                    item: data,
                    type: "notification"
                });
            }
        }
    };
    MyApp.prototype.showAlert = function (title, msg, task) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            title: title,
                            buttons: [
                                {
                                    text: 'Action: ${task}',
                                    handler: function () {
                                        // E.g: Navigate to a specific screen
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    MyApp.prototype.checkForUpdate = function () {
        var _this = this;
        this.staff.getUpdateInformation(this.version_code).subscribe(function (data) {
            console.log("checkForUpdate");
            console.log(data);
            console.log(data["link"]);
            if (data["link"] != null) {
                _this.isUpdateAvailable = true;
                _this.new_version_link = data["link"];
                var alert_1 = _this.alertCtrl.create({
                    title: "Portal Update",
                    message: "A new version of the Staff Portal is available.",
                    buttons: [
                        {
                            text: 'Download Update',
                            role: 'ok',
                            handler: function () {
                                window.open(data["link"], '_system', 'location=yes');
                            }
                        },
                    ]
                });
                alert_1.present();
            }
        });
    };
    MyApp.prototype.goToUpdate = function () {
        window.open(this.new_version_link, '_system', 'location=yes');
    };
    // Make a call to the server to identify how many articles have not being read
    // after which set a badge to alert the user.
    MyApp.prototype.checkArticlesToBeRead = function () {
        var _this = this;
        //Check if the user has ever logged on
        this.storage.get("staff_last_date").then(function (val) {
            if (val) {
                _this.staff.getMoreStaff(val).subscribe(function (data) {
                    //alert( JSON.stringify(data) );
                    _this.articlesToBeRead = _this.articlesToBeRead + data.length;
                    _this.setBadge(_this.articlesToBeRead);
                }, function (err) {
                    //alert("Error");
                    //alert(JSON.stringify(err));
                });
            }
            else {
                _this.staff.grabStaff().subscribe(function (data) {
                    //alert( JSON.stringify(data) );
                    _this.articlesToBeRead = _this.articlesToBeRead + data.length;
                    _this.setBadge(_this.articlesToBeRead);
                }, function (err) {
                    //alert("Error");
                    //alert(JSON.stringify(err));
                });
            }
        });
        // Get stories from the general category to be read
        this.storage.get("last_date").then(function (val) {
            if (val) {
                _this.staff.getMore(val).subscribe(function (data) {
                    //alert( JSON.stringify(data) );
                    _this.articlesToBeRead = _this.articlesToBeRead + data.length;
                    _this.setBadge(_this.articlesToBeRead);
                }, function (err) {
                    //alert("Error");
                    //alert(JSON.stringify(err));
                });
            }
            else {
                _this.staff.grabGeneral().subscribe(function (data) {
                    //alert( JSON.stringify(data) );
                    _this.articlesToBeRead = _this.articlesToBeRead + data.length;
                    _this.setBadge(_this.articlesToBeRead);
                }, function (err) {
                    //alert("Error");
                    //alert(JSON.stringify(err));
                });
            }
        });
    };
    MyApp.prototype.setBadge = function (amt) {
        var _this = this;
        this.badge.get().then(function (bcount) {
            _this.badge.decrease(bcount);
        });
        if (this.articlesToBeRead == 0) {
            //this.badge.set(amt);
            this.badge.increase(amt);
            //this.badge.set(1);
        }
        else {
            this.badge.increase(amt);
            //this.badge.set(1);
        }
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.oneSignal.handleNotificationReceived().subscribe(function (data) {
                //alert("handleNotificationReceived " + this.notHandlerCheck);
                if (_this.notHandlerCheck != true) {
                    //alert("Running handleNotificationReceived from handleNotificationOpened ");
                    _this.notHandlerCheck = true;
                    _this.notificationHandling(data);
                }
            });
            _this.oneSignal.handleNotificationOpened().subscribe(function (data) {
                //alert("handleNotificationOpened " + this.notHandlerCheck);
                if (_this.notHandlerCheck != true) {
                    //alert("Running notificationHandling from handleNotificationOpened ");
                    _this.notHandlerCheck = true;
                    _this.notificationHandling(data);
                }
            });
            // Get and Verify the staff members number, if verified then allow the
            // user to receive notifications
            // Always handle notifications and set them to launch post
            // then check the home page constantly checking
            _this.storage.get('verified_number').then(function (result) {
                if (result) {
                    /* If the number is verified */
                    _this.staff.verifyNumberAgain(result).subscribe(function (data) {
                        //alert("data (tel number for verify again)");
                        //alert( JSON.stringify(data) );
                        if (data["error"] != true) {
                            /* Update the last login */
                            _this.setUserLoginState();
                            /* Set time last verified */
                            _this.storage.set('last_verified', _this.nowDate);
                            /* Initialise the OneSignal Plugin */
                            _this.oneSignal.startInit('bc17d9b8-a757-418d-9e5f-1f369f5df780', '801139965353');
                            _this.oneSignal.inFocusDisplaying(_this.oneSignal.OSInFocusDisplayOption.InAppAlert);
                            var state = _this.oneSignal.getPermissionSubscriptionState();
                            _this.oneSignal.getIds().then(function (ids) {
                                var playerID = ids.userId;
                                _this.playerID = playerID;
                                _this.storage.set("playerID", playerID);
                            });
                            _this.oneSignal.endInit();
                            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
                        }
                        else {
                            // Unable to re verify the user again on the server
                            // Remove the user verified status
                            _this.storage.set('verified', false);
                            _this.storage.set('usertoken', null);
                            _this.oneSignal.setSubscription(false);
                            // Notify the user of the error
                            var alert_2 = _this.alertCtrl.create({
                                title: 'Oops!',
                                subTitle: "Access Error: " + data["message"],
                                buttons: ['OK']
                            });
                            alert_2.present();
                            // Relocate them to the login page
                            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
                        }
                    }, function (err) {
                        //alert("Set Page Error");	
                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
                    });
                }
                else {
                    //alert("Remove Subscription 1");
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
                    _this.oneSignal.setSubscription(false);
                }
            });
            //Google Analytics
            _this.ga.startTrackerWithId(__WEBPACK_IMPORTED_MODULE_14__config__["a" /* GoogleAnalyticsCode */])
                .then(function () {
                //console.log('Google analytics is ready now');
                _this.ga.trackView('test');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
                .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        });
    };
    /* Sets the user information in the sidebar and updates
    their last login on the server */
    MyApp.prototype.setUserLoginState = function () {
        var _this = this;
        this.storage.get('usertoken').then(function (result) {
            if (result) {
                _this.oneSignal.getIds().then(function (ids) {
                    var playerID = ids.userId;
                    _this.staff.updateLastLogin(playerID).subscribe(function (data) {
                        //alert( "Last Login Update Subscribe " + JSON.stringify( data ) );
                        //this.handleIntro();
                    }, function (error) {
                        //this.showError(error);
                    });
                });
            }
        });
        try {
            this.storage.get('userinfo').then(function (result) {
                //console.log( 'userinfo' );
                //console.log( result );
                //alert("User info");
                //alert(result);
                if (result) {
                    _this.username = result.firstname + " " + result.lastname;
                }
                else {
                    setTimeout(function () {
                        _this.storage.get('userinfo').then(function (result) {
                            if (result) {
                                _this.username = result.firstname + " " + result.lastname;
                            }
                        });
                    }, 120000);
                }
            });
        }
        catch (e) {
        }
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.callAlert = function (syncMessage) {
        var alert = this.alertCtrl.create({
            title: "App Update Status",
            subTitle: syncMessage,
            buttons: ['OK']
        });
        alert.present();
    };
    MyApp.prototype.presentLoadingDefault = function (message) {
        var loading = this.loadingCtrl.create({
            content: message
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 5000);
    };
    MyApp.prototype.logout = function () {
        this.storage.set('usertoken', null);
        this.storage.set('introImgShown', null);
        this.storage.set("verified_number", null);
        this.storage.set("articles", null);
        this.storage.set("staff_articles", null);
        this.storage.set("last_date", null);
        this.storage.set("staff_last_date", null);
        this.oneSignal.setSubscription(false);
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
    };
    MyApp.prototype.openPage2 = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page == "home") {
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
        }
        else if (page == "list") {
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */]);
        }
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/app/app.html"*/'<ion-menu [content]="content" type="overlay">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title class="color_blue">Staff Portal Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n\n    <ion-card class = "menucard" no-padding no-margin>\n\n      <ion-item>\n        <ion-avatar item-start>\n          <img src="assets/user.png">\n        </ion-avatar>\n        <h2>Welcome, </h2>\n        <p class = "text800">{{username}}</p>\n      </ion-item>\n\n    </ion-card>\n\n\n    <ion-list>\n      <!--<button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n\n      <button (click)="logout()"></button>-->\n      <ion-item (click)="openPage2(\'home\')" menuClose> <ion-icon class = "menu_icon" name="ios-home-outline"></ion-icon> Home </ion-item>\n      <ion-item (click)="openPage2(\'list\')" menuClose > <ion-icon class = "menu_icon" name="star-outline"></ion-icon> My List </ion-item>\n      <ion-item (click)="checkForUpdate()" menuClose> <ion-icon class = "menu_icon" name="ios-cloud-outline"></ion-icon> Check For Updates </ion-item>\n      \n      <ion-item (click)="goToUpdate()" *ngIf="isUpdateAvailable" menuClose> Install Update </ion-item>\n      <!--<ion-item (click)="openPage2(\'settings\')" menuClose> Settings </ion-item>-->\n\n      <ion-item (click)="logout()" menuClose > <ion-icon class = "menu_icon" name="log-out"></ion-icon> Sign Out </ion-item>\n\n    </ion-list>\n    <span class = "versionsec"> V <span [innerHTML] = "version_code"></span> <br/> {{playerID}} </span>\n    \n  </ion-content>\n\n</ion-menu>\n\n<div *ngIf = "isUpdateAvailable">\n    <h4 class = "boldme"> Application Update Available </h4>\n    <span [innerHTML] = "progressStatus"></span>\n</div>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/app/app.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_11__ionic_native_google_analytics__["a" /* GoogleAnalytics */], __WEBPACK_IMPORTED_MODULE_12__ionic_native_badge__["a" /* Badge */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ApplicationRef"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_13__ionic_native_onesignal__["a" /* OneSignal */], __WEBPACK_IMPORTED_MODULE_11__ionic_native_google_analytics__["a" /* GoogleAnalytics */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_10__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_8__providers_staff_staff__["a" /* StaffProvider */], __WEBPACK_IMPORTED_MODULE_9__providers_auth_service_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_12__ionic_native_badge__["a" /* Badge */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__post__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PostPageModule = (function () {
    function PostPageModule() {
    }
    return PostPageModule;
}());
PostPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__post__["a" /* PostPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__post__["a" /* PostPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__post__["a" /* PostPage */]
        ]
    })
], PostPageModule);

//# sourceMappingURL=post.module.js.map

/***/ }),

/***/ 733:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__post_post__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_staff_staff__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic2_super_tabs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var NewsPage = (function () {
    function NewsPage(badge, superTabsCtrl, http, loadingCtrl, storage, navCtrl, staff, toastCtrl, alertCtrl, platform) {
        this.badge = badge;
        this.superTabsCtrl = superTabsCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.staff = staff;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.articlesToLoad = 10;
        this.url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&per_page=' + this.articlesToLoad;
        this.staff_url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&per_page=' + this.articlesToLoad;
        this.loadingActive = false;
        this.articlesLoaded = 10;
        this.staff_articlesLoaded = 10;
        this.headingName = "Latest News";
        this.articleList = [];
        this.noarticlesorconnection = false;
        this.active_loading = false;
        // Load Post Information
        //this.loadWordpressInfo();
        this.updateUserSavedList();
    }
    // Flow Functions
    NewsPage.prototype.ionViewWillEnter = function () {
        //this.badge.clear();
        this.badge.set(0);
        this.debugLog("ionViewWillEnter Loaded");
        this.doRefresh(null);
        this.pollServer();
    };
    NewsPage.prototype.doRefresh = function (refresher) {
        //alert("Polling");
        if (this.active_loading == false) {
            this.mrefresher = refresher;
            this.active_loading = true;
            /* Once the Refresher Takes Over 8 Secs, we cancel the refresh */
            setTimeout(function () {
                if (refresher) {
                    if (refresher.state == "refreshing") {
                        refresher.cancel();
                    }
                }
            }, 10000);
            var loopNum = 0;
            var amount_to_load = this.articlesLoaded + this.articlesToLoad;
            if (amount_to_load > 30) {
                amount_to_load = 30;
            }
            this.url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&offset=0&per_page=' + amount_to_load;
            //alert( this.url );
            this.debugLog("doRefresh started");
            this.grabWordpressInfo("load");
        }
        else {
            //alert("Loading Occurring");
            try {
                refresher.cancel();
            }
            catch (e) {
            }
        }
    };
    /* Handles grabbing the information from the Wordpress API */
    NewsPage.prototype.grabWordpressInfo = function (loadType) {
        var _this = this;
        if (loadType == "reload") {
            this.presentLoading();
        }
        var url = this.url;
        if (loadType == "reload") {
            url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&per_page=' + this.articlesLoaded;
        }
        // Disable Errors
        this.noarticlesorconnection = false;
        this.http.get(url)
            .map(function (res) { return res.json(); })
            .map(function (data) { return _this.handleArticleSorting(data, loadType); })
            .subscribe(function (data) {
            if (loadType == "load") {
                if (_this.loader) {
                    _this.loader.dismiss();
                }
                _this.items = data;
                // Update Storage
                _this.setStorage("articles", data);
                _this.active_loading = false;
                _this.loadingActive = false;
                // Scan articles for saved selections and higlight them
                _this.checkArticlesSaved(data);
            }
            else {
                if (_this.loader) {
                    _this.loader.dismiss();
                }
                for (var i = 0; i < data.length; i++) {
                    _this.items.push(data[i]);
                }
                // Update Storage
                _this.setStorage("articles", _this.items);
                _this.loadingActive = false;
                if (_this.infin) {
                    _this.infin.complete();
                }
                _this.debugLog("Set Active Loading False");
                _this.active_loading = false;
                _this.loadingActive = false;
                _this.debugLog("active_loading " + _this.active_loading);
            }
            if (_this.mrefresher) {
                //console.log('Async operation has ended');
                _this.mrefresher.complete();
                _this.presentToast(' Refresh Complete! ');
            }
            _this.superTabsCtrl.clearBadge('news_tab');
        }, function (err) {
            _this.debugLog("grabWordpressStaffInfo error occured with loadType " + loadType);
            _this.handleGetWordpressInformationError(err, loadType);
        });
    };
    NewsPage.prototype.handleArticleSorting = function (data, loadType) {
        var posts = [];
        var index = 0;
        if (data.length > 0) {
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var post = data_1[_i];
                /* Set date to the date of the very first index post */
                if (index == 0 && loadType == "load") {
                    this.setLastDate(post['date']);
                }
                var featured_image = this.staff.getFeaturedImage(post);
                var category = this.staff.getCategory(post);
                //let savedState = this.checkIfSaved( post[ 'id' ] );
                var savedStatevar = -1;
                posts.push({ 'author': post['author'], 'id': post['id'], 'title': post['title']['rendered'], 'content': post['content']['rendered'], 'category': category["category"], 'date': post['date'], 'featured_media': post['featured_media'], "post_image": featured_image, "savedState": savedStatevar });
                index++;
                this.articlesLoaded++;
            }
        }
        else {
            this.presentToast(' No more post to be read ');
            this.loadingActive = false;
            if (this.infin) {
                this.infin.complete();
            }
        }
        return posts;
    };
    // Loop through the list of loaded articles and check the ones that
    // are saved in the users listing
    NewsPage.prototype.checkArticlesSaved = function (data) {
        var _this = this;
        var matchesFound = 0;
        this.storage.get("myarticles").then(function (val) {
            // Get the saved articles
            if (val) {
                _this.debugLog(" Loop through " + val.length + "articles in myarticles listings");
                // Loop through articles in myarticles listing
                for (var i = 0; i < val.length; i++) {
                    // Loop through newly loaded data with articles
                    for (var m = 0; m < data.length; m++) {
                        if (val[i]["article"]["id"] == data[m]["id"]) {
                            _this.debugLog(val[i]["article"]["id"] + " matches " + _this.items[m]["id"]);
                            data[m]["savedState"] = 1;
                            // Update matches found so we'll know to update the listing
                            matchesFound++;
                            _this.debugLog(" Update Match Found ");
                        }
                    }
                }
                // If matches found update listing
                if (matchesFound) {
                    _this.items = data;
                    _this.debugLog(" Update Staff Items List ");
                }
            }
        });
    };
    NewsPage.prototype.handleGetWordpressInformationError = function (err, loadType) {
        // Cancel infin staff loader
        if (this.infin) {
            this.infin.complete();
        }
        // Cancel staff loader
        if (this.loader) {
            this.loader.dismiss();
        }
        // Dismiss loader lock
        this.loadingActive = false;
        this.active_loading = false;
        this.debugLog("Revert to older articles");
        // Try to revert to older articles
        if (loadType != "none") {
            var items_temp = this.getStorage("articles");
            /*if(items_temp){
                this.items = items_temp;
            }else{
                this.noarticlesorconnection = true;
            }*/
        }
    };
    NewsPage.prototype.debugLog = function (msg) {
        console.log(msg);
    };
    /* Handles grabbing the information from the Wordpress API specifically for News Articles */
    NewsPage.prototype.loadWordpressInfo = function () {
        var _this = this;
        // Check to see if any articles are saved
        this.storage.get("articles").then(function (val) {
            if (val) {
                //alert("News Articles Found");
                // Get all articles and place into items variable to be displayed
                _this.items = val;
                // Set the length of the articles
                _this.articlesLoaded = _this.items.length;
                _this.doRefresh(null);
                // Get the last date from the last saved article
                _this.getLastDate();
                // Initiate Polling Server for More Updates
                _this.pollServer();
                //this.getMySavedArticles();
            }
            else {
                //alert("News No Articles Found");
                // Set articles loaded to 0
                _this.articlesLoaded = 0;
                // Grab Articles from server
                _this.grabWordpressInfo("load");
                // Initiate Polling Server for More Updates
                //this.pollServer();
            }
        });
    };
    NewsPage.prototype.updateUserSavedList = function () {
        var _this = this;
        /* Check for any items to be removed from the users save list */
        this.storage.get("articles").then(function (newsItemsTemp) {
            if (newsItemsTemp) {
                /* Check for any items to be removed */
                _this.storage.get("edited_articles").then(function (item) {
                    try {
                        if (item && item.length > 0) {
                            for (var m = 0; m < item.length; m++) {
                                for (var i = 0; i < newsItemsTemp.length; i++) {
                                    var articleID = item[m]["article"]["id"];
                                    if (newsItemsTemp[i]["id"] == articleID) {
                                        // Set Item to -1 to say not saved
                                        newsItemsTemp[i]["savedState"] = -1;
                                        // Remove the item found
                                        item.splice(m, 1);
                                        // Update Edited Articles so we know the items are removed
                                        _this.storage.set("edited_articles", item);
                                        // Update Staff Items in Storage
                                        _this.setStorage("articles", newsItemsTemp);
                                        _this.items = newsItemsTemp;
                                    }
                                }
                            }
                        }
                    }
                    catch (e) {
                        _this.debugLog(e);
                    }
                });
            }
        });
    };
    // Helper Functions
    NewsPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000
        });
        toast.present();
    };
    NewsPage.prototype.presentRemoveToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Article removed from list',
            duration: 3000
        });
        toast.present();
    };
    NewsPage.prototype.getCategories = function (item) {
        return JSON.stringify(item["_embedded"]["wp:term"][0]["name"]);
    };
    NewsPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        this.loader.present();
    };
    // Check the server every so often for new messages
    // Handle editing the articles in terms of articles removed / edited
    NewsPage.prototype.pollServer = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__["Observable"].interval(86400000).subscribe(function (x) {
            // Make sure the user if still authorised
            _this.staff.checkUserStatus();
            // Check in with the server to discover more articles
            //this.doRefresh(null);
            // Reload all articles
            // Fix for articles that may not have loaded with images,
            // and also this cleans up articles that were removed
            _this.grabWordpressInfo("reload");
        });
    };
    NewsPage.prototype.getLastDateFromList = function () {
        var ndate = this.items[0]["date"];
        this.lastDate = ndate;
        this.setLastDate(ndate);
    };
    NewsPage.prototype.checkCardSpecial = function (date) {
        return this.staff.checkCardSpecial(date, this.items);
    };
    NewsPage.prototype.showMoreArticlesAvailable = function (index) {
        var _this = this;
        this.moreArticlesAlert = true;
        setTimeout(function () {
            _this.moreArticlesAlert = false;
        }, 200);
    };
    // Handle opening news articles
    NewsPage.prototype.pushPage = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__post_post__["a" /* PostPage */], {
            item: item,
            type: "simple",
        });
    };
    // Load older articles
    NewsPage.prototype.doInfinite = function (infiniteScroll) {
        this.debugLog("Starting doInfinite");
        this.debugLog("Loading State: " + this.active_loading);
        // Check if any other loading is occuring
        if (this.active_loading == false) {
            // Show loading icon
            this.loadingActive = true;
            // If no loading, lock loading to prevent clashing with other loaders
            // activated by the user
            this.active_loading = true;
            if (this.articlesLoaded != this.items.length) {
                this.articlesLoaded = this.items.length;
            }
            this.url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&offset=' + this.articlesLoaded + '&per_page=' + this.articlesToLoad;
            this.grabWordpressInfo("none");
            this.infin = infiniteScroll;
        }
        else {
            //alert("Loading Occuring do infinite");
        }
    };
    // Loop through the available article and select the correct saved articles
    NewsPage.prototype.checkIfSaved = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get("myarticles").then(function (articleList) {
                if (articleList) {
                    if (articleList.length > 0) {
                        for (var i = 0; i < articleList.length; i++) {
                            //alert( articleList[i]["id"] + " " + articleList[i]["article"]["id"] );
                            if (id == articleList[i]["article"]["id"]) {
                                //alert("Match");
                                resolve(true);
                            }
                        }
                        reject(false);
                    }
                    else {
                        reject(false);
                    }
                }
            });
        });
    };
    NewsPage.prototype.saveArticle = function (item, event) {
        var _this = this;
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i]["id"] == item.id) {
                this.items[i]["savedState"] = true;
                this.storage.set("articles", this.items);
            }
        }
        // set a key/value
        this.storage.get("myarticles").then(function (val) {
            var article_double = false;
            if (val) {
                for (var i = 0; i < val.length; i++) {
                    if (val[i]["article"]["id"] == item.id) {
                        //alert(" Article Already Added");
                        article_double = true;
                    }
                }
                if (article_double == false) {
                    val.push({ "article": item });
                    _this.storage.set("myarticles", val);
                }
            }
            else {
                var data = [];
                data.push({ "article": item });
                _this.storage.set("myarticles", data);
            }
            try {
                event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star color_blue";
            }
            catch (e) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Oops!',
                    subTitle: "Unable to save " + e,
                    buttons: ['OK']
                });
                alert_1.present();
            }
            _this.presentToast('Article saved to list');
            //return name;
        });
    };
    NewsPage.prototype.removeArticle = function (item, event, index) {
        var _this = this;
        //console.log(event);
        var savedFound = false;
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i]["id"] == item.id) {
                item["savedState"] = -1;
                this.debugLog("Found");
            }
        }
        // set a key/value
        this.storage.get("myarticles").then(function (val) {
            if (val) {
                for (var i = 0; i < val.length; i++) {
                    if (val[i]["article"]["id"] == item.id) {
                        var mindex = val.indexOf(val[i]);
                        //alert("Val I Index " + mindex )
                        var removed = val.splice(mindex, 1);
                        //alert("Removed " + JSON.stringify(removed) )
                        _this.storage.set("myarticles", val);
                        //alert( JSON.stringify(val) );
                        _this.presentRemoveToast();
                        return name;
                    }
                }
            }
        });
        event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star-outline";
    };
    /* Convert Date to proper human readable date */
    NewsPage.prototype.convertDate = function (date) {
        if (date != "0000-00-00 00:00:00") {
            return __WEBPACK_IMPORTED_MODULE_8_moment___default()(date).format('LL');
            //return this.datePipe.transform(date, 'fullDate');
        }
    };
    NewsPage.prototype.setStorage = function (name, data) {
        // set a key/value
        this.storage.set(name, data);
    };
    NewsPage.prototype.getStorage = function (name) {
        // set a key/value
        this.storage.get(name).then(function (val) {
            //console.log('Requested Storage Data is', val);
            return name;
        });
    };
    NewsPage.prototype.setLastDate = function (date) {
        this.lastDate = date;
        this.storage.set("last_date", date);
    };
    NewsPage.prototype.getLastDate = function () {
        var _this = this;
        this.storage.get("last_date").then(function (val) {
            _this.lastDate = val;
            if (_this.lastDate) {
            }
            else {
                _this.lastDate = _this.items[0]["date"];
            }
        });
    };
    return NewsPage;
}());
NewsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-news',template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/categories/news/news.html"*/'<ion-content>\n	\n	<ion-refresher (ionRefresh)="doRefresh($event)">\n		<ion-refresher-content></ion-refresher-content>\n	</ion-refresher>\n	\n    <ion-grid no-padding>\n		\n		<h1 class = "specialH1"> {{headingName}} </h1>\n		\n		<div *ngFor="let item of items; let i = index" no-padding no-margin>\n	  \n		<div class = "card-background-page card-special" *ngIf = "i == 0" (click)="pushPage(item)">\n			<ion-card no-padding no-margin class = "fullwidth">\n				<img src="{{item.post_image}}"/>\n				<div class="card-title" [innerHTML]="item.title"></div>\n			</ion-card>\n			\n		</div>\n		\n		<ion-row ngClass = "article_preview_wrap {{ checkCardSpecial(item.date) }}" *ngIf = "i > 0">\n        \n		<ion-col (click)="pushPage(item)" *ngIf = "item.post_image" col-4 no-padding class="post-image" [ngStyle]="{\'background-image\': \'url(\' + item.post_image + \')\' }">\n\n        </ion-col>\n\n        <ion-col *ngIf = "item.post_image" (click)="pushPage(item)" col-7 padding class="item-text-wrap">\n          <div class = "postCategory" [innerHTML] = "item.category"></div>\n          <h2 class = "post_title" [innerHTML]="item.title"></h2>\n		  \n          <ion-note class="post-date"> <ion-icon name="calendar"></ion-icon> {{convertDate(item.date)}}</ion-note>\n        </ion-col>\n\n        <ion-col *ngIf = "!item.post_image" (click)="pushPage(item)" col-11 padding class="item-text-wrap">\n          <div class = "postCategory" [innerHTML] = "item.category"></div>\n          <h2 class = "post_title" [innerHTML]="item.title"></h2>\n		  \n          <ion-note class="post-date"> <ion-icon name="calendar"></ion-icon> {{convertDate(item.date)}}</ion-note>\n        </ion-col>\n\n        <ion-col col-1>\n		  \n          <ion-icon *ngIf = "item.savedState == -1" (click) = "saveArticle(item, $event)" name="star-outline" class = "saveArticleIcon ext-right"></ion-icon>\n		  \n		  <ion-icon *ngIf = "item.savedState != -1" (click) = "removeArticle(item, $event, item.savedState)" name="star" class = "saveArticleIcon color_blue ext-right"></ion-icon> \n		  \n        </ion-col>\n\n      </ion-row>\n      </div>\n\n\n      <div class = "groupLoader" *ngIf="loadingActive == true" >\n\n        <ion-row no-padding class = "article_preview_wrap">\n          <ion-col col-4 no-padding class="post-image"> <span class="load_fill"></span> </ion-col>\n\n          <ion-col col-7 padding class="item-text-wrap">\n            <div class = "postCategory"> <span class="load_fill load_fill_category"></span> </div>\n            <h2 class = "post_title"> <span class="load_fill"></span> <span class="load_fill"></span> </h2>\n            <ion-note class="post-date"> <span class="load_fill"></span> </ion-note>\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row no-padding class = "article_preview_wrap">\n          <ion-col col-4 no-padding class="post-image"> <span class="load_fill"></span> </ion-col>\n\n          <ion-col col-7 padding class="item-text-wrap">\n            <div class = "postCategory"> <span class="load_fill load_fill_category"></span> </div>\n            <h2 class = "post_title"> <span class="load_fill"></span> <span class="load_fill"></span> </h2>\n            <ion-note class="post-date"> <span class="load_fill"></span> </ion-note>\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row no-padding class = "article_preview_wrap">\n          <ion-col col-4 no-padding class="post-image"> <span class="load_fill"></span> </ion-col>\n\n          <ion-col col-7 padding class="item-text-wrap">\n            <div class = "postCategory"> <span class="load_fill load_fill_category"></span> </div>\n            <h2 class = "post_title"> <span class="load_fill"></span> <span class="load_fill"></span> </h2>\n            <ion-note class="post-date"> <span class="load_fill"></span> </ion-note>\n          </ion-col>\n\n        </ion-row>\n\n      </div>\n\n\n		<ion-infinite-scroll (ionInfinite)="doInfinite($event)" threshold="35%">\n		<ion-infinite-scroll-content></ion-infinite-scroll-content>\n		</ion-infinite-scroll>\n\n    </ion-grid>\n	\n	<div *ngIf = "moreArticlesAlert" class = "articlesMoreHover"> New Articles Available </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/categories/news/news.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__["a" /* Badge */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__["a" /* Badge */], __WEBPACK_IMPORTED_MODULE_9_ionic2_super_tabs__["a" /* SuperTabsController */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_staff_staff__["a" /* StaffProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["u" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["r" /* Platform */]])
], NewsPage);

//# sourceMappingURL=news.js.map

/***/ }),

/***/ 734:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StaffPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__post_post__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_staff_staff__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic2_super_tabs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var StaffPage = (function () {
    function StaffPage(badge, ngzone, superTabsCtrl, http, loadingCtrl, storage, navCtrl, staff, toastCtrl, alertCtrl, platform) {
        this.badge = badge;
        this.ngzone = ngzone;
        this.superTabsCtrl = superTabsCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.staff = staff;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.articlesToLoad = 10;
        this.url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&per_page=' + this.articlesToLoad;
        this.staff_url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&per_page=' + this.articlesToLoad;
        this.staffloadingActive = false;
        this.active_loading = false;
        this.staff_articlesLoaded = 10;
        this.headingName = "Latest News";
        this.articleList = [];
        this.enable_reload = true;
        this.enable_loadmore = true;
        this.noarticlesorconnection = false;
        // Load Post Information
        this.updateUserSavedList();
    }
    // Flow Functions
    StaffPage.prototype.ionViewWillEnter = function () {
        this.badge.clear();
        //this.badge.set(0);
        this.debugLog("ionViewWillEnter Loaded");
        this.doRefresh(null);
        this.pollServer();
    };
    // Handle loading new content using the refresh
    StaffPage.prototype.doRefresh = function (refresher) {
        this.debugLog("doRefresh Loaded");
        if (this.active_loading == false) {
            this.mrefresher = refresher;
            this.active_loading = true;
            // Set a watcher for articles not being able to load
            this.setRefreshTracker(refresher);
            var amount_to_load = 0;
            amount_to_load = this.staff_articlesLoaded + this.articlesToLoad;
            if (amount_to_load > 30) {
                amount_to_load = 30;
            }
            this.staff_url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&offset=0&per_page=' + amount_to_load;
            this.debugLog("Set URL " + this.staff_url);
            this.grabWordpressStaffInfo("load");
        }
        else {
            //alert("Loading Occurring");
            if (refresher) {
                refresher.cancel();
            }
        }
    };
    StaffPage.prototype.setRefreshTracker = function (refresher) {
        var _this = this;
        /* Once the Refresher Takes Over 8 Secs, we cancel the refresh */
        setTimeout(function () {
            if (refresher) {
                if (refresher.state == "refreshing") {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Refresh',
                        message: 'Articles taking a while to load, would you like to continue loading?',
                        buttons: [
                            {
                                text: 'Cancel',
                                role: 'cancel',
                                handler: function () {
                                    refresher.cancel();
                                }
                            },
                            {
                                text: 'Continue',
                                handler: function () {
                                    _this.setRefreshTracker(refresher);
                                }
                            }
                        ]
                    });
                    alert_1.present();
                }
            }
        }, 20000);
    };
    /* Handles grabbing the information from the Wordpress API specifically for Staff Articles */
    StaffPage.prototype.grabWordpressStaffInfo = function (loadType) {
        var _this = this;
        this.debugLog("grabWordpressStaffInfo started with loadType " + loadType);
        // Activate Loader
        this.staffloadingActive = true;
        // Disable Errors
        this.noarticlesorconnection = false;
        var url = this.staff_url;
        if (loadType == "reload") {
            url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&per_page=' + this.staff_articlesLoaded;
        }
        this.http.get(url)
            .map(function (res) { return res.json(); })
            .map(function (data) { return _this.handleArticleSorting(data, loadType); })
            .subscribe(function (data) {
            // Debugging Information
            _this.debugLog(" Subscribe Data Successful for load type " + loadType);
            _this.debugLog(" Data Length " + data.length);
            // Show Loader
            _this.staffloadingActive = true;
            if (loadType == "load") {
                _this.handleLoad(data);
            }
            else {
                // Used for reload and none loadTypes
                _this.handleAddition(data);
            }
            // Scan articles for saved selections and higlight them
            _this.checkArticlesSaved(data);
            if (_this.staff_loader) {
                _this.staff_loader.dismiss();
            }
            if (_this.mrefresher) {
                _this.mrefresher.complete();
                _this.presentToast(' Refresh Complete! ');
            }
            // Reset Badge if set
            _this.superTabsCtrl.clearBadge('staff_tab');
        }, function (err) {
            _this.debugLog("grabWordpressStaffInfo error occured with loadType " + loadType);
            _this.handleGetWordpressInformationError(err);
        });
    };
    // 
    StaffPage.prototype.handleArticleSorting = function (data, loadType) {
        var posts = [];
        var index = 0;
        if (data.length > 0) {
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var post = data_1[_i];
                /* Set date to the date of the very first index post */
                if (index == 0 && loadType == "load") {
                    this.setLastDate(post['date']);
                }
                var featured_image = this.staff.getFeaturedImage(post);
                var category = this.staff.getCategory(post);
                //let savedState = this.checkIfSaved( post[ 'id' ] );
                var savedStatevar = -1;
                posts.push({ 'author': post['author'], 'id': post['id'], 'title': post['title']['rendered'], 'content': post['content']['rendered'], 'category': category["category"], 'date': post['date'], 'featured_media': post['featured_media'], "post_image": featured_image, "savedState": savedStatevar });
                index++;
                this.staff_articlesLoaded++;
            }
            //this.getLastStaffDate();
            return posts;
        }
        else {
            this.debugLog(" No Post Available ");
            this.presentToast(' No more post to be read ');
            this.staffloadingActive = false;
            this.active_loading = false;
            if (this.infin_staff) {
                this.infin_staff.complete();
            }
            return posts;
        }
    };
    StaffPage.prototype.handleGetWordpressInformationError = function (err) {
        // Cancel infin staff loader
        if (this.infin_staff) {
            this.infin_staff.complete();
        }
        // Cancel staff loader
        if (this.staff_loader) {
            this.staff_loader.dismiss();
        }
        // Dismiss loader lock
        this.staffloadingActive = false;
        this.active_loading = false;
        this.debugLog("Revert to older articles");
        // Try to revert to older articles
        var staff_items_temp = this.getStorage("staff_articles");
        /*if(staff_items_temp){
            this.staff_items = staff_items_temp;
        }else{
            this.noarticlesorconnection = true;
        }*/
    };
    // Handles setting data to staff items
    StaffPage.prototype.handleLoad = function (data) {
        this.debugLog("handleLoad started for " + data.length);
        this.staff_items = data;
        this.active_loading = false;
        this.staffloadingActive = false;
    };
    StaffPage.prototype.handleAddition = function (data) {
        this.debugLog("handleAddition started for " + data.length);
        // Loop through all new data items
        for (var i = 0; i < data.length; i++) {
            this.staff_items.push(data[i]);
        }
        // Remove staff loader lock
        this.staffloadingActive = false;
        // Set loading type back to false
        this.active_loading = false;
        // Complete Loader icon
        if (this.infin_staff) {
            this.infin_staff.complete();
        }
    };
    StaffPage.prototype.debugLog = function (msg) {
        console.log(msg);
    };
    // Goes throught the entire selection of loaded articles and updates
    // removed saved articles
    StaffPage.prototype.updateUserSavedList = function () {
        var _this = this;
        this.storage.get("staff_articles").then(function (staffItemsTemp) {
            if (staffItemsTemp) {
                /* Check for any items to be removed */
                _this.storage.get("edited_articles").then(function (item) {
                    try {
                        if (item && item.length > 0) {
                            for (var m = 0; m < item.length; m++) {
                                for (var i = 0; i < staffItemsTemp.length; i++) {
                                    var articleID = item[m]["article"]["id"];
                                    if (staffItemsTemp[i]["id"] == articleID) {
                                        // Set Item to -1 to say not saved
                                        staffItemsTemp[i]["savedState"] = -1;
                                        // Remove the item found
                                        item.splice(m, 1);
                                        // Update Edited Articles so we know the items are removed
                                        _this.storage.set("edited_articles", item);
                                        // Update Staff Items in Storage
                                        _this.setStorage("staff_articles", staffItemsTemp);
                                        _this.staff_items = staffItemsTemp;
                                    }
                                }
                            }
                        }
                    }
                    catch (e) {
                        //console.log(e);
                    }
                });
            }
        });
    };
    // Helper Functions
    StaffPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000
        });
        toast.present();
    };
    StaffPage.prototype.presentRemoveToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Article removed from list',
            duration: 3000
        });
        toast.present();
    };
    StaffPage.prototype.getCategories = function (item) {
        return JSON.stringify(item["_embedded"]["wp:term"][0]["name"]);
    };
    StaffPage.prototype.staffPresentLoading = function () {
        this.staff_loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        this.staff_loader.present();
    };
    // Check the server every so often for new messages
    StaffPage.prototype.pollServer = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__["Observable"].interval(86400000).subscribe(function (x) {
            if (_this.lastStaffDate == null) {
                _this.getLastDate();
            }
            _this.staff.checkUserStatus();
            _this.doRefresh(null);
        });
    };
    // Loop through the list of loaded articles and check the ones that
    // are saved in the users listing
    StaffPage.prototype.checkArticlesSaved = function (data) {
        var _this = this;
        var matchesFound = 0;
        this.storage.get("myarticles").then(function (val) {
            // Get the saved articles
            if (val) {
                _this.debugLog(" Loop through " + val.length + "articles in myarticles listings");
                // Loop through articles in myarticles listing
                for (var i = 0; i < val.length; i++) {
                    // Loop through newly loaded data with articles
                    for (var m = 0; m < data.length; m++) {
                        if (val[i]["article"]["id"] == data[m]["id"]) {
                            _this.debugLog(val[i]["article"]["id"] + " matches " + _this.staff_items[m]["id"]);
                            data[m]["savedState"] = 1;
                            // Update matches found so we'll know to update the listing
                            matchesFound++;
                            _this.debugLog(" Update Match Found ");
                        }
                    }
                }
                // If matches found update listing
                if (matchesFound) {
                    _this.staff_items = data;
                    _this.debugLog(" Update Staff Items List ");
                }
            }
        });
    };
    StaffPage.prototype.checkIfSaved = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get("myarticles").then(function (articleList) {
                if (articleList) {
                    if (articleList.length > 0) {
                        for (var i = 0; i < articleList.length; i++) {
                            //alert( articleList[i]["id"] + " " + articleList[i]["article"]["id"] );
                            if (id == articleList[i]["article"]["id"]) {
                                //alert("Match");
                                resolve(true);
                            }
                        }
                        reject(false);
                    }
                    else {
                        reject(false);
                    }
                }
            });
        });
    };
    // Handle opening news articles
    StaffPage.prototype.pushPage = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__post_post__["a" /* PostPage */], {
            item: item,
            type: "simple",
        });
    };
    StaffPage.prototype.doInfiniteStaff = function (infiniteScroll) {
        this.debugLog(" doInfiniteStaff function started ");
        if (this.active_loading == false) {
            this.staffloadingActive = true;
            this.active_loading = true;
            this.debugLog("Staff Articles Loaded " + this.staff_articlesLoaded);
            this.debugLog("Staff Items Loaded " + this.staff_items.length);
            if (this.staff_articlesLoaded != this.staff_items.length) {
                this.staff_articlesLoaded = this.staff_items.length;
            }
            this.staff_url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&offset=' + this.staff_articlesLoaded + '&per_page=' + this.articlesToLoad;
            this.debugLog(" Set URL to " + this.staff_url);
            this.debugLog(" Call to grabWordpressStaffInfo, loadType none ");
            this.grabWordpressStaffInfo("none");
            this.infin_staff = infiniteScroll;
        }
        else {
            this.debugLog("Unable to load, another loading process is occuring");
        }
    };
    StaffPage.prototype.saveArticle = function (item, event) {
        var _this = this;
        //alert("clicked");
        //console.log(event);
        var savedFound = false;
        for (var i = 0; i < this.staff_items.length; i++) {
            if (this.staff_items[i]["id"] == item.id) {
                this.staff_items[i]["savedState"] = true;
                //console.log("Found");
                this.storage.set("staff_articles", this.staff_items);
            }
        }
        // set a key/value
        this.storage.get("myarticles").then(function (val) {
            if (val) {
                val.push({ "article": item });
                _this.storage.set("myarticles", val);
            }
            else {
                var data = [];
                data.push({ "article": item });
                _this.storage.set("myarticles", data);
            }
            try {
                event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star color_blue";
            }
            catch (e) {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Oops!',
                    subTitle: "Unable to save " + e,
                    buttons: ['OK']
                });
                alert_2.present();
            }
            _this.presentToast('Article saved to list');
            //return name;
        });
    };
    StaffPage.prototype.removeArticle = function (item, event, index) {
        var _this = this;
        for (var i = 0; i < this.staff_items.length; i++) {
            if (this.staff_items[i]["id"] == item.id) {
                this.staff_items[i]["savedState"] = -1;
                this.setStorage("staff_articles", this.staff_items);
            }
        }
        event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star-outline";
        // set a key/value
        this.storage.get("myarticles").then(function (val) {
            if (val) {
                for (var i = 0; i < val.length; i++) {
                    if (val[i]["article"]["id"] == item.id) {
                        var mindex = val.indexOf(val[i]);
                        //alert("Val I Index " + mindex )
                        var removed = val.splice(mindex, 1);
                        //alert("Removed " + JSON.stringify(removed) )
                        _this.storage.set("myarticles", val);
                        //alert( JSON.stringify(val) );
                        _this.presentRemoveToast();
                        return name;
                    }
                }
            }
        });
    };
    /* Convert Date to proper human readable date */
    StaffPage.prototype.convertDate = function (date) {
        if (date != "0000-00-00 00:00:00") {
            return __WEBPACK_IMPORTED_MODULE_8_moment___default()(date).format('LL');
            //return this.datePipe.transform(date, 'fullDate');
        }
    };
    StaffPage.prototype.setStorage = function (name, data) {
        this.storage.set(name, data);
    };
    StaffPage.prototype.getStorage = function (name) {
        // set a key/value
        this.storage.get(name).then(function (val) {
            return name;
        });
    };
    StaffPage.prototype.checkCardSpecial = function (date) {
        return this.staff.checkCardSpecial(date, this.staff_items);
    };
    StaffPage.prototype.showMoreArticlesAvailable = function (index) {
        var _this = this;
        this.moreArticlesAlert = true;
        setTimeout(function () {
            _this.moreArticlesAlert = false;
        }, 200);
    };
    StaffPage.prototype.setLastDate = function (date) {
        this.lastStaffDate = date;
        this.storage.set("staff_last_date", date);
    };
    StaffPage.prototype.getLastDate = function () {
        var _this = this;
        this.storage.get("staff_last_date").then(function (val) {
            _this.lastStaffDate = val;
            if (_this.lastStaffDate) {
            }
            else {
                _this.lastStaffDate = _this.staff_items[0]["date"];
            }
        });
    };
    return StaffPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* Nav */])
], StaffPage.prototype, "nav", void 0);
StaffPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-staff',template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/categories/staff/staff.html"*/'<ion-content>\n	\n	<ion-refresher (ionRefresh)="doRefresh($event)">\n		<ion-refresher-content></ion-refresher-content>\n	</ion-refresher>\n  \n    <ion-grid no-padding>\n	\n		<h1 class = "specialH1"> {{headingName}} </h1>\n		\n		<div *ngFor="let item of staff_items; let i = index" no-padding no-margin>\n		\n			<div class = "card-background-page card-special" no-padding *ngIf = "i == 0" (click)="pushPage(item)">\n				<ion-card no-padding no-margin class = "fullwidth">\n					<img src="{{item.post_image}}"/>\n					<div class="card-title" [innerHTML]="item.title"></div>\n				</ion-card>\n				\n			</div>\n			\n      <ion-row ngClass = "article_preview_wrap {{ checkCardSpecial(item.date) }}" *ngIf = "i > 0">\n        \n		<ion-col (click)="pushPage(item)" *ngIf = "item.post_image" col-4 no-padding class="post-image" [ngStyle]="{\'background-image\': \'url(\' + item.post_image + \')\' }">\n\n        </ion-col>\n\n        <ion-col *ngIf = "item.post_image" (click)="pushPage(item)" col-7 padding class="item-text-wrap">\n          <h2 class = "post_title" [innerHTML]="item.title"></h2>\n          <ion-note class="post-date"> <ion-icon name="calendar"></ion-icon> {{convertDate(item.date)}}</ion-note>\n		  \n        </ion-col>\n\n        <ion-col *ngIf = "!item.post_image" (click)="pushPage(item)" col-11 padding class="item-text-wrap">\n          <div class = "postCategory" [innerHTML] = "item.category"></div>\n          <h2 class = "post_title" [innerHTML]="item.title"></h2>\n		  \n			<ion-note class="post-date"> <ion-icon name="calendar"></ion-icon> {{convertDate(item.date)}}	</ion-note>\n			</ion-col>\n\n        <ion-col col-1>\n          <ion-icon *ngIf = "item.savedState == -1" (click) = "saveArticle(item, $event)" name="star-outline" class = "saveArticleIcon ext-right"></ion-icon>\n		  \n		  <ion-icon *ngIf = "item.savedState != -1" (click) = "removeArticle(item, $event, item.savedState)" name="star" class = "saveArticleIcon color_blue ext-right"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n      </div>\n\n		\n	  <div class = "groupLoader" *ngIf="staffloadingActive == true" >\n\n        <ion-row no-padding class = "article_preview_wrap">\n          <ion-col col-4 no-padding class="post-image"> <span class="load_fill"></span> </ion-col>\n\n          <ion-col col-7 padding class="item-text-wrap">\n            <div class = "postCategory"> <span class="load_fill load_fill_category"></span> </div>\n            <h2 class = "post_title"> <span class="load_fill"></span> <span class="load_fill"></span> </h2>\n            <ion-note class="post-date"> <span class="load_fill"></span> </ion-note>\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row no-padding class = "article_preview_wrap">\n          <ion-col col-4 no-padding class="post-image"> <span class="load_fill"></span> </ion-col>\n\n          <ion-col col-7 padding class="item-text-wrap">\n            <div class = "postCategory"> <span class="load_fill load_fill_category"></span> </div>\n            <h2 class = "post_title"> <span class="load_fill"></span> <span class="load_fill"></span> </h2>\n            <ion-note class="post-date"> <span class="load_fill"></span> </ion-note>\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row no-padding class = "article_preview_wrap">\n          <ion-col col-4 no-padding class="post-image"> <span class="load_fill"></span> </ion-col>\n\n          <ion-col col-7 padding class="item-text-wrap">\n            <div class = "postCategory"> <span class="load_fill load_fill_category"></span> </div>\n            <h2 class = "post_title"> <span class="load_fill"></span> <span class="load_fill"></span> </h2>\n            <ion-note class="post-date"> <span class="load_fill"></span> </ion-note>\n          </ion-col>\n\n        </ion-row>\n\n      </div>\n\n\n      <ion-infinite-scroll (ionInfinite)="doInfiniteStaff($event)" threshold="35%">\n       <ion-infinite-scroll-content></ion-infinite-scroll-content>\n     </ion-infinite-scroll> \n\n    </ion-grid>\n	\n	<div *ngIf = "moreArticlesAlert" class = "articlesMoreHover"> New Staff Articles Available </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/categories/staff/staff.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__["a" /* Badge */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__ionic_native_badge__["a" /* Badge */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_9_ionic2_super_tabs__["a" /* SuperTabsController */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_staff_staff__["a" /* StaffProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["u" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["r" /* Platform */]])
], StaffPage);

//# sourceMappingURL=staff.js.map

/***/ }),

/***/ 735:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_badge__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_post_post__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SettingsPage = (function () {
    function SettingsPage(ga, badge, platform, navCtrl, navParams, storage, oneSignal) {
        this.ga = ga;
        this.badge = badge;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.oneSignal = oneSignal;
        this.appbuttontext = "Skip To App";
        this.pagerOption = true;
        this.debugMode = false;
        this.initializeApp();
    }
    SettingsPage.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.setupOneSignalNotification();
            _this.setupGoogleAnalytics();
        });
    };
    SettingsPage.prototype.setupOneSignalNotification = function () {
        var _this = this;
        this.oneSignal.startInit(__WEBPACK_IMPORTED_MODULE_7__config__["c" /* oneSignalAppId */], __WEBPACK_IMPORTED_MODULE_7__config__["d" /* sender_id */]);
        //The inFocusDisplaying() method, controls how OneSignal notifications will be shown when one is received while your app is in focus. 
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        //The handleNotificationReceived() is a method that runs when a notification is received. It calls the onPushReceived()
        this.oneSignal.handleNotificationReceived().subscribe(function (data) { return _this.onPushReceived(data, data.payload); });
        // The handleNotificationOpened() is a method that runs when a notification is tapped or when closing an alert notification shown in the app.
        this.oneSignal.handleNotificationOpened().subscribe(function (data) { return _this.onPushOpened(data, data.notification.payload); });
        // Check to see if the user gave us permission
        var state = this.oneSignal.getPermissionSubscriptionState();
        //The endInit() method must be called to complete the initialization of OneSignal.
        this.oneSignal.endInit();
        // Get the User ID, this identifies the user. We'll use this ID and set the Player ID
        this.oneSignal.getIds().then(function (ids) {
            var playerID = ids.userId;
            alert(playerID);
            _this.storage.set("playerID", playerID);
        });
    };
    SettingsPage.prototype.setupGoogleAnalytics = function () {
        var _this = this;
        //Google Analytics
        this.ga.startTrackerWithId('UA-104903092-1')
            .then(function () {
            //console.log('Google analytics is ready now');
            _this.ga.trackView('test');
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
    };
    // Code to handle the notifications
    SettingsPage.prototype.onPushReceived = function (data, payload) {
        alert('Push recevied:' + payload.body);
        if (this.notHandlerCheck != true) {
            //alert("Running handleNotificationReceived from handleNotificationOpened ");
            this.notHandlerCheck = true;
            this.notificationHandling(data);
        }
    };
    SettingsPage.prototype.onPushOpened = function (data, payload) {
        alert('Push opened: ' + payload.body);
        if (this.notHandlerCheck != true) {
            //alert("Running notificationHandling from handleNotificationOpened ");
            this.notHandlerCheck = true;
            this.notificationHandling(data);
        }
    };
    // Custom behavious to handle how the app should behave with a new notification
    SettingsPage.prototype.notificationHandling = function (data) {
        this.notHandlerCheck = false;
        if (this.debugMode == true) {
            this.storage.set('launchPost', data);
            if (data) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_post_post__["a" /* PostPage */], {
                    item: data,
                    type: "notification"
                });
            }
        }
        else {
            var sid = data.notification.payload.additionalData.postID;
            data["sid"] = sid;
            this.storage.set('launchPost', data);
            if (data) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_post_post__["a" /* PostPage */], {
                    item: data,
                    type: "notification"
                });
            }
        }
    };
    SettingsPage.prototype.provideAppWithData = function () {
        var _this = this;
        this.oneSignal.getIds().then(function (ids) {
            _this.playerID = ids.userId;
            _this.storage.set("playerID", _this.playerID);
        });
    };
    SettingsPage.prototype.skipIntro = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    SettingsPage.prototype.slideChanged = function () {
        var currentIndex = this.slides.getActiveIndex();
        this.pagerOption = true;
        if (currentIndex >= 2) {
            this.appbuttontext = "Start App!";
            this.pagerOption = false;
        }
        else {
            this.appbuttontext = "Skip Intro";
        }
    };
    return SettingsPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* Slides */])
], SettingsPage.prototype, "slides", void 0);
SettingsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-settings',template:/*ion-inline-start:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/settings/settings.html"*/'<ion-header class = "postHeader">\n  \n  <ion-navbar>\n    <ion-title>Settings</ion-title>\n  </ion-navbar>\n  \n  </ion-header>\n  \n  <ion-content padding>\n  \n    \n    <ion-item>\n      <ion-label>Enabled Push Notifications</ion-label>\n      <ion-checkbox color="dark" checked="true"></ion-checkbox>\n    </ion-item>\n\n    \n    <ion-list radio-group>\n\n      <ion-list-header>\n        Theme Settings\n      </ion-list-header>\n    \n      <ion-item>\n        <ion-label>Light Theme</ion-label>\n        <ion-radio checked="true" value="light"></ion-radio>\n      </ion-item>\n    \n      <ion-item>\n        <ion-label>Dark Theme</ion-label>\n        <ion-radio value="rust" value="dark"></ion-radio>\n      </ion-item>\n    \n    </ion-list>\n  \n\n  </ion-content>'/*ion-inline-end:"/Users/romariorenee/Business/Business/Apps/portal_android/src/pages/settings/settings.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__["a" /* OneSignal */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_badge__["a" /* Badge */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__["a" /* GoogleAnalytics */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_badge__["a" /* Badge */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__["a" /* OneSignal */]])
], SettingsPage);

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Post */
/* unused harmony export User */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WpProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_wp_api_angular__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_wp_api_angular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_wp_api_angular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Post = (function () {
    function Post(authorId, id, title, content, excerpt, date, mediaId) {
        this.authorId = authorId;
        this.id = id;
        this.title = title;
        this.content = content;
        this.excerpt = excerpt;
        this.date = date;
        this.mediaId = mediaId;
    }
    return Post;
}());

var User = (function () {
    function User(id, name, userImageUrl) {
        this.id = id;
        this.name = name;
        this.userImageUrl = userImageUrl;
    }
    return User;
}());

var WpProvider = (function () {
    function WpProvider(wpApiPosts, wpApiMedia, wpApiUsers, http) {
        var _this = this;
        this.wpApiPosts = wpApiPosts;
        this.wpApiMedia = wpApiMedia;
        this.wpApiUsers = wpApiUsers;
        this.http = http;
        this.wpApiUsers.getList()
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.users = [];
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var user = data_1[_i];
                var oneUser = new User(user['id'], user['name'], user['avatar_urls']['96']);
                _this.users.push(oneUser);
            }
        });
    }
    WpProvider.prototype.getPosts = function () {
        var _this = this;
        return this.wpApiPosts.getList()
            .map(function (res) { return res.json(); })
            .map(function (data) {
            var posts = [];
            for (var _i = 0, data_2 = data; _i < data_2.length; _i++) {
                var post = data_2[_i];
                var onePost = new Post(post['author'], post['id'], post['title']['rendered'], post['content']['rendered'], post['excerpt']['rendered'], post['date'], post['featured_media']);
                onePost.media_url = _this.getMedia(onePost.mediaId);
                posts.push(onePost);
            }
            return posts;
        });
        /*return this.http.get('http://simpson-motors.com/index.php/wp-json/wp/v2/posts')
        .map(res => res.json())
        .map(data => {
          var posts = [];
          for (let post of data) {
            console.log(data);
            let onePost = new Post(post[ 'author' ], post[ 'id' ], post[ 'title' ][ 'rendered' ], post[ 'content' ][ 'rendered' ], post[ 'excerpt' ][ 'rendered' ], post[ 'date' ], post[ 'featured_media' ]);
            onePost.media_url = this.getMedia(onePost.mediaId);
            posts.push(onePost);
          }
          return posts;
        });*/
    };
    WpProvider.prototype.getMedia = function (id) {
        return this.wpApiMedia.get(id)
            .map(function (res) { return res.json(); })
            .map(function (data) {
            return data['source_url'];
        });
    };
    WpProvider.prototype.getUserImage = function (userId) {
        for (var _i = 0, _a = this.users; _i < _a.length; _i++) {
            var usr = _a[_i];
            if (usr.id === userId) {
                return usr.userImageUrl;
            }
        }
    };
    WpProvider.prototype.getUserName = function (userId) {
        for (var _i = 0, _a = this.users; _i < _a.length; _i++) {
            var usr = _a[_i];
            if (usr.id === userId) {
                return usr.name;
            }
        }
    };
    return WpProvider;
}());
WpProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_wp_api_angular__["WpApiPosts"], __WEBPACK_IMPORTED_MODULE_3_wp_api_angular__["WpApiMedia"], __WEBPACK_IMPORTED_MODULE_3_wp_api_angular__["WpApiUsers"], __WEBPACK_IMPORTED_MODULE_4__angular_http__["Http"]])
], WpProvider);

//# sourceMappingURL=wp.js.map

/***/ })

},[416]);
//# sourceMappingURL=main.js.map