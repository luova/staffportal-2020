import { Injectable } from '@angular/core';
import {Http ,Response } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the StaffProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class StaffProvider {
 
	private apiURL = "http://portaladmin.luovalabs.com/dev/index.php/staff/verify/";
	private apiCheckUpdates = "http://portaladmin.luovalabs.com/dev/index.php/staff/checkversion/";
	private apiVerifyAgain = "http://portaladmin.luovalabs.com/dev/index.php/staff/verifyAgain/";
	private apiCreateTrack = "http://portaladmin.luovalabs.com/dev/index.php/tracking/loginCreate/";
	private apiTrack = "http://portaladmin.luovalabs.com/dev/index.php/tracking/login/";
	private apiTrackUser = "http://portaladmin.luovalabs.com/dev/index.php/tracking/post/";
	private apiTrackNotification = "http://portaladmin.luovalabs.com/dev/index.php/tracking/notification/";
	private apiToken = "http://portaladmin.luovalabs.com/dev/index.php/staff/token/";
	private articleAvailable = "https://www.simpsonmotors.com/wp-json/wp/v2/posts/";
	private staff_verify_bypass_url = "http://portaladmin.luovalabs.com/dev/index.php/staff/autoverify/";
	private apiVerifyTelephone = "http://portaladmin.luovalabs.com/dev/index.php/sendText/";
	private apiVerifyTelephoneText = "http://portaladmin.luovalabs.com/dev/index.php/verifyText/";
	public url: string = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160';
	
    public staff_url: string = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160';
	
	public articlesToLoad = 10;
    load_url: string = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&per_page=' + this.articlesToLoad;
    load_staff_url: string = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&per_page=' + this.articlesToLoad;
    items: any;
    staff_items: any;
    loader: any;
    staff_loader: any;
    featured: any = [];
    staff_featured: any = [];
    public articleList:any[] = [];
    userDetails : any;
    responseData: any;
    newsGroup = "staff";
    public loadingActive:boolean = false;
    public skipAllowed:boolean = false;

    public articlesLoaded = 0;
    public staff_articlesLoaded = 0;
    public combine_articlesLoaded = 0;
	public nowDate: any = new Date();
	public ONE_HOUR: any = 60 * 60 * 1000; /* ms */
	
	public playerid = null;
	
	constructor(public http: Http, public storage:Storage, public plt: Platform) {
		
		this.storage.get("myarticles").then((val) => {
			this.articleList = val;
		});
		
		this.storage.get('playerID').then((result) => {
			
			if( result ){
				this.playerid = result;
			}
				
		});
		
		let s = this.nowDate < this.ONE_HOUR;
		//alert(s);

		/* Check the last time the user was verified */
		this.storage.get('last_verified').then((result) => {
			
			/* if 1 hours ago */
			/* get the users information */
			if( s ){
				
				this.storage.get('verified_number').then((result) => {
										
					
					
					if( result ){
						/* If the number is verified */
						this.verifyNumberAgain(result).subscribe(data => {
							
							if( data["error"] == true ){
								
								this.disableUser();
							
							}else{
								
								/* Set time last verified */
								this.storage.set('last_verified', this.nowDate);
							
							}
						});
						
					}else{
						this.disableUser();
					}
				
				});
						
			}
				
		});
		
	}

	checkUserStatus(){
		
		this.storage.get('verified_number').then((result) => {
					
			if( result ){
				/* If the number is verified */
				this.verifyNumberAgain(result).subscribe(data => {
					
					if( data["error"] == true ){
						
						this.disableUser();
					
					}else{
						
						/* Set time last verified */
						this.storage.set('last_verified', this.nowDate);
					
					}
				});
				
			}else{
				this.disableUser();
			}
		
		});
				
	}
	
	disableUser(){
		
		this.storage.set('verified',false);
		this.storage.set('usertoken', null);
		
	}

	verifyNumber(number){
		return this.http.get( this.apiURL + number ).map((res : Response ) => res.json());
	}

	verifyNumberAgain(number){
		return this.http.get( this.apiVerifyAgain + number ).map((res : Response ) => res.json());
	}

	verifyNumberWithText(telephone){
		return this.http.get( this.apiVerifyTelephone + telephone ).map((res : Response ) => res.json());
	}

	verifyNumberWithTextCode(telephone, verification){
		return this.http.get( this.apiVerifyTelephoneText + telephone + "/" + verification ).map((res : Response ) => res.json());
	}

	updateLastLogin(token){
		return this.http.get( this.apiTrack + token ).map((res : Response ) => res.json());
	}

	getUpdateInformation(version){
		let type = "android";
		if (this.plt.is('ios')) {
			type = "ios"
		}

		let url = this.apiCheckUpdates + version + "/" + type;
		//alert(url);
		return this.http.get( url ).map((res : Response ) => res.json());
	}

	updateLogin(playerID, tel){
		return this.http.get( this.apiCreateTrack + playerID + "/" + tel).map((res : Response ) => res.json());
	}
  
	updateNotificationRead(playerID, notificationID, postID){
		//alert( this.playerid );
		let plyid = null;
		if( this.playerid == null){
			plyid = playerID;
		}else{
			plyid = this.playerid;
		}
		//alert( plyid );
		
		return this.http.get( this.apiTrackNotification + plyid  + "/" + notificationID + "/" + postID).map((res : Response ) => res.json());
		
	}
  

	createToken(playerID, number){
		return this.http.get( this.apiToken + playerID + "/" + number ).map((res : Response ) => res.json());
	}

	track(playerID, title, postid){
		
		let plyid = null;
		if( this.playerid == null){
			plyid = playerID;
		}else{
			plyid = this.playerid;
		}
		
		//alert(plyid);
		
		if( plyid ){
			//alert("plyid is here" + plyid);
			return this.http.get( "http://portaladmin.luovalabs.com/dev/index.php/tracking/post/"+postid+"/"+ encodeURIComponent(title) + "/" + plyid  + "" ).map((res : Response ) => res.json());
		}else{
			
			//alert("Check storage");
			this.storage.get('playerID').then((result) => {
				
				if( result ){
					//alert("Results id : " + result)
					return this.http.get( "http://portaladmin.luovalabs.com/dev/index.php/tracking/post/"+postid+"/"+encodeURIComponent(title) + "/" + result  + "" ).map((res : Response ) => res.json());
					
				}else{
					//alert("Nothing Found");
				}
					
			});
			
		}
	
	}
	
	getMore(date){
		
		return this.http.get( this.url+"&after="+date ).map((res : Response ) => res.json());
		
	}
	
	getMoreStaff(date){
		return this.http.get( this.staff_url+"&after="+date ).map((res : Response ) => res.json());
	}
	
	getNotificationPost(id){
		return this.http.get( "https://www.simpsonmotors.com/wp-json/wp/v2/posts/" + id ).map((res : Response ) => res.json());
	}
	
	handleStaffPost(data){
		
		
		
	}

	checkarticle(postID){
		
		//return new Promise((resolve,reject) => {

			this.articleAvailable += postID;
			return this.http.get( this.articleAvailable )  
			.map(res => res.json())
			.map(data => {
				
				if( data["data"] ){
					if(data["data"]["status"] == 404){
						//resolve(false);
						return false;
					}
				}else{
					//resolve(true);
					return true;
				}
			
			});

			
		//});

	}
	
	/* Handles grabbing the information from the Wordpress API specifically for Staff Articles */
	grabWordpressStaffInfo(loadType) {

        // Check if storage if empty and also check the last post
        // if empty, load all post and save to storage
        // if not empty, update post data if new article available
        // if error occurs loading articles, load from storage

        this.http.get( this.load_staff_url )
        .map(res => res.json())
        .map(data => {
			
			var posts = [];

			for (let post of data) {

				/* Determine if the post has any featured image attached to it */
				let featured_image = null;
				
				if( post['_embedded'] ){

				  if( post['_embedded']['wp:featuredmedia'] ){
					let fmedia = post['_embedded']['wp:featuredmedia'][0];
					if( fmedia['source_url'] ){
						featured_image = fmedia['source_url'];
					}
				  }
				  
				}

				let savedState = this.checkIfSaved( post[ 'id' ] );

				posts.push({'author':post['author' ], 'id':post[ 'id' ], 'title':post[ 'title' ][ 'rendered' ], 'content':post[ 'content' ][ 'rendered' ], 'category':'Staff', 'date':post[ 'date' ], 'featured_media':post[ 'featured_media' ], "post_image":featured_image, "savedState":savedState
				});

				this.staff_articlesLoaded++;

			}
		
			this.storage.set("staff_articles", posts);
			return posts;
		  
        })
        .subscribe(
            data => {
                // Update Storage
                //this.storage.set("staff_articles", data);
				//console.log("Complete Staff");
				//console.log(data);
            },
            err => {
				//alert("Error Occured");
				let items_temp = this.getStorage("staff_articles");
				this.items = items_temp;
				//if(items_temp){
					//this.items = items_temp;
				//}
            }
        );
    }
	
	/* Handles grabbing the information from the Wordpress API specifically for Staff Articles */
	grabStaff() {

        // Check if storage if empty and also check the last post
        // if empty, load all post and save to storage
        // if not empty, update post data if new article available
        // if error occurs loading articles, load from storage
		
		//alert(this.load_staff_url);
		
        return this.http.get( this.load_staff_url )
		.map(res => res.json())
        .map(data => {
			
			//alert( "data" );
			//alert( JSON.stringify(data) );
			
			var posts = [];

			for (let post of data) {

				let featured_image = this.getFeaturedImage(post);
				let savedState = this.checkIfSaved( post[ 'id' ] );

				posts.push({'author':post['author' ], 'id':post[ 'id' ], 'title':post[ 'title' ][ 'rendered' ], 'content':post[ 'content' ][ 'rendered' ], 'category':'Staff', 'date':post[ 'date' ], 'featured_media':post[ 'featured_media' ], "post_image":featured_image, "savedState":savedState
				});
				
				this.storage.set("staff_last_date", post[ 'date' ]);
				this.staff_articlesLoaded++;

			}
		
			this.storage.set("staff_articles", posts);
			
			//alert( "POSTs" );
			//alert( JSON.stringify(posts) );
			return posts;
		
		});
		
    }
	
	/* Handles grabbing the information from the Wordpress API specifically for Staff Articles */
	grabGeneral() {

        // Check if storage if empty and also check the last post
        // if empty, load all post and save to storage
        // if not empty, update post data if new article available
        // if error occurs loading articles, load from storage

        return this.http.get( this.load_url )
		.map(res => res.json())
        .map(data => {
			
			var posts = [];

			for (let post of data) {

				let featured_image = this.getFeaturedImage(post);
				let category = this.getCategory(post);

				let savedState = this.checkIfSaved( post[ 'id' ] );

				posts.push({'author':post['author' ], 'id':post[ 'id' ], 'title':post[ 'title' ][ 'rendered' ], 'content':post[ 'content' ][ 'rendered' ], 'category':category["category"], 'date':post[ 'date' ], 'featured_media':post[ 'featured_media' ], "post_image":featured_image, "savedState":savedState
				});
				
				this.storage.set("last_date", post[ 'date' ]);
				this.articlesLoaded++;

			}
		
			this.storage.set("articles", posts);
			
			return posts;
		
		});
		
    }
	
	getCategory(post){
		
		let featured_category;
		let featured_category_plain;
		let info = [];
		
		if( post['_embedded']['wp:term'] ){
			let pCategory = post['_embedded']['wp:term'][0][0];
			if( pCategory['name'] ){
				featured_category = pCategory['name'];

				if( featured_category == "Automakers"){
				  featured_category = "<span class = 'color_blue'>" + featured_category + "</span>";
				  featured_category_plain = "automakers";
				}else if( featured_category == "News"){
				  featured_category = "<span class = 'color_red'>" + featured_category + "</span>";
				  featured_category_plain = "news";
				}else if( featured_category == "Staff"){
				  featured_category = "<span class = 'color_green'>" + featured_category + "</span>";
				  featured_category_plain = "staff";
				}else if( featured_category == "Uncategorized"){
				  featured_category = "";
				  featured_category_plain = "";
				}
			}
		}
		
		info["category"] = featured_category;
		info["plain"] = featured_category_plain;
		
		return info;
	}
	
	getFeaturedImage(post){
		
		/* Determine if the post has any featured image attached to it */
		let featured_image = null;
		
		/* Grab the image from the actual post */
		let str = post[ 'excerpt' ][ 'rendered' ] + "";

		var elem= document.createElement("div");
		elem.innerHTML = str;

		var images = elem.getElementsByTagName("img");
		let src = "";

		/* Loop through all images found in the html content */
		for(var i=0; i < images.length; i++){
		   //console.log(images[i].src);
		   src = images[i].src;
		   var newStr = str.replace(/<img[^>]*>/g, '');
		}

		if( post['_embedded'] ){

		  if( post['_embedded']['wp:featuredmedia'] ){
			let fmedia = post['_embedded']['wp:featuredmedia'][0];
			if( fmedia['source_url'] ){
				featured_image = fmedia['source_url'];
			}
		  }
		  
		}
		
		/* If no featured image is found then use an image from the src */
		if( featured_image == null ){
			if( src != null ){
			   featured_image = src;
			}
		}
		
		//console.log("featured_image");
		//console.log(featured_image);
		
		return featured_image;		
				
	}

    /* Handles grabbing the information from the Wordpress API */
    grabWordpressInfo(loadType) {

        /*if( loadType != "none"){
          this.presentLoading();
          this.items = this.getStorage("articles");
        }*/

        // Check if storage if empty and also check the last post
        // if empty, load all post and save to storage
        // if not empty, update post data if new article available
        // if error occurs loading articles, load from storage

        this.http.get( this.load_url )
        .map(res => res.json())
        .map(data => {

          var posts = [];
          let index = 0;
			
			for (let post of data) {

            //console.log(data);

            /* Grab the image from the actual post */
            let str = post[ 'excerpt' ][ 'rendered' ] + "";

            var elem= document.createElement("div");
            elem.innerHTML = str;

            var images = elem.getElementsByTagName("img");
            let src = "";

            /* Loop through all images found in the html content */
            for(var i=0; i < images.length; i++){
               //console.log(images[i].src);
               src = images[i].src;
               var newStr = str.replace(/<img[^>]*>/g, '');
            }

            /* Determine if the post has any featured image attached to it */
            let featured_image = null;
            let featured_category = null;
            let featured_category_plain = null;

            if( post['_embedded'] ){

              if( post['_embedded']['wp:featuredmedia'] ){
                let fmedia = post['_embedded']['wp:featuredmedia'][0];
                if( fmedia['source_url'] ){
                    featured_image = fmedia['source_url'];
                }
              }

              /*console.log("WP Term");
              console.log(post['_embedded']['wp:term']);
              console.log(post['_embedded']['wp:term'][0][0]);
              console.log(post['_embedded']['wp:term'][0][0]["name"]);*/

              if( post['_embedded']['wp:term'] ){
                let pCategory = post['_embedded']['wp:term'][0][0];
                if( pCategory['name'] ){
                    featured_category = pCategory['name'];

                    if( featured_category == "Automakers"){
                      featured_category = "<span class = 'color_blue'>" + featured_category + "</span>";
                      featured_category_plain = "automakers";
                    }else if( featured_category == "News"){
                      featured_category = "<span class = 'color_red'>" + featured_category + "</span>";
                      featured_category_plain = "news";
                    }else if( featured_category == "Staff"){
                      featured_category = "<span class = 'color_green'>" + featured_category + "</span>";
                      featured_category_plain = "staff";
                    }else if( featured_category == "Uncategorized"){
                      featured_category = "";
                      featured_category_plain = "";
                    }
                }
              }
          }

            //console.log(featured_category);

            /* If no featured image is found then use an image from the src */
            if( featured_image == null ){
                if( src != null ){
                   featured_image = src;
                }
            }

			if( this.articlesLoaded == 0 ){

			  this.featured.push({'author':post['author' ], 'id':post[ 'id' ], 'title':post[ 'title' ][ 'rendered' ], 'content':post[ 'content' ][ 'rendered' ], 'category':featured_category, 'excerpt':newStr, 'date':post[ 'date' ], 'featured_media':post[ 'featured_media' ], 'post_image': featured_image});
				
			  this.storage.set("last_date", post[ 'date' ]);

			}else{
				
				let savedState = this.checkIfSaved( post[ 'id' ] );
				
				posts.push({'author':post['author' ], 'id':post[ 'id' ], 'title':post[ 'title' ][ 'rendered' ], 'content':post[ 'content' ][ 'rendered' ], 'category':featured_category, 'excerpt':newStr, 'date':post[ 'date' ], 'featured_media':post[ 'featured_media' ], 'post_image': featured_image, "savedState":savedState});
			}

			this.articlesLoaded++;
            this.combine_articlesLoaded++;
			
			}
			
			this.storage.set("featured", this.featured);
			this.storage.set("articles", posts);
			
			return posts;
		  
        })
        .subscribe(
            data => {
                // Update Storage
                //this.storage.set("articles", data);
				//console.log("Complete General");
				//console.log(data);
            },
            err => {
							
				this.items = this.getStorage("articles");
				
			}
        );

    }
	
	
	
	/* Helper Function when looping through articles from API, checks to see if articles loaded are saved by the user or not! */
	checkIfSaved( id ){
		
		//console.log( "Length " + this.articleList.length );
		
		if( this.articleList.length > 0 ){
			
			for( let i = 0; i < this.articleList.length; i++ ){
				
				if( id == this.articleList[i]["article"].id){
					//alert(this.articleList.length );
					return i;
				}
				
			}
			return -1;
			
		}
		return -1;
	}
	
	checkCardSpecial(date, items){
		
		let lastMonth = new Date();
		lastMonth.setMonth(lastMonth.getMonth(), 0);
		
		let article = new Date(date);
		let stateMonth = article.getTime() >= lastMonth.getTime();
		
		if( stateMonth ){
			//alert( "Article " + article );
			//alert( "Last Month " + lastMonth );
			return "card-special";
		}
	}
	
	/* Handles Setting Local Storage to Phone */
	setStorage(name, data){

       // set a key/value
       this.storage.set(name, data);

    }

	/* Handles Retrieving Local Storage from Phone */
    getStorage(name){

       // set a key/value
       this.storage.get(name).then((val) => {
          //console.log('Requested Storage Data is', val);
          return name;
				});

    }

	/* Check if the user has auto verification enabled */
	checkStaffAutoVerification(tel){

		return this.http.get( this.staff_verify_bypass_url + tel + "/3243")
		.map(res => res.json());
		
	}
	
	
}
