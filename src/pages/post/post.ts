import { NavController, NavParams, Platform } from 'ionic-angular';
import { Component, ViewChild, ElementRef} from '@angular/core';
//import { DatePipe } from '@angular/common';
import { Storage } from '@ionic/storage';
import { StaffProvider } from '../../providers/staff/staff';
import { ToastController, AlertController, ModalController } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';
import moment from 'moment';
import { DomSanitizer } from '@angular/platform-browser';
import { SuperTabsController } from 'ionic2-super-tabs';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Badge } from '@ionic-native/badge';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
  providers:[OneSignal, Badge]
})

export class PostPage {

	selectedItem: any;
	private readTime:any = null;
	private wordsPerMinute:number = 270;
	public contentLoaded = false;
	public stitle;
	public sid;
	public postDate;
	public articleContent;
	public notificationID;
	public articleError = false;
	public playerID;
	public replaceVideoEmbed = "";
	
	  /**
    * Property that will store the selected hyperlink from the view template
    * @private
    * @name _link
    * @type {string}
    */
	   private _link : string;
	   private _src : string;

	constructor(private photoViewer:PhotoViewer, public _browser: InAppBrowser, private _element : ElementRef, public badge:Badge, private platform:Platform, private superTabsCtrl: SuperTabsController, private sanitizer: DomSanitizer, private nav: NavController, public navParams: NavParams, private storage: Storage, public staff:StaffProvider, public toastCtrl: ToastController, public alertCtrl:AlertController, private modalCtrl: ModalController, public oneSignal: OneSignal) {
		
		// Get the user playerID
		this.storage.get("playerID").then((data) => {
				
			if(data == null){
				//alert("Get Player ID");
				this.oneSignal.getIds().then((ids) => {
				
					this.playerID = ids.userId;
					this.storage.set("playerID",this.playerID);
						
				});
			}else{
				//alert("Already got it");
				this.playerID = data;
			}
			
		});
		
		
	}
	
	@ViewChild('mainHeader') defaultContent: ElementRef;
	
	
	
	hideToolbar() {
		this.superTabsCtrl.showToolbar(false);
	}
	
	showToolbar() {
		this.superTabsCtrl.showToolbar(true);
	}

	ionViewWillLeave(){

		/*alert("Clear badge on leave");
		this.badge.get().then((bcount) => {
			alert(bcount);
			this.badge.decrease(bcount);
			alert( this.badge.get() );
		});*/
		
	}

	  /**
    * Use Angular OnInit lifecycle hook to trigger functionality when the view is initialising
    * @public 
    * @method ngOnInit
    * @return {none}
    */
   	public ngOnInit() : void
    {
      //this._enableDynamicHyperlinks();
	}
	
	/**
    * Enable hyperlinks that are embedded within a HTML string
    * @private
    * @method _enableDynamicHyperlinks
    * @return {none}
    */
	private _enableDynamicHyperlinks() : void
	{
      // Provide a minor delay to allow the HTML to be rendered and 'found'
      // within the view template
      setTimeout(() => 
      {
         // Query the DOM to find ALL occurrences of the <a> hyperlink tag
		 const urls : any    = this._element.nativeElement.querySelectorAll('a');
		 const imgurls : any    = this._element.nativeElement.querySelectorAll('img');

         // Iterate through these
         urls.forEach((url) => 
         {
            // Listen for a click event on each hyperlink found
            url.addEventListener('click', (event) => 
            {
               // Retrieve the href value from the selected hyperlink
			   event.preventDefault();
               this._link = event.target.href;

               // Log values to the console and open the link within the InAppBrowser plugin
			   //alert('Name is: ' + event.target.innerText);

			   window.open("https://www.luovalabs.com", "_blank");
            }, false);
		 });
		 
		 
      }, 2000);
	}
	   
	/**
    * Creates/launches an Ionic Native InAppBrowser window to display hyperlink locations within 
    * @private
    * @method _launchInAppBrowser
    * @param {string}    link           The URL to visit within the InAppBrowser window 
    * @return {none}
    */
   	private _launchInAppBrowser(link : string) : void
   	{
      let opts : string = "location=yes,clearcache=yes,hidespinner=no"
	  this._browser.create(link, '_self', opts);
	  
   	}


	 ngAfterViewInit() {
		
		//alert("Clear badge on destroy");
		this.badge.get().then((bcount) => {
			//alert(bcount);
			this.badge.decrease(bcount-1);
			//alert( this.badge.get() );
		});
		
		try{
			let y = (document.querySelector("#mainHeader") as HTMLElement).style.display = "none";
			let i = (document.querySelector("super-tabs-toolbar") as HTMLElement).style.display = "none";
			let x = (document.querySelector(".supertabs") as HTMLElement).style.marginTop = "0px";
			let h = (document.querySelector(".supertabs") as HTMLElement).style.height = "100%";
		}catch(e){
			//alert(e);
		}

		this.hideToolbar();

	} 

	// Update the view before leaving by changing it back to normal
	ionViewDidLeave() {
		
		this.showToolbar();
		
		try{
			let y = (document.querySelector("#mainHeader") as HTMLElement).style.display = "inline";
			let i = (document.querySelector("super-tabs-toolbar") as HTMLElement).style.display = "block";
			let x = (document.querySelector(".supertabs") as HTMLElement).style.marginTop = "68px";
			let h = (document.querySelector(".supertabs") as HTMLElement).style.height = "100%";
		}catch(e){
			//alert(e);
		}
		
	}

	// for tasks you want to do every time you enter in the view (setting event listeners, updating a table, etc.).
	// Check if the article is still valid
	// Track the users progress
	ionViewWillEnter(){

		// If we navigated to this page, we will have an item available as a nav param
		this.selectedItem = this.navParams.get('item');
		let type = this.navParams.get('type');
		
		// Determine whether or not this article is from a regular page or its from a notification
		var re = new RegExp("<a([^>]* )href=\"([^\"]+)\"", "g");
		
		//alert("type " + type);
		if( type == "simple" ){
			
			//alert("Simple");
			this.stitle = this.selectedItem.title;
			this.sid = this.selectedItem.id;
			this.postDate = this.selectedItem.date;
			this.articleContent = this.selectedItem.content;
			//this.articleContent = this.articleContent.replace(re, "<a$1href=\"#\" data-nlink =\"photoViewer.show('$2')\"");

			this.articleContent = this.sanitizer.bypassSecurityTrustHtml(this.articleContent);

			// Check to see if this post if still available to users and hasn't been removed or edited.
			this.postAvailable();
	
			//this.calcReadTime();
			
		}else{
			
			this.sid = this.selectedItem.sid;
			this.notificationID = this.selectedItem.notification.payload.notificationID;
			
			// testing
			//this.sid = this.selectedItem;
			//this.notificationID = "e873bfa9-a092-41ec-8178-7f51acdcf947";
			// testing

			this.staff.getNotificationPost(this.sid).subscribe(notification => {

				// Use different attributes to access the information
				this.stitle = notification["title"]["rendered"];
				this.postDate = notification["date"];
				this.articleContent = notification["content"]["rendered"];
				//this.articleContent = this.articleContent.replace(re, "<a$1href=\"#\" data-nlink =\"photoViewer.show('$2')\"");
				
				//this.articleContent = this.articleContent.replace("<img", "<img imageViewer");
				
				this.updateNotificationRead();
						
				// Check to see if this post if still available to users and hasn't been removed or edited.
				this.postAvailable();

			},
			error => {
			  
			  let alert = this.alertCtrl.create({
				title: 'Oops!',
				subTitle: "Unable to fetch article. " + error,
				buttons: ['OK']
			  });
			  
			  alert.present();
			  
			});

			/*

			// Use different attributes to access the information
			this.stitle = this.selectedItem.notification.payload.additionalData.postTitle;
			this.sid = this.selectedItem.notification.payload.additionalData.postID;
			this.postDate = this.selectedItem.notification.payload.additionalData.postDate;
			this.notificationID = this.selectedItem.notification.payload.notificationID;
			this.articleContent = this.selectedItem.ncontent;
			
			this.updateNotificationRead();

			*/

			
		}
		
		
	}


	checkContent(event){
		
		//console.log(event);
		//alert(JSON.stringify(event));
		//alert(JSON.stringify(event.target.currentSrc));
		//alert(JSON.stringify(event.target.dataset.nlink));
		
		//let browser = this.iab.create("https://www.google.com","_blank");
		//browser.show();

		if( event.target.localName == "img"){
			//alert(event.target.currentSrc);
			this.openLink(event.target.currentSrc);
		}

	}
	
	openLink(url){
		//const browser = this.iab.create(url, '_blank', 'location=yes,enableViewportScale=yes,hardwareback=yes,zoom=yes');
		//browser.show();
		//this.photoViewer.show('https://mysite.com/path/to/image.jpg');
		window.open(url, '_blank');
	}

	async postAvailable(){
		try{
			let postStatus = await this.staff.checkarticle(this.sid);
			console.log(" postStatus " + postStatus );
			
			// Track the reading of the post by the user
			this.trackUserPostRead();
			
			// Allow content to be shown
			this.contentLoaded = true;
	
			// Remove launchPost variable which handles notifications pending to be shown
			this.storage.set("launchPost",null);
			
		}catch(error){
			console.error("Error occured while fetching the post information.");
			// Don't content to be shown
			this.contentLoaded = false;
			this.articleError = true;
		}
	}

	
	trackUserPostRead(){
		
		try{
			this.staff.track(this.playerID, this.stitle, this.sid).subscribe(data => {
			//alert( JSON.stringify(data) );
			});
		}catch(error){
			//alert(error);
		}
		
	}

	// Update the datebase with the users ID regarding the notification read
	updateNotificationRead(){
		
		try{
			
			this.staff.updateNotificationRead(this.playerID, this.notificationID, this.sid ).subscribe(results => {
		
				//alert( "update Notification Read " + JSON.stringify(results) );
			
			},
			error => {
				//alert( "Error updating Notification Read " + JSON.stringify(error) );
			});
		
		}catch(error){
			//alert(error);
		}
	
	}
	
	/* Convert Date to proper human readable date */
	convertDate(date){
		if(date!= "0000-00-00 00:00:00"){
			return moment(date).format('ll');
		}
	}

}
