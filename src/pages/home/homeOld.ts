import { Component, ApplicationRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { ModalController, Platform, NavParams, ViewController, NavController, LoadingController, AlertController } from 'ionic-angular';

import { SearchPage } from '../search/search';
import { PostPage } from '../post/post';
import { StaffPage } from '../categories/staff/staff';
import { NewsPage } from '../categories/news/news';

import { Storage } from '@ionic/storage';
import { AuthService } from '../../providers/auth-service/auth-service';
import { StaffProvider } from '../../providers/staff/staff';
import { OneSignal } from '@ionic-native/onesignal';
import { Badge } from '@ionic-native/badge';
import { SuperTabsController } from 'ionic2-super-tabs';
import { Observable } from 'rxjs/Rx';
import { StatusBar } from '@ionic-native/status-bar';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[OneSignal, Badge],
})
export class HomePage {
	
	/* PAGES FOR ARTICLES */
	page_staff: any = StaffPage;
	page_news: any = NewsPage;
	
	/* Variables - For Updates */
	public downloadstate;
	public dstate;
	public progressStatus;
	
	public username;
	public email;
	public noNotification;
	//public tabBarElement;
	public articlesToBeRead = 0;
	isUpdateAvailable:boolean = false; 
  
    constructor(private statusBar: StatusBar, private appRef: ApplicationRef, public loadingCtrl: LoadingController, private superTabsCtrl: SuperTabsController, public badge:Badge, public modalCtrl: ModalController, private storage: Storage, public navCtrl: NavController, public authService:AuthService, public staff:StaffProvider, public alertCtrl:AlertController, private platform: Platform, public oneSignal: OneSignal) {
		
		//alert("home");
		//this.checkLaunchPost();
		//this.photoviewer();
		//this.presentModal();

        let info = this.authService.getUserInfo();
        if( info ){
          this.username = info['name'];
          this.email = info['email'];
		}
		
		let url = "https://luovalabs.com/wp-content/uploads/2019/10/LuovaLabs-Website-Development-SEO-1024x607.png";

		//this.photoviewer.show(url);
		
  }

  ionViewWillLeave(){

	/*alert("Clear badge on leave");
	this.badge.get().then((bcount) => {
		alert(bcount);
		this.badge.decrease(bcount);
		alert( this.badge.get() );
	});*/
	
  }

  
  ionViewWillEnter(){

	/*alert("Clear badge on enter");
	this.badge.get().then((bcount) => {
		alert(bcount);
		this.badge.decrease(bcount);
		alert( this.badge.get() );
	});*/

	this.platform.ready().then( ()=>{
		
		this.statusBar.styleDefault();

		Observable.interval(300000).subscribe(x => {

			this.storage.get("staff_last_date").then((val) => {
				
				if(val){
					//alert(val); // 2017-11-13T14:41:13

					this.staff.getMoreStaff(val).subscribe(
						data => {
							if( data.length > 0 ){
								this.superTabsCtrl.setBadge('staff_tab', data.length);
							}
						},
						err => {
							//alert("Error");
							//alert(JSON.stringify(err));
						}
					);
				
				}
			});

			this.storage.get("last_date").then((val) => {
				
				if(val){
					//alert(val);

					this.staff.getMore(val).subscribe(
						data => {
							if( data.length > 0 ){
								this.superTabsCtrl.setBadge('news_tab', data.length);
							}
						},
						err => {
							//alert("Error");
							//alert(JSON.stringify(err));
						}
					);
				
				}
			});

		});

	});
	
  }

  ngAfterViewInit() {
	
	this.storage.get("staff_last_date").then((val) => {
		
		if(val){
			//alert(val); // 2017-11-13T14:41:13

			this.staff.getMoreStaff(val).subscribe(
				data => {
					if( data.length > 0 ){
						this.superTabsCtrl.setBadge('staff_tab', data.length);
					}
				},
				err => {
					//alert("Error");
					//alert(JSON.stringify(err));
				}
			);
		
		}
	});

	this.storage.get("last_date").then((val) => {
		
		if(val){
			//alert(val);

			this.staff.getMore(val).subscribe(
				data => {
					if( data.length > 0 ){
						this.superTabsCtrl.setBadge('news_tab', data.length);
					}
				},
				err => {
					//alert("Error");
					//alert(JSON.stringify(err));
				}
			);
		
		}
	});

}
	
	checkLaunchPost(){
		
		
		this.storage.get("launchPost").then((data) => {
			
			if( data != null ){
				try{
					
					this.navCtrl.push(PostPage, {
					   item: data,
					   type: "notification"
					});
				
					
				}catch(e){
					
					//alert(e);
					
				}
				
				this.noNotification = false;
				
			}else{
				//alert("No post found");
				this.noNotification = true;
			}
		});
		
		
	}
	
	/* Search Pop Over */
    openModal(characterNum) {
       let modal = this.modalCtrl.create(SearchPage, characterNum);
       modal.present();
    }

}