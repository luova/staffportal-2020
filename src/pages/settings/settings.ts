import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Slides } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Badge } from '@ionic-native/badge';
import { OneSignal, OSNotificationPayload } from '@ionic-native/onesignal';

import { oneSignalAppId, sender_id } from '../../config';
import { PostPage } from '../../pages/post/post';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
  providers:[OneSignal, Badge]
})
export class SettingsPage {

  	@ViewChild(Slides) slides: Slides;
  	public appbuttontext = "Skip To App";
    public playerID;
	public pagerOption = true;
	private notHandlerCheck;
	private debugMode = false;
	
	constructor(private ga:GoogleAnalytics, private badge:Badge, private platform: Platform, public navCtrl: NavController, public navParams: NavParams, public storage:Storage, public oneSignal: OneSignal) {
		
		this.initializeApp();

	}

	initializeApp(){
		
		this.platform.ready().then(() => {

			this.setupOneSignalNotification();
			this.setupGoogleAnalytics();
			
		});
	}

	setupOneSignalNotification(){

		this.oneSignal.startInit(oneSignalAppId, sender_id);

		//The inFocusDisplaying() method, controls how OneSignal notifications will be shown when one is received while your app is in focus. 

		this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

		//The handleNotificationReceived() is a method that runs when a notification is received. It calls the onPushReceived()

		this.oneSignal.handleNotificationReceived().subscribe(data => this.onPushReceived(data, data.payload));

		// The handleNotificationOpened() is a method that runs when a notification is tapped or when closing an alert notification shown in the app.
		this.oneSignal.handleNotificationOpened().subscribe(data => this.onPushOpened(data, data.notification.payload));

		// Check to see if the user gave us permission
		let state = this.oneSignal.getPermissionSubscriptionState();
		
		//The endInit() method must be called to complete the initialization of OneSignal.
		this.oneSignal.endInit();

		// Get the User ID, this identifies the user. We'll use this ID and set the Player ID
		this.oneSignal.getIds().then((ids) => {

			let playerID = ids.userId;
			alert(playerID);
			this.storage.set("playerID",playerID);
				
		});
					
	}

	setupGoogleAnalytics(){

		//Google Analytics
		this.ga.startTrackerWithId('UA-104903092-1')
		.then(() => {
			//console.log('Google analytics is ready now');
			this.ga.trackView('test');
			// Tracker is ready
			// You can now track pages or set additional information such as AppVersion or UserId
		})
		.catch(e => console.log('Error starting GoogleAnalytics', e));

	}

	// Code to handle the notifications
	private onPushReceived(data, payload: OSNotificationPayload) {
		alert('Push recevied:' + payload.body);

		if( this.notHandlerCheck != true ){
			//alert("Running handleNotificationReceived from handleNotificationOpened ");
			this.notHandlerCheck = true;
			this.notificationHandling(data);
		}
	}
	
	private onPushOpened(data, payload: OSNotificationPayload) {
		alert('Push opened: ' + payload.body);

		if( this.notHandlerCheck != true ){
			//alert("Running notificationHandling from handleNotificationOpened ");
			this.notHandlerCheck = true;
			this.notificationHandling(data);
		}

	}

	// Custom behavious to handle how the app should behave with a new notification
	private notificationHandling(data){
		
		this.notHandlerCheck = false;
		
        if( this.debugMode == true ){
			
			this.storage.set('launchPost', data);
			if(data){
				this.navCtrl.push(PostPage, {
					item: data,
					type: "notification"
				});
			}
		}else{
			
			let sid = data.notification.payload.additionalData.postID;
			data["sid"] = sid;

			this.storage.set('launchPost', data);
			if(data){
				this.navCtrl.push(PostPage, {
					item: data,
					type: "notification"
				});
			}
		}
    
    }

	provideAppWithData(){
		
		this.oneSignal.getIds().then((ids) => {
			
			this.playerID = ids.userId;
			this.storage.set("playerID", this.playerID);
	
		});
	}

	skipIntro(){
		this.navCtrl.setRoot(HomePage);
	}

	slideChanged() {

		let currentIndex = this.slides.getActiveIndex();
		this.pagerOption = true;
		
		if( currentIndex >= 2){
			this.appbuttontext = "Start App!";
			this.pagerOption = false;
		}else{
			this.appbuttontext = "Skip Intro";
		}

	}
  
	
}
