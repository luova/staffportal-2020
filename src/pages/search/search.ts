import { Component } from '@angular/core';
import { ModalController, IonicPage, Platform, NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { PostPage } from '../post/post';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { ToastController, AlertController } from 'ionic-angular';
import moment from 'moment';

/**
 * Generated class for the SearchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})

export class SearchPage {

  searchQuery: string = '';
  items: any[];
  itemsOrig: any[];
  searchTextDisplay = true;
  loadingResults = false;
  private showInternal = true;
  public resText = "";

  public searchList: any[] = [];
  public subscription;
  public url = "";

  constructor( public alertCtrl:AlertController, private http: Http, public platform: Platform, public params: NavParams, public viewCtrl: ViewController, private storage: Storage, private navCtrl:NavController) {

    this.storage.get("articles").then((val) => {
		this.items = val;
		this.itemsOrig = val;
   });

  //  this.initializeItems();

  }

  initializeItems() {
    this.items = this.itemsOrig;
  }

  public keys;
  getItems(ev: any) {
	  
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;
    this.keys = val;
    
    this.resText = " for " + val;

    if( val.length != 0 ){
      this.searchTextDisplay = false;
    }else{
      this.searchTextDisplay = true;
    }

    if( val.length >= 2 ){
      //setTimeout(() => {
      // if the value is an empty string don't filter the items
      /*if (val && val.trim() != '') {
        this.items = this.items.filter((item) => {
          return (item["title"].toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }*/
      
      this.searchForString(val.toLowerCase());

      //}, 1500);
    }
  }

  /*

  if ( this.subscription ) {
    this.subscription.unsubscribe();
  }
  this.subscription = this.http.get( 'awesomeApi' )
  .subscribe((res)=> {
    // your awesome code..
  })

  */

  searchForString(keyword){

      this.loadingResults = true;

      this.url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&search=' + keyword;
      
      //if ( this.subscription ) {
          //this.subscription.unsubscribe();
      //}

      this.http.get( this.url )
      .debounceTime(1500)
      .distinctUntilChanged()
      .switchMap((keyword) => this.http.get('https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&search=' + this.keys))
      .map(res => res.json())
      .subscribe(
          data => {
            //console.log("data");
            //console.log(data);
            // this.searchList = [];
            if( data ){
              for (let x = 0; x < data.length; x++) {

                //if( ! this.checkID( data[x][ 'id' ] ) ){

                  this.searchList.push({'author':data[x]['author' ], 'id':data[x][ 'id' ], 'title':data[x][ 'title' ][ 'rendered' ], 'content':data[x][ 'content' ][ 'rendered' ], 'date':data[x][ 'date' ], 'featured_media':data[x][ 'featured_media' ]});

                //}
              }
            }

            this.loadingResults = false;
            this.showInternal = false;

          },
          err => {

            //console.log(err);
            
            let alert = this.alertCtrl.create({
              title: 'Unable to load search results',
              buttons: ['OK']
            });
            alert.present();

            this.loadingResults = false;
            this.showInternal = true;

          });
  }

  checkID( id ){
    if( this.items ){
      for( let i = 0; i < this.items.length; i++ ){

          if( id == this.items[i]["id"] ){
              return true;
          }

      }
    }
    
    return false;

  }

  pushPage(item){

    // push another page onto the navigation stack
    // causing the nav controller to transition to the new page
    // optional data can also be passed to the pushed page.
    this.navCtrl.push(PostPage, {
      item: item,
	  type: 'simple'
    });

  }

  dismiss() {
      this.viewCtrl.dismiss();
  }

  /* Convert Date to proper human readable date */
  convertDate(date){
    if(date!= "0000-00-00 00:00:00"){
      return moment(date).format('LL');
      //return this.datePipe.transform(date, 'fullDate');
    }
  }

}
