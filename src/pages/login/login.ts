import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { StaffProvider } from '../../providers/staff/staff';
import { HomePage } from '../home/home';
import { WelcomePage } from '../welcome/welcome';
import { Storage } from '@ionic/storage';
import { Sim } from '@ionic-native/sim';
import { SMS } from '@ionic-native/sms';
import { OneSignal } from '@ionic-native/onesignal';
import {Http} from '@angular/http';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [Sim, SMS, OneSignal]
})

export class LoginPage {

	loader: any;
	credentials = { telephone: '', verification: '' };
	private verfyStep1 = true;
	private verfyStep2 = false;
	private saveNumber;
	private sendState = "";
  private loginAction:boolean = false;
  public autoVerify:boolean = false;
  private loading;

	constructor(private http: Http, private menu: MenuController, public navCtrl: NavController, public navParams: NavParams, public authService:AuthService, public alertCtrl:AlertController, public loadingCtrl:LoadingController, public storage: Storage, public sim:Sim, public staff:StaffProvider, private sms: SMS, private oneSignal:OneSignal) {

    // Check to see if the user if already verified in the application
    // If so then redirect to the HomePage
    // This maybe handled by the App Component already though
		this.storage.get('usertoken').then((result) => {
			if( result ){
				this.navCtrl.setRoot(HomePage);
      }
    });
    
	}

  // Helper Functions
	presentLoading() {
		this.loader = this.loadingCtrl.create({
			content: "Please wait. Verifying Phone Number ..."
		});
		
		this.loader.present();
	}

	showError(text){
	
		if( this.loader != null ){
			this.loader.dismiss();
		}

		let alert = this.alertCtrl.create({
			title: 'Fail',
			subTitle: text,
			buttons: ['OK']
		});
		alert.present();

  }
  
  // Disable the menu so unauthorized users cannot get into the app
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
    this.menu.enable(false);
  }

  // Reenable the menu swipping ability
  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise 
    // the rest of the pages won't be able to swipe to open menu
    this.menu.swipeEnable(true);
    this.menu.enable(true);
  }

  // Verify the phone number that the user is entering is valid
  verify(){

    // Verify the phone number format
    this.credentials.telephone = this.credentials.telephone.replace(/\D/g,'');
    let telephoneNumber = this.credentials.telephone;
    this.saveNumber = telephoneNumber;

    if( telephoneNumber.length != 10 ){

      let alert = this.alertCtrl.create({
        title: 'Invalid Number',
        subTitle: 'Invalid phone number entered, remember to include your area code.',
        buttons: ['OK']
      });
      alert.present();

    }else{

      // Loading
      let loading = this.loadingCtrl.create({
        content: 'Sending Verification Code. Please wait...'
      });
      
      loading.present();
      this.loading = loading;

      this.checkStaffVerification();
 
    }
  }

  // Determine if the user is allowed to skip the authentication
  checkStaffVerification(){

    /* Check if the user is allowed to skip number authorisation */
    // The admin personal adjust this setting from their admin panel
    this.staff.checkStaffAutoVerification(this.credentials.telephone).subscribe(verificationStateBypass => {
      
      console.log("verificationStateBypass");
      console.log(verificationStateBypass);

      if( verificationStateBypass ){
        console.log("User Auto Verified");
        this.generateVerificationNumberForAutoVerify(this.credentials.telephone);
      }else{
        console.log("User Not Auto Verified. Proceed with Auth Verification");
        this.verifyTheUserWithTextMessage();
      }

    },
    error => {
      
      this.loginAction = false;
      this.verifyTheUserWithTextMessage();
      
    });
      
  }
    
  verifyTheUserWithTextMessage(){

    console.log("verifyNumberWithText");

    let apiVerifyTelephone = "http://portaladmin.luovalabs.com/dev/index.php/sendText/";

    this.http.get( "http://portaladmin.luovalabs.com/dev/index.php/sendText/" + this.credentials.telephone  )
		.map(res => res.json())
		.subscribe(
			data => {

      this.loading.dismiss();

      // Switch Form Fields
      this.verfyStep1 = false;
      this.verfyStep2 = true;

    },
    error => {

      alert("An error occured with sending the verification code.");
      alert(error);
      
    });

  }

  // This function generates a code saved to the users device
  // to enable the user to bypass authentication without a code
  generateVerificationNumberForAutoVerify(telephoneNumber){

    this.autoVerify = true;

    let loading = this.loadingCtrl.create({
      content: 'Auto verifying your account. Please wait...'
    });
    
    loading.present();
    
    let random = this.generateRandomNumber();
    this.storage.set('verify_number', random);
    this.credentials.verification = random + "";

    setTimeout(() => {
      loading.dismiss();
      
      this.loginAction = false;
      this.handleIntro();
      
    }, 2500);

  }

  generateRandomNumber(){
    return Math.floor(Math.random() * (999999 - 100000)) + 100000;
  }

	/* Triggers the generation of another random number and resends the SMS */
	retryVerificationNumber(){
		this.verifyTheUserWithTextMessage();
	}

  LoginUsingVerifyCode(){

    let verification = this.credentials.verification;
    let telephone = this.credentials.telephone;

    this.staff.verifyNumberWithTextCode(telephone, verification).subscribe(allowed => {
      
      if (allowed["error"] == false) {

        this.sendState = "Verification Successful, One Moment";

        // Set variable stating login successfull
        // Setting this tells our application that the user is successfully verified
        
        this.storage.set('verified_number', this.credentials.telephone);
        
        this.handleIntro();

      }else{
        
        this.sendState = "Invalid Auth Code";
        this.loginAction = false;
        this.showError("An error occured verifying your access, please enter the authentication code sent to your phone");

      }
      
    });

  }

  /* Allow the user to re-enter their telephone number */
  reenterNumber(){

    this.sendState = "";
    this.verfyStep2 = false;
    this.verfyStep1 = true;

  }

	handleIntro(){
		
		// Determine if the user has seen the intro our not as yet
		this.storage.get('introImgShown').then((result) => {
		  //If the intro is shown then redirect the user to the home screen
		  if(result == true){
			  this.navCtrl.setRoot(HomePage);
		  } else {
        this.storage.set('introImgShown', true);
        this.navCtrl.setRoot(WelcomePage);
		  }

		});

	}

}
