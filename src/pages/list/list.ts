import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PostPage } from '../post/post';
import { Storage } from '@ionic/storage';
import { StaffProvider } from '../../providers/staff/staff';
import { ToastController, AlertController } from 'ionic-angular';
/*import { trigger, state, style, animate, transition } from '@angular/animations';*/
import moment from 'moment';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage:Storage, public staff:StaffProvider, public toastCtrl: ToastController) {
    this.grabArticles();
  }

  public noarticles:boolean = false;
  public articleList:any[] = [];
  public removedArticleList:any[] = [];
  public editAvailable = false;

  grabArticles(){

    // set a key/value
    this.storage.get("myarticles").then((val) => {

        //console.log(val);

        if( val ){
          this.noarticles = false;
          this.articleList = val;
        }else{

          this.noarticles = true;
        }

       return name;
     });

  }

	presentRemoveToast() {
      let toast = this.toastCtrl.create({
        message: 'Article removed from list',
        duration: 3000
      });
      toast.present();
    }
	
	removeArticle(item, event, index){

		//console.log("Remove Article Information");
		//console.log(index);
		/*console.log(item);
		console.log(event);*/
		
		//alert("remove");
		// set a key/value
		
		this.removedArticleList.push(item);
		
		this.storage.set("edited_articles",this.removedArticleList);
		
		//console.log(this.removedArticleList);
		
		this.storage.get("myarticles").then((val) => {

			if( val ){
				val.splice(index, 1);
				this.storage.set("myarticles", val);
			}
			
			this.articleList = val;
			this.presentRemoveToast();
			
		});

    }
	
  /* Convert Date to proper human readable date */
  convertDate(date){
    if(date!= "0000-00-00 00:00:00"){
      return moment(date).format('LLLL');
    }
  }
  
	editItems(event){
		
		//console.log(event);
		
		if( this.editAvailable == false ){
			event.target.innerText = "Done";
		}else{
			event.target.innerText = "Edit";
		}
		
		this.editAvailable =! this.editAvailable;
	  
	}

  getTitle(item){
    //console.log(item);
    try{
      //console.log(item["article"]);
      return item["article"]["title"];
    }catch(e){

    }
  }

  getDate(item){
    try{
      return moment(item["article"]["date"]).format('LLLL');
    }catch(e){

    }
  }

  pushPage(item){
	//alert(JSON.stringify(item));
    // push another page onto the navigation stack
    // causing the nav controller to transition to the new page
    // optional data can also be passed to the pushed page.
    this.navCtrl.push(PostPage, {
      item: item["article"],
	  type: 'simple'
    });

  }

  getStorage(name){

    // set a key/value
    this.storage.get(name).then((val) => {
       //console.log('Requested Storage Data is', val);
       return name;
     });

  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ListPage, {
      item: item
    });
  }
}
