import { Component } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { NavController, LoadingController, Platform, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';
import { PostPage } from '../../post/post';
import { Storage } from '@ionic/storage';
import { StaffProvider } from '../../../providers/staff/staff';
import { Observable } from 'rxjs/Rx';
import moment from 'moment';
import { SuperTabsController } from 'ionic2-super-tabs';
import { Badge } from '@ionic-native/badge';

@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
  providers:[Badge],
})

export class NewsPage {

    public articlesToLoad = 10;
    url: string = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&per_page=' + this.articlesToLoad;
    staff_url: string = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&per_page=' + this.articlesToLoad;
    items: any;
    loader: any;
    public loadingActive:boolean = false;
	public moreArticlesAlert;

    public articlesLoaded = 10;
    public staff_articlesLoaded = 10;

	public headingName = "Latest News";
	
	public articleList:any[] = [];
	public lastDate;
	public noarticlesorconnection = false;

	public mrefresher;
	public active_loading = false;
  
    constructor( public badge:Badge, private superTabsCtrl: SuperTabsController, private http: Http, public loadingCtrl: LoadingController, private storage: Storage, public navCtrl: NavController, public staff:StaffProvider, public toastCtrl: ToastController, public alertCtrl:AlertController, private platform: Platform) {
		
		// Load Post Information
		//this.loadWordpressInfo();
		this.updateUserSavedList();

	}
	
	// Flow Functions
	ionViewWillEnter(){
		
		//this.badge.clear();
		this.badge.set(0);
		
		this.debugLog("ionViewWillEnter Loaded");
		this.doRefresh(null);
		this.pollServer();
	}

	doRefresh(refresher) {
		
		//alert("Polling");

		if( this.active_loading == false){

			this.mrefresher = refresher;
			this.active_loading = true;

			/* Once the Refresher Takes Over 8 Secs, we cancel the refresh */
			setTimeout(() => {
				if(refresher){
					if( refresher.state == "refreshing"){
						refresher.cancel();
					}
				}
			}, 10000);

			let loopNum = 0
			let amount_to_load = this.articlesLoaded + this.articlesToLoad;
			if( amount_to_load > 30){
				amount_to_load = 30;
			}

			this.url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&offset=0&per_page=' + amount_to_load;
			//alert( this.url );
			this.debugLog("doRefresh started");
			this.grabWordpressInfo("load");

		}else{
			//alert("Loading Occurring");
			try{
				refresher.cancel();
			}catch(e){
				
			}
		}
		
	}

	/* Handles grabbing the information from the Wordpress API */
    grabWordpressInfo(loadType) {
				
		if( loadType == "reload" ){
			this.presentLoading();
		}

		let url = this.url;
		if( loadType == "reload" ){
			url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&per_page=' + this.articlesLoaded;						
		}
		
		// Disable Errors
		this.noarticlesorconnection = false;

		this.http.get( url )
		.map(res => res.json())
		.map(data => { return this.handleArticleSorting(data,loadType); })
		.subscribe(
			data => {

				if( loadType == "load"){
									
					if( this.loader ){
						this.loader.dismiss();
					}
	
					this.items = data;
					
					// Update Storage
					this.setStorage("articles", data);
								
					this.active_loading = false;
					this.loadingActive = false;

					// Scan articles for saved selections and higlight them
					this.checkArticlesSaved(data);

				}else{
						
					if( this.loader ){
						this.loader.dismiss();
					}
					
					for( var i = 0; i < data.length; i++ ){
						this.items.push( data[i] );
					}

					// Update Storage
					this.setStorage("articles", this.items);

					this.loadingActive = false;
								
					if( this.infin ){ 
						this.infin.complete();
					}

					this.debugLog("Set Active Loading False");
					this.active_loading = false;
					this.loadingActive = false;
					this.debugLog("active_loading " + this.active_loading );

				}
								
				if(this.mrefresher){
					//console.log('Async operation has ended');
					this.mrefresher.complete();
					this.presentToast(' Refresh Complete! ');
				}
				
				this.superTabsCtrl.clearBadge('news_tab');

			},
			err => {
				
				this.debugLog("grabWordpressStaffInfo error occured with loadType " + loadType);
				this.handleGetWordpressInformationError(err,loadType);			

			}
		);
			
	}

	handleArticleSorting(data,loadType){

		var posts = [];
		let index = 0; 
		
		if( data.length > 0  ){
			for (let post of data) {
				
				/* Set date to the date of the very first index post */
				if( index == 0 && loadType == "load" ){
					this.setLastDate( post[ 'date' ] );
				}
				
				let featured_image = this.staff.getFeaturedImage(post);
				let category = this.staff.getCategory(post);
				//let savedState = this.checkIfSaved( post[ 'id' ] );
				let savedStatevar = -1;
				posts.push({'author':post['author' ], 'id':post[ 'id' ], 'title':post[ 'title' ][ 'rendered' ], 'content':post[ 'content' ][ 'rendered' ], 'category':category["category"],'date':post[ 'date' ], 'featured_media':post[ 'featured_media' ], "post_image":featured_image, "savedState":savedStatevar});
				index++;
				this.articlesLoaded++;
			}

		}else{
			
			this.presentToast(' No more post to be read ');
			this.loadingActive = false;

			if( this.infin ){
				this.infin.complete();
			}
		}
		
		return posts;
					
	}
	
	// Loop through the list of loaded articles and check the ones that
	// are saved in the users listing
	checkArticlesSaved(data){

		let matchesFound = 0;

		this.storage.get("myarticles").then((val) => {
		
			// Get the saved articles
			if( val ){
				this.debugLog(" Loop through " + val.length + "articles in myarticles listings");
				// Loop through articles in myarticles listing
				for( let i = 0; i < val.length; i++){
					// Loop through newly loaded data with articles
					for( let m = 0; m < data.length; m++){

						if( val[i]["article"]["id"] == data[m]["id"] ){
							this.debugLog( val[i]["article"]["id"] + " matches " + this.items[m]["id"] );
							data[m]["savedState"] = 1;

							// Update matches found so we'll know to update the listing
							matchesFound++;
							this.debugLog(" Update Match Found ");
						}

					}
				}

				// If matches found update listing
				if( matchesFound ){
					this.items = data;
					this.debugLog(" Update Staff Items List ");
				}
			} 

		});

	}
	
	handleGetWordpressInformationError(err, loadType){

		// Cancel infin staff loader
		if( this.infin ){ 
			this.infin.complete();
		}

		// Cancel staff loader
		if( this.loader ){
			this.loader.dismiss();
		}

		// Dismiss loader lock
		this.loadingActive = false;

		this.active_loading = false;
		
		this.debugLog("Revert to older articles");

		// Try to revert to older articles
		if( loadType != "none"){
			let items_temp = this.getStorage("articles");
			/*if(items_temp){
				this.items = items_temp;
			}else{
				this.noarticlesorconnection = true;
			}*/
		}
							
	}
	
	debugLog(msg){
		console.log(msg);
	}

	/* Handles grabbing the information from the Wordpress API specifically for News Articles */
	loadWordpressInfo() {
		
		// Check to see if any articles are saved
        this.storage.get("articles").then((val) => {
			
			if( val ){
				
				//alert("News Articles Found");

				// Get all articles and place into items variable to be displayed
				this.items = val;

				// Set the length of the articles
				this.articlesLoaded = this.items.length;
				
				this.doRefresh(null);

				// Get the last date from the last saved article
				this.getLastDate();
				
				// Initiate Polling Server for More Updates
				this.pollServer();
				//this.getMySavedArticles();
				
			}else{

				//alert("News No Articles Found");
				// Set articles loaded to 0
				this.articlesLoaded = 0;
				
				// Grab Articles from server
				this.grabWordpressInfo("load");
				
				// Initiate Polling Server for More Updates
				//this.pollServer();
			}
        });
		
	}
	
	updateUserSavedList(){
		
		/* Check for any items to be removed from the users save list */
		this.storage.get("articles").then((newsItemsTemp) => {
			
			if( newsItemsTemp ){
				
				/* Check for any items to be removed */
				this.storage.get("edited_articles").then((item) => {
					
					try{
						if(item && item.length > 0){
							for( let m = 0; m < item.length; m++ ){
								for( let i = 0; i < newsItemsTemp.length; i++){
									
									let articleID = item[m]["article"]["id"];
									
									if( newsItemsTemp[i]["id"] == articleID ){
										
										// Set Item to -1 to say not saved
										newsItemsTemp[i]["savedState"] = -1;
										
										// Remove the item found
										item.splice(m, 1);
										
										// Update Edited Articles so we know the items are removed
										this.storage.set("edited_articles",item);
										
										// Update Staff Items in Storage
										this.setStorage("articles", newsItemsTemp);
										
										this.items = newsItemsTemp;
									}
								}
							}
						}
					}catch(e){
						this.debugLog(e);
					}
					
				});
			}
		});
    }
	
	
	// Helper Functions
	
    presentToast(msg) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 3000
      });
      toast.present();
    }

    presentRemoveToast() {
      let toast = this.toastCtrl.create({
        message: 'Article removed from list',
        duration: 3000
      });
      toast.present();
    }
	
    getCategories( item ){

        return JSON.stringify( item["_embedded"]["wp:term"][0]["name"] );

    }

    presentLoading() {
		this.loader = this.loadingCtrl.create({
		content: "Please wait..."
		});
		this.loader.present();
    }
	
	// Check the server every so often for new messages
	// Handle editing the articles in terms of articles removed / edited
	pollServer(){
		
		Observable.interval(86400000).subscribe(x => {
			
			// Make sure the user if still authorised
			this.staff.checkUserStatus();

			// Check in with the server to discover more articles
			//this.doRefresh(null);

			// Reload all articles
			// Fix for articles that may not have loaded with images,
			// and also this cleans up articles that were removed
			this.grabWordpressInfo("reload"); 	
			

		});
  
	}
    
	getLastDateFromList(){
		
		let ndate = this.items[0]["date"];
		this.lastDate = ndate;
		this.setLastDate( ndate );
		
	}
	
	checkCardSpecial(date){
		return this.staff.checkCardSpecial(date,this.items);
	}
	
	
	showMoreArticlesAvailable(index){
		
		this.moreArticlesAlert = true;
		setTimeout(() => {
			this.moreArticlesAlert = false;
		}, 200);

	}
	
	// Handle opening news articles
    pushPage(item){
		this.navCtrl.push(PostPage, {
			item: item,
			type: "simple",
		});
    }

    public infin;
	
	// Load older articles
	doInfinite(infiniteScroll) {
		
		this.debugLog("Starting doInfinite");

		this.debugLog("Loading State: " + this.active_loading);
		// Check if any other loading is occuring
		if( this.active_loading == false ){
			
			// Show loading icon
			this.loadingActive = true;

			// If no loading, lock loading to prevent clashing with other loaders
			// activated by the user
			this.active_loading = true;

			if( this.articlesLoaded != this.items.length ){
				this.articlesLoaded = this.items.length;
			} 

			this.url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&offset='+ this.articlesLoaded +'&per_page=' + this.articlesToLoad;
			
			this.grabWordpressInfo("none");
			this.infin = infiniteScroll;

		}else{
			//alert("Loading Occuring do infinite");
		}
		
	}
		
	// Loop through the available article and select the correct saved articles
	checkIfSaved( id ){
		
		return new Promise((resolve,reject) => {
			this.storage.get("myarticles").then((articleList) => {
				
				if(articleList ){
					if( articleList.length > 0 ){
						
						for( let i = 0; i < articleList.length; i++ ){
							//alert( articleList[i]["id"] + " " + articleList[i]["article"]["id"] );
							if( id == articleList[i]["article"]["id"]){
								//alert("Match");
								resolve(true);
							}
							
						}
						reject(false);
						
					}else{
						reject(false);
					}
				}
			});
		});
	}	

    saveArticle(item, event){
			
		for( let i = 0; i < this.items.length; i++){
			if( this.items[i]["id"] == item.id){
				this.items[i]["savedState"] = true;
				this.storage.set("articles", this.items);
			}
		}
			
		// set a key/value
		this.storage.get("myarticles").then((val) => {

			let article_double = false;

          if( val ){
						
						for( let i = 0; i < val.length; i++){
							if( val[i]["article"]["id"] == item.id ){
									//alert(" Article Already Added");
									article_double = true;
							}
							
						}

						if( article_double == false ){
							val.push( {"article":item} );
							this.storage.set("myarticles", val);
						}

          }else{

            let data:any[] = [];
            data.push({"article":item});
            this.storage.set("myarticles", data);

          }
			
			try{
				event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star color_blue";
			}catch(e){
				
				let alert = this.alertCtrl.create({
                  title: 'Oops!',
                  subTitle: "Unable to save " + e,
                  buttons: ['OK']
                });

                alert.present();
				
			}
          
			this.presentToast('Article saved to list');

         //return name;
       });

    }
	
	removeArticle(item, event, index){

      	//console.log(event);
		let savedFound = false;
		
		for( let i = 0; i < this.items.length; i++){
			if( this.items[i]["id"] == item.id){
				item["savedState"] = -1;
				this.debugLog("Found");
			}
		}

		// set a key/value
		this.storage.get("myarticles").then((val) => {
			
			if( val ){
				for( let i = 0; i < val.length; i++){
					if( val[i]["article"]["id"] == item.id ){
						let mindex = val.indexOf(val[i]);
						//alert("Val I Index " + mindex )
						let removed = val.splice(mindex, 1);
						//alert("Removed " + JSON.stringify(removed) )
						this.storage.set("myarticles", val);
						//alert( JSON.stringify(val) );
						this.presentRemoveToast();
						return name;
					}
				}
			}

		});
		
		event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star-outline";

    }

    /* Convert Date to proper human readable date */
    convertDate(date){
      if(date!= "0000-00-00 00:00:00"){
				return moment(date).format('LL');
        //return this.datePipe.transform(date, 'fullDate');
      }
    }

     setStorage(name, data){

       // set a key/value
       this.storage.set(name, data);

     }

     getStorage(name){

       // set a key/value
       this.storage.get(name).then((val) => {
          //console.log('Requested Storage Data is', val);
          return name;
        });

    }
	 

	setLastDate(date){
		this.lastDate = date;
		this.storage.set("last_date", date);
	}
	
	getLastDate(){
		this.storage.get("last_date").then((val) => {
			this.lastDate = val;
			if( this.lastDate ){
				
			}else{
				this.lastDate = this.items[0]["date"];
			}
        });
	}

}
