import { Component, NgZone, ViewChild} from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { NavController, Nav, LoadingController, ToastController, AlertController, Platform, NavParams, ViewController } from 'ionic-angular';
import { PostPage } from '../../post/post';
import { Storage } from '@ionic/storage';
import { StaffProvider } from '../../../providers/staff/staff';
import { Observable } from 'rxjs/Rx';
import moment from 'moment';
import { SuperTabsController } from 'ionic2-super-tabs';
import { AsyncPipe } from '@angular/common';

import { ListPage } from '../../list/list';
import { Badge } from '@ionic-native/badge';

@Component({
  selector: 'page-staff',
  templateUrl: 'staff.html',
  providers:[Badge],
})
export class StaffPage {

	@ViewChild(Nav) nav: Nav;

    public articlesToLoad = 10;
    url: string = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories_exclude=160&per_page=' + this.articlesToLoad;
    staff_url: string = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&per_page=' + this.articlesToLoad;
    staff_items: any;
    staff_loader: any;
	public staffloadingActive:boolean = false;
	public active_loading:boolean = false;

    public staff_articlesLoaded = 10;
	
	public headingName = "Latest News";
	public articleList:any[] = [];
	public lastStaffDate;
	public mrefresher;

	public enable_reload = true;
	public enable_loadmore = true;
	public noarticlesorconnection = false;
	
	public infin_staff;

    constructor( public badge:Badge, private ngzone:NgZone, private superTabsCtrl: SuperTabsController, private http: Http, public loadingCtrl: LoadingController, private storage: Storage, public navCtrl: NavController, public staff:StaffProvider, public toastCtrl: ToastController, public alertCtrl:AlertController, private platform: Platform) {
		
		// Load Post Information
		this.updateUserSavedList();
		 
	}

	// Flow Functions
	ionViewWillEnter(){
	
		this.badge.clear();
		//this.badge.set(0);
	
		this.debugLog("ionViewWillEnter Loaded");
		this.doRefresh(null);
		this.pollServer();
	}

	// Handle loading new content using the refresh
	doRefresh(refresher) {

		this.debugLog("doRefresh Loaded");
		if( this.active_loading == false){

			this.mrefresher = refresher;
			this.active_loading = true;

			// Set a watcher for articles not being able to load
			this.setRefreshTracker(refresher);

			let amount_to_load = 0;
			amount_to_load = this.staff_articlesLoaded + this.articlesToLoad;
			if( amount_to_load > 30){
				amount_to_load = 30;
			}
			
			this.staff_url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&offset=0&per_page=' + amount_to_load;
			this.debugLog("Set URL " + this.staff_url);
			this.grabWordpressStaffInfo("load");

		}else{
			//alert("Loading Occurring");
			if( refresher ){
				refresher.cancel();
			}
		}

	}
		
	
	setRefreshTracker(refresher){
		
		/* Once the Refresher Takes Over 8 Secs, we cancel the refresh */
		setTimeout(() => {
			if(refresher){
				if( refresher.state == "refreshing"){
					
					
					let alert = this.alertCtrl.create({
					title: 'Refresh',
					message: 'Articles taking a while to load, would you like to continue loading?',
					buttons: [
						{
						text: 'Cancel',
						role: 'cancel',
						handler: () => {
							refresher.cancel();
						}
						},
						{
						text: 'Continue',
						handler: () => {
							this.setRefreshTracker(refresher);
						}
						}
					]
					});
					alert.present();

				}
			}
		}, 20000);
	}

	/* Handles grabbing the information from the Wordpress API specifically for Staff Articles */
	grabWordpressStaffInfo(loadType) {
		
		this.debugLog("grabWordpressStaffInfo started with loadType " + loadType);

		// Activate Loader
		this.staffloadingActive = true;

		// Disable Errors
		this.noarticlesorconnection = false;
		
		let url = this.staff_url;
		if( loadType == "reload" ){					
			url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&per_page=' + this.staff_articlesLoaded;
		}
		
		this.http.get( url )
		.map(res => res.json())
		.map(data => { return this.handleArticleSorting(data,loadType);  })
		.subscribe(
			data => {
				
				// Debugging Information
				this.debugLog(" Subscribe Data Successful for load type " + loadType);
				this.debugLog(" Data Length " + data.length );
				
				// Show Loader
				this.staffloadingActive = true;

				if( loadType == "load" ){
					this.handleLoad(data);
				} else{
					// Used for reload and none loadTypes
					this.handleAddition(data);
				}

				// Scan articles for saved selections and higlight them
				this.checkArticlesSaved(data);

				if( this.staff_loader ){
					this.staff_loader.dismiss();				
				}

				if(this.mrefresher){
					this.mrefresher.complete();
					this.presentToast(' Refresh Complete! ');
				}

				// Reset Badge if set
				this.superTabsCtrl.clearBadge('staff_tab');

			},
			err => {

				this.debugLog("grabWordpressStaffInfo error occured with loadType " + loadType);
				this.handleGetWordpressInformationError(err);
				
			}
		);

	}

	// 
	handleArticleSorting(data,loadType){
	
		var posts = [];
		let index = 0;

		if (data.length > 0){

			for (let post of data) {

				/* Set date to the date of the very first index post */
				if( index == 0 && loadType == "load" ){
					this.setLastDate( post[ 'date' ] );
				}
				
				let featured_image = this.staff.getFeaturedImage(post);
				let category = this.staff.getCategory(post);
				//let savedState = this.checkIfSaved( post[ 'id' ] );
				let savedStatevar = -1;
				posts.push({'author':post['author' ], 'id':post[ 'id' ], 'title':post[ 'title' ][ 'rendered' ], 'content':post[ 'content' ][ 'rendered' ], 'category':category["category"],'date':post[ 'date' ], 'featured_media':post[ 'featured_media' ], "post_image":featured_image, "savedState":savedStatevar });
				index++;
				this.staff_articlesLoaded++;
				 
			}
			
			//this.getLastStaffDate();
			
			return posts;
			
		}else{
			
			this.debugLog(" No Post Available ");
			this.presentToast(' No more post to be read ');
			
			this.staffloadingActive = false;
			this.active_loading = false;
			
			if( this.infin_staff ){
				this.infin_staff.complete();
			}
			return posts;
		}

		
	}

	handleGetWordpressInformationError(err){

		// Cancel infin staff loader
		if( this.infin_staff ){ 
			this.infin_staff.complete();
		}

		// Cancel staff loader
		if( this.staff_loader ){
			this.staff_loader.dismiss();
		}

		// Dismiss loader lock
		this.staffloadingActive = false;
		this.active_loading = false;
		
		this.debugLog("Revert to older articles");
		
		// Try to revert to older articles
		let staff_items_temp = this.getStorage("staff_articles");
		/*if(staff_items_temp){
			this.staff_items = staff_items_temp;
		}else{
			this.noarticlesorconnection = true;
		}*/
	}

	// Handles setting data to staff items
	handleLoad(data){

		this.debugLog("handleLoad started for " + data.length );
		this.staff_items = data;
	
		this.active_loading = false;
		this.staffloadingActive = false;
		
	}
	
	handleAddition(data){
		
		this.debugLog("handleAddition started for " + data.length );
		// Loop through all new data items
		for( var i = 0; i < data.length; i++ ){
			this.staff_items.push( data[i] );
		}

		// Remove staff loader lock
		this.staffloadingActive = false;
		
		// Set loading type back to false
		this.active_loading = false;
		

		// Complete Loader icon
		if( this.infin_staff ){ 
			this.infin_staff.complete(); 
		}

	}
		

	debugLog(msg){
		console.log(msg);
	}

	// Goes throught the entire selection of loaded articles and updates
	// removed saved articles
	updateUserSavedList(){

		this.storage.get("staff_articles").then((staffItemsTemp) => {
			
			if( staffItemsTemp ){
				
				/* Check for any items to be removed */
				this.storage.get("edited_articles").then((item) => {
					
					try{
						if(item && item.length > 0){
							for( let m = 0; m < item.length; m++ ){
								for( let i = 0; i < staffItemsTemp.length; i++){
									
									let articleID = item[m]["article"]["id"];
									
									if( staffItemsTemp[i]["id"] == articleID ){
										
										// Set Item to -1 to say not saved
										staffItemsTemp[i]["savedState"] = -1;
										
										// Remove the item found
										item.splice(m, 1);
										
										// Update Edited Articles so we know the items are removed
										this.storage.set("edited_articles",item);
										
										// Update Staff Items in Storage
										this.setStorage("staff_articles", staffItemsTemp);
										
										this.staff_items = staffItemsTemp;
									}
								}
							}
							
							
							
						}
					}catch(e){
						//console.log(e);
					}
					
				});
			}
		});
		
    }
	
	
	
	// Helper Functions
    presentToast(msg) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 3000
      });
      toast.present();
    }

    presentRemoveToast() {
      let toast = this.toastCtrl.create({
        message: 'Article removed from list',
        duration: 3000
      });
      toast.present();
    }
	
    getCategories( item ){
        return JSON.stringify( item["_embedded"]["wp:term"][0]["name"] );
    }
	
    staffPresentLoading() {
      this.staff_loader = this.loadingCtrl.create({
        content: "Please wait..."
      });
      this.staff_loader.present();
    }
	
	// Check the server every so often for new messages
	pollServer(){
		
		Observable.interval(86400000).subscribe(x => {
			if(this.lastStaffDate == null){
				this.getLastDate();
			}
			
			this.staff.checkUserStatus();

			this.doRefresh(null);
			
		});
  
	}
	
	// Loop through the list of loaded articles and check the ones that
	// are saved in the users listing
	checkArticlesSaved(data){

		let matchesFound = 0;

		this.storage.get("myarticles").then((val) => {
		
			// Get the saved articles
			if( val ){
				this.debugLog(" Loop through " + val.length + "articles in myarticles listings");
				// Loop through articles in myarticles listing
				for( let i = 0; i < val.length; i++){
					// Loop through newly loaded data with articles
					for( let m = 0; m < data.length; m++){

						if( val[i]["article"]["id"] == data[m]["id"] ){
							this.debugLog( val[i]["article"]["id"] + " matches " + this.staff_items[m]["id"] );
							data[m]["savedState"] = 1;

							// Update matches found so we'll know to update the listing
							matchesFound++;
							this.debugLog(" Update Match Found ");
						}

					}
				}

				// If matches found update listing
				if( matchesFound ){
					this.staff_items = data;
					this.debugLog(" Update Staff Items List ");
				}
			}

		});

	}
		
	checkIfSaved( id ){
		
		return new Promise((resolve,reject) => {
			this.storage.get("myarticles").then((articleList) => {
				
				if( articleList ){
					if( articleList.length > 0 ){
						
						for( let i = 0; i < articleList.length; i++ ){
							//alert( articleList[i]["id"] + " " + articleList[i]["article"]["id"] );
							if( id == articleList[i]["article"]["id"]){
								//alert("Match");
								resolve(true);
							}
							
						}
						reject(false);
						
					}else{
						reject(false);
					}
				}
			});
		});
	}
	
    // Handle opening news articles
    pushPage(item){
		this.navCtrl.push(PostPage, {
			item: item,
			type: "simple",
		});
    }
	
	doInfiniteStaff(infiniteScroll) {

		this.debugLog(" doInfiniteStaff function started ");

		if( this.active_loading == false ){

			this.staffloadingActive = true;
			this.active_loading = true;

			this.debugLog("Staff Articles Loaded " + this.staff_articlesLoaded);
			this.debugLog("Staff Items Loaded " + this.staff_items.length);
			
			if( this.staff_articlesLoaded != this.staff_items.length ){
				this.staff_articlesLoaded = this.staff_items.length;
			} 

			this.staff_url = 'https://www.simpsonmotors.com/index.php/wp-json/wp/v2/posts?_embed&categories=160&offset='+ this.staff_articlesLoaded +'&per_page=' + this.articlesToLoad;

			this.debugLog(" Set URL to " + this.staff_url );
			this.debugLog(" Call to grabWordpressStaffInfo, loadType none ");
			
			this.grabWordpressStaffInfo("none");
			this.infin_staff = infiniteScroll;

		}else{
			this.debugLog("Unable to load, another loading process is occuring");
		}

    }

    saveArticle(item, event){
	
		//alert("clicked");
		//console.log(event);
		let savedFound = false;
			
		for( let i = 0; i < this.staff_items.length; i++){
			if( this.staff_items[i]["id"] == item.id){
				this.staff_items[i]["savedState"] = true;
				//console.log("Found");
				this.storage.set("staff_articles", this.staff_items);
			}
		}
			
      // set a key/value
      this.storage.get("myarticles").then((val) => {

          if( val ){
            val.push( {"article":item} );
            this.storage.set("myarticles", val);
          }else{

            let data:any[] = [];
            data.push({"article":item});
            this.storage.set("myarticles", data);

          }
			
			try{
				event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star color_blue";
			}catch(e){
				
				let alert = this.alertCtrl.create({
                  title: 'Oops!',
                  subTitle: "Unable to save " + e,
                  buttons: ['OK']
                });

                alert.present();
				
			}
          
			this.presentToast('Article saved to list');

         //return name;
       });

    }
	
	removeArticle(item, event, index){

		for( let i = 0; i < this.staff_items.length; i++){
			if( this.staff_items[i]["id"] == item.id){
				this.staff_items[i]["savedState"] = -1;
				this.setStorage("staff_articles", this.staff_items);
			}
		}
		
		event.target.className = "saveArticleIcon ext-right icon icon-md ion-md-star-outline";
		
		// set a key/value
		this.storage.get("myarticles").then((val) => {

			if( val ){
				for( let i = 0; i < val.length; i++){
					if( val[i]["article"]["id"] == item.id ){
						let mindex = val.indexOf(val[i]);
						//alert("Val I Index " + mindex )
						let removed = val.splice(mindex, 1);
						//alert("Removed " + JSON.stringify(removed) )
						this.storage.set("myarticles", val);
						//alert( JSON.stringify(val) );
						this.presentRemoveToast();
						return name;
					}
				}
			}

		});

    }

    /* Convert Date to proper human readable date */
    convertDate(date){
      if(date!= "0000-00-00 00:00:00"){
		return moment(date).format('LL');
        //return this.datePipe.transform(date, 'fullDate');
      }
    }

    setStorage(name, data){
       this.storage.set(name, data);
    }

    getStorage(name){
		// set a key/value
		this.storage.get(name).then((val) => {
			return name;
        });
	}
	 
	checkCardSpecial(date){
		return this.staff.checkCardSpecial(date,this.staff_items);
	}
	
	public moreArticlesAlert;
	
	showMoreArticlesAvailable(index){
		
		this.moreArticlesAlert = true;
		setTimeout(() => {
		  this.moreArticlesAlert = false;
		}, 200);
	}
	 
	setLastDate(date){
		this.lastStaffDate = date;
		this.storage.set("staff_last_date", date);
	}
	
	getLastDate(){
		this.storage.get("staff_last_date").then((val) => {
			this.lastStaffDate = val;
			if( this.lastStaffDate ){
				
			}else{
				this.lastStaffDate = this.staff_items[0]["date"];
			}
        });
	}

}
