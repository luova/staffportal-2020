import { Injectable } from '@angular/core';

@Injectable()
export class Config{
    public wpurl = "";
}

export const sender_id = '801139965353';
export const oneSignalAppId = 'bc17d9b8-a757-418d-9e5f-1f369f5df780';
export const debugMode = false;
export const GoogleAnalyticsCode = 'UA-104903092-1';