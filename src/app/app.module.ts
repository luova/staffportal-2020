import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PostPageModule } from '../pages/post/post.module';
import { SearchPageModule } from '../pages/search/search.module';
import { ListPage } from '../pages/list/list';
import { LoginPageModule } from '../pages/login/login.module';
import { NewsPage } from '../pages/categories/news/news';
import { StaffPage } from '../pages/categories/staff/staff';
import { WelcomePageModule } from '../pages/welcome/welcome.module';

import { SettingsPage } from '../pages/settings/settings';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { Http } from '@angular/http';
import { WpProvider } from '../providers/wp/wp';
import {
  WpApiModule,
  WpApiLoader,
  WpApiStaticLoader
} from 'wp-api-angular';
import { AuthService } from '../providers/auth-service/auth-service';
import { StaffProvider } from '../providers/staff/staff';
import { SuperTabsModule } from 'ionic2-super-tabs';
import {Badge } from '@ionic-native/badge';

import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
//import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { OneSignal } from '@ionic-native/onesignal';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

export function WpApiLoaderFactory(http) {
  return new WpApiStaticLoader(http, 'http://simpson-motors.com/wp-json/');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    StaffPage,
    NewsPage,
    ListPage,
    SettingsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    PostPageModule,
    LoginPageModule,
    WelcomePageModule,
    SearchPageModule,
	SuperTabsModule.forRoot(),
    IonicStorageModule.forRoot(),
        WpApiModule.forRoot({
      provide: WpApiLoader,
      useFactory: (WpApiLoaderFactory),
      deps: [Http]
    }),
	IonicModule.forRoot(MyApp, {
		tabsHideOnSubPages: false
	})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
	  StaffPage,
    NewsPage,
    ListPage,
    SettingsPage
  ],
  providers: [
    StatusBar,
    OneSignal,
    SplashScreen,
    InAppBrowser,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WpProvider,
    StaffProvider,
    Badge,
    PhotoViewer,
  ]
})
export class AppModule {}
