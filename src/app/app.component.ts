import { Component, ViewChild, NgZone, ApplicationRef } from '@angular/core';
import { Nav, Platform, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { PostPage } from '../pages/post/post';
import { WelcomePage } from '../pages/welcome/welcome';
import { LoginPage } from '../pages/login/login';
import { StaffProvider } from '../providers/staff/staff';
import { AuthService } from '../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';

import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { ToastController, AlertController } from 'ionic-angular';
import { Badge } from '@ionic-native/badge';

import { OneSignal, OSNotificationPayload } from '@ionic-native/onesignal';
import { oneSignalAppId, sender_id, debugMode, GoogleAnalyticsCode } from '../config';

@Component({
  templateUrl: 'app.html',
  providers:[GoogleAnalytics, Badge]
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
	
	public username = "";
	public playerID;
	rootPage: any;
	public swapRoot = false;
	public nowDate: Date = new Date();
	public ONE_HOUR = 60 * 60 * 1000; /* ms */
	public debugMode = debugMode;  
	public articlesToBeRead = 0;
	public version_code;
	public progressStatus;
	private notHandlerCheck;
	
	isUpdateAvailable:boolean = false; 

	pages: Array<{title: string, component: any}>;

	constructor(private appRef: ApplicationRef, private ngzone:NgZone,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private oneSignal: OneSignal, private ga: GoogleAnalytics, public loadingCtrl:LoadingController, public storage: Storage, public staff:StaffProvider, public authService:AuthService, public alertCtrl:AlertController, private badge: Badge) {
		
		this.initializeAppTest();

		//this.appVersion.getVersionNumber().then((s) => {
		//	this.version_code = s;
	 	//});    
		this.version_code = "5.3";

		// used for an example of ngFor and navigation
		this.pages = [
			{ title: 'Home', component: HomePage },
			{ title: 'List', component: ListPage }
		];
	}
	
	initializeAppTest(){
		
		this.platform.ready().then(() => {



			this.storage.get("verified_number").then((val) => {
				if(val){
				
					this.nav.setRoot(HomePage);
					this.setupOneSignalNotification();

					//this.getUserInformation();
					//this.setUserLoginState();

				}else{
					this.nav.setRoot(LoginPage);
				}
			});

			this.checkForUpdate();
			
		});
	}


	setupOneSignalNotification(){
		
		this.oneSignal.startInit(oneSignalAppId, sender_id);

		//The inFocusDisplaying() method, controls how OneSignal notifications will be shown when one is received while your app is in focus. 

		this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

		this.oneSignal.setSubscription(true);

		//The handleNotificationReceived() is a method that runs when a notification is received. It calls the onPushReceived()

		this.oneSignal.handleNotificationReceived().subscribe(data => this.onPushReceived(data, data.payload));

		// The handleNotificationOpened() is a method that runs when a notification is tapped or when closing an alert notification shown in the app.
		this.oneSignal.handleNotificationOpened().subscribe(data => this.onPushOpened(data, data.notification.payload));

		// Check to see if the user gave us permission
		let state = this.oneSignal.getPermissionSubscriptionState();
		
		//The endInit() method must be called to complete the initialization of OneSignal.
		this.oneSignal.endInit();

		// Get the User ID, this identifies the user. We'll use this ID and set the Player ID
		this.oneSignal.getIds().then((ids) => {

			let playerID = ids.userId;
			//alert(playerID);
			this.storage.set("playerID",playerID);
				
		});
		
		window["plugins"].OneSignal.clearOneSignalNotifications();
		this.badge.clear();
		
	}

	getUserInformation(){

		try{
			
			this.storage.get('userinfo').then((result) => {
					
				if(result){
					this.username = result.firstname + " " + result.lastname;
				}else{
					
					setTimeout(()=>{
						
						this.storage.get('userinfo').then((result) => {
							
							if(result){
								this.username = result.firstname + " " + result.lastname;
							}
						});
						
					},120000);
					
				}
				
			});
			
		}catch(e){
			
		}

	}

	// Code to handle the notifications
	private onPushReceived(data, payload: OSNotificationPayload) {
		//alert('Push recevied:' + payload.body);

		if( this.notHandlerCheck != true ){
			//alert("Running handleNotificationReceived from handleNotificationOpened ");
			this.notHandlerCheck = true;
			this.notificationHandling(data);
		}
	}
	
	private onPushOpened(data, payload: OSNotificationPayload) {
		//alert('Push opened: ' + payload.body);

		if( this.notHandlerCheck != true ){
			//alert("Running notificationHandling from handleNotificationOpened ");
			this.notHandlerCheck = true;
			this.notificationHandling(data);
		}

	}

	// Custom behavious to handle how the app should behave with a new notification
	private notificationHandling(data){
		
		this.notHandlerCheck = false;
		
        if( this.debugMode == true ){
			
			this.storage.set('launchPost', data);
			if(data){
				this.nav.push(PostPage, {
					item: data,
					type: "notification"
				});
			}
		}else{
			
			let sid = data.notification.payload.additionalData.postID;
			data["sid"] = sid;

			this.storage.set('launchPost', data);
			if(data){
				this.nav.push(PostPage, {
					item: data,
					type: "notification"
				});
			}
		}
    
    }
	
	async showAlert(title, msg, task) {
		const alert = await this.alertCtrl.create({
		  title: title,
		  buttons: [
			{
			  text: 'Action: ${task}',
			  handler: () => {
				// E.g: Navigate to a specific screen
			  }
			}
		  ]
		})
		alert.present();
	}
	  
	public new_version_link = null;

	checkForUpdate(){
		this.staff.getUpdateInformation(this.version_code).subscribe(data => {
			console.log("checkForUpdate");
			console.log(data);
			console.log(data["link"]);
			
			if ( data["link"] != null ){
				this.isUpdateAvailable = true;
				this.new_version_link = data["link"];

				let alert = this.alertCtrl.create({
					title: "Portal Update",
					message: "A new version of the Staff Portal is available.",
					buttons: [
						{
							text: 'Download Update',
							role: 'ok',
							handler: () => {
								window.open(data["link"],'_system', 'location=yes');
							}
						},
					]
				});
		
				alert.present();
				
			}
		});
	}

	goToUpdate(){
		window.open(this.new_version_link,'_system', 'location=yes');
	}
	
	// Make a call to the server to identify how many articles have not being read
	// after which set a badge to alert the user.

	checkArticlesToBeRead(){
		
		//Check if the user has ever logged on
			this.storage.get("staff_last_date").then((val) => {
				
				if(val){
					
					this.staff.getMoreStaff(val).subscribe(
						data => {
							//alert( JSON.stringify(data) );
							this.articlesToBeRead = this.articlesToBeRead + data.length;
							this.setBadge( this.articlesToBeRead );
						},
						err => {
							//alert("Error");
							//alert(JSON.stringify(err));
						}
					);
					
				}else{
					
					this.staff.grabStaff().subscribe(
						data => {
							//alert( JSON.stringify(data) );
							this.articlesToBeRead = this.articlesToBeRead + data.length;
							this.setBadge( this.articlesToBeRead );
						},
						err => {
							//alert("Error");
							//alert(JSON.stringify(err));
						}
					);
					
				}
				
			});
			

			// Get stories from the general category to be read
			this.storage.get("last_date").then((val) => {
				
				if(val){
					
					this.staff.getMore(val).subscribe(
						data => {
							//alert( JSON.stringify(data) );
							this.articlesToBeRead = this.articlesToBeRead + data.length;
							this.setBadge( this.articlesToBeRead );
						},
						err => {
							//alert("Error");
							//alert(JSON.stringify(err));
						}
					);
					
				}else{
					
					this.staff.grabGeneral().subscribe(
						data => {
							//alert( JSON.stringify(data) );
							this.articlesToBeRead = this.articlesToBeRead + data.length;
							this.setBadge( this.articlesToBeRead );
						},
						err => {
							//alert("Error");
							//alert(JSON.stringify(err));
						}
					);
				
				}
			
			});
			
	}
	
	setBadge(amt){
		
		this.badge.get().then((bcount) => {
			this.badge.decrease(bcount);
		});

		if( this.articlesToBeRead == 0 ){
			//this.badge.set(amt);
			this.badge.increase(amt);
			//this.badge.set(1);
		}else{
			this.badge.increase(amt);
			//this.badge.set(1);
		}
		
	}
	
	initializeApp() {

		this.platform.ready().then(() => {
			
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			this.statusBar.styleDefault();
			this.splashScreen.hide();
			
			this.oneSignal.handleNotificationReceived().subscribe((data) => {
				//alert("handleNotificationReceived " + this.notHandlerCheck);
				if( this.notHandlerCheck != true ){
					//alert("Running handleNotificationReceived from handleNotificationOpened ");
					this.notHandlerCheck = true;
					this.notificationHandling(data);
				}
			});
			
			this.oneSignal.handleNotificationOpened().subscribe((data) => {
				//alert("handleNotificationOpened " + this.notHandlerCheck);
				if( this.notHandlerCheck != true ){
					//alert("Running notificationHandling from handleNotificationOpened ");
					this.notHandlerCheck = true;
					this.notificationHandling(data);
				}
			});

			// Get and Verify the staff members number, if verified then allow the
			// user to receive notifications

			// Always handle notifications and set them to launch post
			// then check the home page constantly checking
		
			this.storage.get('verified_number').then((result) => {
				
				if( result ){
					/* If the number is verified */
					
					this.staff.verifyNumberAgain(result).subscribe(data => {
						
						//alert("data (tel number for verify again)");
						//alert( JSON.stringify(data) );
						
						if( data["error"] != true ){
								
							/* Update the last login */
							this.setUserLoginState();
							
							/* Set time last verified */
							this.storage.set('last_verified', this.nowDate);
							
							/* Initialise the OneSignal Plugin */
							this.oneSignal.startInit('bc17d9b8-a757-418d-9e5f-1f369f5df780', '801139965353');
							this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

							let state = this.oneSignal.getPermissionSubscriptionState()
							
							this.oneSignal.getIds().then((ids) => {
					
								let playerID = ids.userId;
								this.playerID = playerID;
								this.storage.set("playerID",playerID);
									
							});
									
							this.oneSignal.endInit();
							
							this.nav.setRoot(HomePage);
							
						}else{
							
							// Unable to re verify the user again on the server
							// Remove the user verified status
							this.storage.set('verified',false);
							this.storage.set('usertoken', null);
							this.oneSignal.setSubscription(false);
							
							// Notify the user of the error
							let alert = this.alertCtrl.create({
								title: 'Oops!',
								subTitle: "Access Error: " + data["message"],
								buttons: ['OK']
							});

							alert.present();
							
							// Relocate them to the login page
							this.nav.setRoot(LoginPage);
							
						}
					},
					err => {
						
						//alert("Set Page Error");	
						this.nav.setRoot(HomePage);

					});
					
				}else{
					
					//alert("Remove Subscription 1");
					this.nav.setRoot(LoginPage);
					this.oneSignal.setSubscription(false);
				}		
			});
	  
			//Google Analytics
			this.ga.startTrackerWithId(GoogleAnalyticsCode)
			.then(() => {
				//console.log('Google analytics is ready now');
				this.ga.trackView('test');
				// Tracker is ready
				// You can now track pages or set additional information such as AppVersion or UserId
			})
			.catch(e => console.log('Error starting GoogleAnalytics', e));

		});
	
	}

	
	/* Sets the user information in the sidebar and updates 
	their last login on the server */
	setUserLoginState(){
			
		this.storage.get('usertoken').then((result) => {

		  if(result){
			
			this.oneSignal.getIds().then((ids) => {
					
				let playerID = ids.userId;
				this.staff.updateLastLogin(playerID).subscribe(data => {
					//alert( "Last Login Update Subscribe " + JSON.stringify( data ) );
					//this.handleIntro();
				},
				error => {
				  //this.showError(error);
				});
					
			});
		  }

		});
		
		try{
			
			this.storage.get('userinfo').then((result) => {
					
				//console.log( 'userinfo' );
				//console.log( result );
				//alert("User info");
				//alert(result);
				
				if(result){
					this.username = result.firstname + " " + result.lastname;
				}else{
					
					setTimeout(()=>{
						
						this.storage.get('userinfo').then((result) => {
							
							if(result){
								this.username = result.firstname + " " + result.lastname;
							}
						});
						
					},120000);
					
				}
				
			});
			
		}catch(e){
			
		}
	}
	
	openPage(page) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		this.nav.setRoot(page.component);
	}

	callAlert(syncMessage){

		let alert = this.alertCtrl.create({
			title: "App Update Status",
			subTitle: syncMessage,
			buttons: ['OK']
		});

		alert.present();

	}


	presentLoadingDefault(message) {

		let loading = this.loadingCtrl.create({
		  content: message
		});
	  
		loading.present();
	  
		setTimeout(() => {
		  loading.dismiss();
		}, 5000);

	}

	

	logout(){
		this.storage.set('usertoken', null);
		this.storage.set('introImgShown', null);
		this.storage.set("verified_number",null);
		
		this.storage.set("articles", null);
		this.storage.set("staff_articles", null);
		this.storage.set("last_date", null);
		this.storage.set("staff_last_date", null);
		
		this.oneSignal.setSubscription(false);
		
		this.nav.setRoot(LoginPage);
	}
	
	openPage2(page) {
		
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		if( page == "home"){
			this.nav.setRoot(HomePage);
		}else if( page == "list"){
			this.nav.setRoot(ListPage);
		}
		
	}
}
